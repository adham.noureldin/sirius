//////////////////////////////////////////////
///////////////// Test Servo /////////////////
//////////////////////////////////////////////

#include <Arduino.h>

#include <ESP32Servo.h>

Servo servo; // create servo object to control a servo
// 16 servo objects can be created on the ESP32

#define ANGLE_OUVRE_ALPHA 20
#define ANGLE_FERME_ALPHA 72

#define ANGLE_OUVRE_BETA 180
#define ANGLE_FERME_BETA 45

int pos = 0; // variable to store the servo position
// Recommended PWM GPIO pins on the ESP32 include 2,4,12-19,21-23,25-27,32-33
// Possible PWM GPIO pins on the ESP32-S2: 0(used by on-board button),1-17,18(used by on-board LED),19-21,26,33-42
// Possible PWM GPIO pins on the ESP32-S3: 0(used by on-board button),1-21,35-45,47,48(used by on-board LED)
// Possible PWM GPIO pins on the ESP32-C3: 0(used by on-board button),1-7,8(used by on-board LED),9-10,18-21
int servoPin = 15;        // GPIO pin connected to the servo
const int switchPin = 19; // Pin de l'interrupteur
const int led1Pin = 4;    // Pin de la LED 1
const int led2Pin = 13;   // Pin de la LED 2

void setup()
{
  // Allow allocation of all timers
  Serial.begin(9600);
  delay(1000);
  Serial.println("Serial OK");
  delay(1000);
  pinMode(servoPin, OUTPUT);
  pinMode(switchPin, INPUT);
  pinMode(led1Pin, OUTPUT);
  pinMode(led2Pin, OUTPUT);
  Serial.println("Pins OK");
  // ESP32PWM::allocateTimer(0);
  // ESP32PWM::allocateTimer(1);
  // ESP32PWM::allocateTimer(2);
  // ESP32PWM::allocateTimer(3);
  Serial.println("Timer OK");
  // servo.setPeriodHertz(50); // standard 50 hz servo
  Serial.println("Period OK");
  servo.attach(servoPin);         // attaches the servo on pin 18 to the servo object
                                  // using default min/max of 1000us and 2000us
                                  // different servos may require different min/max settings
                                  // for an accurate 0 to 180 sweep
  servo.write(ANGLE_FERME_ALPHA); // tell servo to go to position in variable 'pos'
  Serial.println("Setup done");
}

void loop()
{
  if (digitalRead(switchPin) == LOW)
  {
    servo.write(ANGLE_FERME_ALPHA); // tell servo to go to position in variable 'pos'
    digitalWrite(led1Pin, LOW);
    digitalWrite(led2Pin, HIGH);
    Serial.println("Fermeture porte");
  }
  else
  {
    servo.write(ANGLE_OUVRE_ALPHA); // tell servo to go to position in variable 'pos'
    digitalWrite(led1Pin, HIGH);
    digitalWrite(led2Pin, LOW);
    Serial.println("Ouverture porte");
  }
}

////////////////////////////////////////////
///////////////// Test Sep /////////////////
////////////////////////////////////////////

// #include <Arduino.h>

// const int seppin = 22;    // Pin pour déclencher la séparation
// const int switchPin = 19; // Pin de l'interrupteur
// const int led1Pin = 4;    // Pin de la LED 1
// const int led2Pin = 13;   // Pin de la LED 2

// void setup()
// {
//   pinMode(switchPin, INPUT);
//   pinMode(led1Pin, OUTPUT);
//   pinMode(led2Pin, OUTPUT);
//   pinMode(seppin, OUTPUT);
//   digitalWrite(seppin, LOW);
//   delay(1000);
//   Serial.begin(115200);
//   delay(1000);
//   Serial.println("Serial OK");
// }

// void loop()
// {
//   if (digitalRead(switchPin) == LOW)
//   {
//     digitalWrite(seppin, HIGH);
//     digitalWrite(led1Pin, LOW);
//     digitalWrite(led2Pin, HIGH);
//     Serial.println("Séparation");
//   }
//   else
//   {
//     digitalWrite(seppin, LOW);
//     digitalWrite(led1Pin, HIGH);
//     digitalWrite(led2Pin, LOW);
//     Serial.println("Pas de séparation");
//   }
// }

/////////////////////////////////////////////
//////////////// Test Buzzer ////////////////
/////////////////////////////////////////////

// #include <Arduino.h>

// const int buzzerPin = 2;  // Pin du buzzer
// const int switchPin = 19; // Pin de l'interrupteur
// const int led1Pin = 4;    // Pin de la LED 1
// const int led2Pin = 13;   // Pin de la LED 2

// void setup()
// {
//   pinMode(switchPin, INPUT);
//   pinMode(led1Pin, OUTPUT);
//   pinMode(led2Pin, OUTPUT);
//   pinMode(buzzerPin, OUTPUT);
//   digitalWrite(buzzerPin, LOW);
//   delay(1000);
//   Serial.begin(9600);
//   delay(1000);
//   Serial.println("Serial OK");
// }

// void loop()
// {
//   if (digitalRead(switchPin) == LOW)
//   {
//     digitalWrite(buzzerPin, LOW);
//     digitalWrite(led1Pin, LOW);
//     digitalWrite(led2Pin, HIGH);
//     Serial.println("Buzzer OFF");
//   }
//   else
//   {
//     digitalWrite(buzzerPin, HIGH);
//     digitalWrite(led1Pin, HIGH);
//     digitalWrite(led2Pin, LOW);
//     Serial.println("Buzzer ON");
//   }
// }

/////////////////////////////////////////////
//////////////// Test Infla /////////////////
/////////////////////////////////////////////

// #include <Arduino.h>

// const int mafpin = 32;
// const int switchPin = 19; // Pin de l'interrupteur
// const int led1Pin = 4;    // Pin de la LED 1
// const int led2Pin = 13;   // Pin de la LED 2

// void setup()
// {
//   pinMode(switchPin, INPUT);
//   pinMode(led1Pin, OUTPUT);
//   pinMode(led2Pin, OUTPUT);
//   pinMode(mafpin, OUTPUT);
//   digitalWrite(mafpin, LOW);
//   delay(1000);
//   Serial.begin(9600);
//   delay(1000);
//   Serial.println("Serial OK");
// }

// void loop()
// {
//   if (digitalRead(switchPin) == LOW)
//   {
//     digitalWrite(mafpin, LOW);
//     digitalWrite(led1Pin, LOW);
//     digitalWrite(led2Pin, HIGH);
//     Serial.println("Pas de mise à feu");
//   }
//   else
//   {
//     digitalWrite(mafpin, HIGH);
//     digitalWrite(led1Pin, HIGH);
//     digitalWrite(led2Pin, LOW);
//     Serial.println("mise à feu");
//   }
// }
