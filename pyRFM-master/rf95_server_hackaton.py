#!/usb/bin/env python

"""
	waits for incomming message and sends response
"""
__author__	= """Alexander Krause <alexander.krause@ed-solutions.de>"""
__date__ 		= "2016-12-28"
__version__	= "0.1.0"
__license__ = "GPL"

import sys
import os
import array

sys.path.append(
	os.path.join(
		os.path.dirname(__file__),
		'..'
	)
)
	
import lib as pyrfm
import struct
import numpy as np
import time

conf={
	'll':{
		'type':'rfm95'
	},
	'pl':{
		'type':	'serial_seed',
		'port':	'COM5'
	}
}
ll=pyrfm.getLL(conf)

print('HW-Version: ', ll.getVersion())

if ll.setOpModeSleep(True,True):
	ll.setFiFo()
	ll.setOpModeIdle()
	ll.setModemConfig('Bw125Cr45Sf128')
	# ll.setModemConfig('Bw125Cr48Sf4096')
	
	ll.setPreambleLength(8)
	ll.setFrequency(868)
	ll.setTxPower(14)
	
	while True:
		#ll.sendStr('Hello world!')
		#ll.waitPacketSent()
		
		if ll.waitRX(timeout=3):
			try:
				data=ll.recv()
				
				# print hour mn s
				byte_array = array.array('B', data).tobytes()[4:-1]

				print('time:', time.strftime("%H:%M:%S", time.localtime()))
				print('message (as bytes):', byte_array)  # Print the byte array

				# Convert byte array to hexadecimal representation
				hex_string = ''.join('{:02x}'.format(byte) for byte in byte_array)

				# print('message (as hexadecimal):', hex_string)  # Print the message as a hexadecimal string
				print('')
			except Exception as e:
				print(e)
				print('error')
			
			
