import numpy as np
import serial

# Configuration du port série
ser = serial.Serial('COM5', 9600, timeout=1)  # Remplacez 'COM5' par le port approprié et 9600 par le baud rate correspondant

try:
    while True:
        if ser.in_waiting > 0:
            # Lecture des données reçues
            received_data = ser.readline().decode().strip()
            
            # Affichage du message reçu
            print("Message reçu:", received_data)

except KeyboardInterrupt:
    pass

finally:
    # Fermeture du port série à la fin
    ser.close()