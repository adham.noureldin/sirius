# serial communication :
import time
import serial  # pip install pyserial
import serial.tools.list_ports


def ascii_to_str(hex_string):
    bytes_object = bytes.fromhex(hex_string)
    ascii_string = bytes_object.decode("ASCII")
    return ascii_string


# get all the serial ports
ports = list(serial.tools.list_ports.comports())
# Open the serial port
print("Liste des ports disponibles :")
for p in ports:
    print(p)
ser = serial.Serial(
    ports[1][0], 115200, timeout=3
)  # changer le port s'il ne correspond pas à celui du ftdi
# ser = serial.Serial("/dev/ttyUSB0", 115200, timeout=3) # changer le port s'il ne correspond pas à celui du ftdi
# write the initialisation command and print the response
ser.write("AT+MODE=TEST\r\n".encode())  # mode test
# wait for the response
time.sleep(1)
while ser.in_waiting == 0:
    pass
print(ser.readline().decode())
ser.write(
    "AT+TEST=RFCFG,868,SF12,500,8,8,14,ON,OFF,OFF\r\n".encode()
)  # configurer le module en mode LoRaWAN
time.sleep(1)

while ser.in_waiting == 0:
    pass
print(ser.readline().decode())
ser.write("AT+TEST=RXLRPKT\r\n".encode())  # activer le mode réception continue
time.sleep(1)

while ser.in_waiting == 0:
    pass
print(ser.readline().decode())
# delay
time.sleep(1)
# read the serial port and save the data in a file
file_name = "data.txt"
while True:
    try:
        while ser.in_waiting == 0:
            pass
        line = ser.readline().decode()
        if line != "":
            # print(str(time.time()), line)
            print(line)
            print(line[11:-3])
            print(ascii_to_str(line[11:-3]))
            # f.write(line)
            # f.flush()

    except Exception as e:
        print(e)  # for debugging
        pass

