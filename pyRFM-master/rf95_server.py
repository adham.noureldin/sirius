#!/usb/bin/env python

"""
	waits for incomming message and sends response
"""
__author__ = """Alexander Krause <alexander.krause@ed-solutions.de>"""
__date__ = "2016-12-28"
__version__ = "0.1.0"
__license__ = "GPL"

import sys
import os
import array
import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import lib as pyrfm

conf = {"ll": {"type": "rfm95"}, "pl": {"type": "serial_seed", "port": "COM11"}}
ll = pyrfm.getLL(conf)

binToDec = lambda x: int(x, 2)
binToDec_vec = np.vectorize(binToDec)

print("HW-Version: ", ll.getVersion())
if ll.setOpModeSleep(True, True):
    ll.setFiFo()
    ll.setOpModeIdle()
    ll.setModemConfig("Bw500Cr45Sf128")
    ll.setPreambleLength(8)
    ll.setFrequency(868)
    ll.setTxPower(13)

    while True:
        if ll.waitRX(timeout=3):
            data = ll.recv()
            # data_dec = binToDec_vec(data)
            print("msg: ", data)
            ll.sendStr("Got your message!")
