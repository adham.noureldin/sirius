#define BUFF_SIZE 20
#define HWSERIAL Serial1

uint8_t txBuf[BUFF_SIZE], crc; //array 8 bits
int recState = 0; //State of the camera, 0 if sleep state, 1 if activated

void setup(void) 
{
    Serial.begin(9600);  
    HWSERIAL.begin(115200);
    //give the runcam time to send whatever it does out of reset
    delay(3000);

    //serial command to toggle recording
    //more info on the serial packet structure --> https://support.runcam.com/hc/en-us/articles/360014537794-RunCam-Device-Protocol
    //Request packet structure : Header, Command ID, Action ID, Check code
    txBuf[0] = 0xCC;
    txBuf[1] = 0x01; 
    txBuf[2] = 0x01;  
    txBuf[3] = calcCrc(txBuf, 3);  //compute the CRC
    Serial.println(txBuf[3]);
}

void loop() 
{
    Serial.println("Enter 1 to start recording or 2 to stop recording:");

    while (Serial.available() == 0) {
      Serial.println("Starting not available");
    }

    int userInput = Serial.parseInt();

    if(userInput == 1) 
    {
        Power_on();
    }
    else if(userInput == 2)
    {
        Power_off();
    } 

    delay(500);
}

void Power_on()
{
      if(recState == 0) 
    {
        Serial.println("Starting Recording");
        recState = 1;
        HWSERIAL.write(txBuf, 4);
    }
}

void Power_off()
{
      if(recState == 1) 
    {
        Serial.println("Stopping Recording");
        recState = 0;
        HWSERIAL.write(txBuf, 4);
    }
}


uint8_t calcCrc( uint8_t *buf, uint8_t numBytes )
{
    uint8_t crc = 0;
    for( uint8_t i=0; i<numBytes; i++ )
        crc = crc8_calc( crc, *(buf+i), 0xd5 );
        
    return crc;
    
}//calcCrcTx

//crc : cyclic redundancy check, used to detect ingerence, transmission errors
uint8_t crc8_calc( uint8_t crc, unsigned char a, uint8_t poly )
{
    crc ^= a; //XOR byte to byte
    for (int ii = 0; ii < 8; ++ii) //Boucle sur chaque bit de crc
    {
        if (crc & 0x80)
            crc = (crc << 1) ^ poly; //décalage de bits vers la gauche puis XOR avec poly
        else
            crc = crc << 1; //décalage de bits vers la gauche 
            
    }
    
    return crc;
    
}