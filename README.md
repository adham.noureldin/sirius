# IMU

Cette branche contient le code pour l'intégration des IMUs. Le code s'appuie beaucoup sur la partie IX ("Algorithme de navigation inertielle") du rapport de passations Soft. Il faut donc l'avoir lue pour comprendre le code. Dans le code, les commentaires y renvoient souvent. On le nomme dans la suite "le pdf".

Toutes les fonctions sont des méthodes de la classe IMU, sauf quelques fonctions "utilitaires"
telles que le calcul du produit d'une matrice par un vecteur, par un nombre...

Quand on teste le code, il faut donc donner le nom correspondant à l'IMU qu'on utilise dans la variable "NOM_IMU" (définie dans imu.h).


## Très important

1 - On va utiliser l'extension PlatformIO de vscode qui nous fournit un environnement assez puissant pour travailler aisément sur les cartes arduino. Assurez vous de le bien installer et que ça marche bien avant de continuer.



## Description et oraganisation du code

### SDCardManager [Adham]
La classe SensorDataHandler est l'unité de structure de cette partie. Elle constitue une structure de donée qui stock le fichier binaire associé à un capteur, les deux buffers qui gèrent l'écriture dans le fichier, et elle gère leur utilisation.

Vous n'avez pas à utiliser cette classe du tout, dans la suite, elle ne sert que pour la classe SDCardManager.

La classe SDCardManager gère l'ajout de nouveaux capteurs, ainsi que l'écriture de data dedans. Elle sert donc d'interface, et vous pouvez utiliser ses fonctions.

Attention : le nom d'un capteur est le même que le nom de fichier associé, il doit donc se terminer par ".bin".

À améliorer dans cette partie : 
1 - Écrire des tableaux de bytes et non des bytes dans HandleSensorData, et tester avec des données assez volumineuses (des floats surtout).
2 - Ajouter des interrupts lorsqu'on écrit sur la carte SD.
3 - Faire que la classe SDCardManager soit un singleton (plus de sûreté sur la référence des instances de capteurs et sur le fait qu'on ne fera pas deux instances de la classe sur la même carte par accident)


## Pour commencer à travailler :
1 - Cloner le répertoire chez vous (le <repository_url> vous le trouverez sous clone -> clone with HTTPS):
```
git clone <repository_url>
```

2 - Aller dans le répertoire :
```
cd <repository>
```

3 - Configurer git :
```
git config --global user.name "Your Name"
git config --global user.email "your.email@example.com"
```

4 - Créer une nouvelle branche :
```
git checkout -b name-of-branch
```

5 - Vivre votre vie, faire vos modifications

6 - Mettre vos modifications en action
```
git add .
git commit -m "Your commit message"
git push origin name-of-branch
```

7 - Si vous êtes vraiment sûr de vous, merge vos modifications sur la main branch
```
git checkout main
git pull origin main
git merge name-of-branch
git add .
git merge --continue
git commit -m "Merge branch 'name-of-branch' into main"
git push origin main
```

8 - Pour supprimer votre branche que vous avez crée si elle n'est plus utile :
```
git branch -d your-feature-branch
```


### Quoi après ?

1 - Pour retravailler sur le repo, il est très recommandé de pull toutes les nouvelles modifications
```
git pull origin main
ou
git pull <repository_url>
```
avec main étant le nom du main branch


Je vais laisser le README par défaut de gitlab pour l'instant, mais ça ne va pas tarder à être supprimé.



