#include "carteInfo.h"

CarteInfo::CarteInfo()
{
#ifdef CAMERA_ON
  cam = Camera();
#endif
}

unsigned long long CarteInfo::getMicros()
{
  static unsigned long long time = (unsigned long long)micros();
  static unsigned long long lastCall = micros();
  unsigned long long newCall = micros();

  if (newCall > lastCall)
  {
    time += newCall - lastCall;
  }
  else
  {
    time += newCall + ((1ULL << 32) - 1 - lastCall);
  }
  lastCall = newCall;

  return time;
}

void CarteInfo::superDelay(unsigned long millisDelay)
{
  unsigned long start = millis();

  unsigned long lastUpdateMillis = 0;

  do
  {
    // #ifdef GPS_ON // inutile, le gps a pas besoin d'etre update aussi souvent vu que le serial a un buffer
    //     while (GPSSERIAL.available())
    //     {
    //       gps.Encode();
    //     }
    // #endif

#ifdef ECRAN_ON
    if (millis() > lastUpdateMillis + 20)
    {
      screen.Update();
      lastUpdateMillis = millis();
    }
#endif
  } while (millis() - start < millisDelay);
}

bool CarteInfo::UpdatePressureParachuteFlag()
{
#ifdef BAROMETRE
  if (a_decolle)
  {
    if (!pressureParachuteFlag)
    { // si on n'aa pas encore detecté l'apogée on regarde si on l'a atteint
      pressureParachuteFlag = barometer.PressureHasReachedMinimum();
    } // sinon on ne fait rien
  }
#endif
  return pressureParachuteFlag;
}

void CarteInfo::updateLoRaData()
{
#ifdef LORA_ON
#ifndef IS_BETA
  alpha_lora_count++;
  LoRa_data_alpha_beta.count = alpha_lora_count;
#else
  beta_lora_count++;
  LoRa_data_beta.count = beta_lora_count;
#endif
  LoRa_data_alpha_beta.seq_state = Seq_in.state_seq > -1;
  LoRa_data_alpha_beta.info_state = true;
  LoRa_data_alpha_beta.latitude = fromFloatToUint32_t(gps.GetLatitude());
  LoRa_data_alpha_beta.longitude = fromFloatToUint32_t(gps.GetLongitude());
  LoRa_data_alpha_beta.num_satellites = fromUint4ToUint8(gps.GetSatellites());
  LoRa_data_alpha_beta.gpstime = fromFloatToUint32_t(GetGPSTime());
  LoRa_data_alpha_beta.pressure = fromFloatToUint16_t(GetPressionStatique());
  LoRa_data_alpha_beta.temperature = fromUint7ToUint8(trunc(GetTemperature()));

  if (Seq_in.a_decolle)
  {
    LoRa_data_alpha_beta.time_since_launch = fromUint7ToUint8((millis() - t_millis_decollage) / 1000);
  }
  else
  {
    LoRa_data_alpha_beta.time_since_launch = fromUint7ToUint8(0);
  }
  LoRa_data_alpha_beta.parachute_state = Seq_in.para;

#ifdef IS_BETA
  LoRa_data_beta.count++;
  LoRa_data_beta.separation_state = Seq_in.a_separe;
  LoRa_data_beta.exp_state = Exp_in.state_xp > -1;
  LoRa_data_beta.normAcc = fromFloatToUint16_t(Exp_in.normAcc);
  LoRa_data_beta.normV = fromFloatToUint16_t(Exp_in.normV);
  LoRa_data_beta.q0 = fromFloatToUint16_t(Exp_in.quaternion[0]);
  LoRa_data_beta.q1 = fromFloatToUint16_t(Exp_in.quaternion[1]);
  LoRa_data_beta.q2 = fromFloatToUint16_t(Exp_in.quaternion[2]);
  LoRa_data_beta.q3 = fromFloatToUint16_t(Exp_in.quaternion[3]);
  LoRa_data_beta.px = fromFloatToUint16_t(Exp_in.position[0]);
  LoRa_data_beta.py = fromFloatToUint16_t(Exp_in.position[1]);
  LoRa_data_beta.pz = fromFloatToUint16_t(Exp_in.position[2]);
  LoRa_data_beta.pressure_reservoir = fromUint7ToUint8(trunc(GetPInt() * 10.0));
#endif
#endif // LORA_ON
}

void CarteInfo::updateSendReceiveSerialCommunications(bool now = false)
{
  bool received_seq = ET_Seq_in.receiveData();
  bool received_exp = ET_Exp_in.receiveData(); // affiche alpha avec Exp_in.alpha
  // if(received_exp){
  //   Serial.println(Exp_in.alpha);
  // }
  if (received_seq)
  {

#ifdef IS_BETA
#ifdef LORA_ON
    LoRa_data_beta.allumage_moteur = Seq_in.ordre_maf;
#endif
#endif
    if (!a_decolle && Seq_in.a_decolle)
    {
      t_millis_decollage = millis(); // si on vient de décoller on set le temps de décollage
      a_decolle = true;              // l'etat de décollage est mis à jour une seule fois
      now = true;
      Serial.println("Decollage");
    }
    if (!telemON && Seq_in.telemON)
    {
      telemON = true;
      Serial.println("Telem ON");
#ifdef CAMERA_ON
      cam.Power_on();
#endif
    }
    else if (telemON && !Seq_in.telemON)
    {
      telemON = false;
      Serial.println("Telem OFF");
      if (!a_decolle) // si on a décollé mais que le shunt telem est remis, on considère que c'est un bug donc on n'éteint pas la caméra
      {
#ifdef CAMERA_ON
        cam.Power_off();
#endif
      }
    }
  }
  if (!mafdone && MafDone())
  {
    mafdone = true;
    now = true;
  }
  // seul l'envoie est fait chaque seconde
  if (millis() - lastSerialCommTime > TIME_BETWEEN_SERIAL_COMMUNICATION || now)
  {
    lastSerialCommTime = millis();
    // Serial.println(Seq_in.state_seq);
    // Serial.print("coucuuu ");
    // Serial.println(Exp_in.alpha);

    Seq_out.alphaOk = Exp_in.alphaOk;
    Seq_out.thetaOk = Exp_in.thetaOk;

    if (pint > pintmin)
    {
      Seq_out.pintOK = true;
    }
    else
    {
      Seq_out.pintOK = false;
    }

    // ////// TESTESTSTESTSETESTESTESTSETSETSETSET
    // Seq_out.pintOK = false;

    Seq_out.flag_pression_parachute_beta = UpdatePressureParachuteFlag();
    Seq_out.imus_prets = Exp_in.imus_prets;
    Seq_out.mafdone = mafdone;
    Seq_out.gpsOK = gps.IsOK();
    Exp_out.a_decolle = a_decolle;
    Exp_out.telemON = Seq_in.telemON;

    // Serial.println(Seq_out.imus_prets);

    ET_Seq_out.sendData();
    ET_Exp_out.sendData();
  }
}

void CarteInfo::UpdatePInt()
{
  pint = analogRead(barometer_int_pin) * pression_slope + pression_offset;
  dernieres_mesures_pint[index_derniere_mesure_pint] = pint;
  perte_pression = (pint - dernieres_mesures_pint[(index_derniere_mesure_pint + 1) % 10]);
  index_derniere_mesure_pint = (index_derniere_mesure_pint + 1) % 10;
#ifdef PRINT_PINT
  Serial.print((float)getMicros() / 1000000);
  Serial.print(";");
  Serial.print(GetPInt());
  Serial.print(";");
  Serial.println(GetPertePInt(), 4);
#endif
}

float CarteInfo::GetPInt()
{
  return pint;
  // return 3.6; // juste un example
}

float CarteInfo::GetPertePInt()
{
  return perte_pression;
}

float CarteInfo::GetPressionStatique()
{
#ifdef BAROMETRE
  return barometer.GetPressure();
#endif
}

float CarteInfo::GetTemperature()
{
#ifdef BAROMETRE
  return barometer.GetTemperature();
#endif
}

float CarteInfo::GetPressionAltitude()
{
#ifdef BAROMETRE
  return barometer.GetAltitude();
#endif
}

// float GetPitot();
// double GetLatitude();
// double GetLongitude();
// double GetAltitudeGPS();
// double GetCourse();
// double GetSpeed();
// double GetHDOP();
// uint32_t GetSatellites();
// uint32_t GetGPSTime();

float CarteInfo::GetPitot()
{
#ifdef PITOT_ON
  return pitot.pres_pa();
#else
  return 0;
#endif
}

double CarteInfo::GetLatitude()
{
#ifdef GPS_ON
  return gps.GetLatitude();
#else
  return 0;
#endif
}

double CarteInfo::GetLongitude()
{
#ifdef GPS_ON
  return gps.GetLongitude();
#else
  return 0;
#endif
}

double CarteInfo::GetAltitudeGPS()
{
#ifdef GPS_ON
  return gps.GetAltitude();
#else
  return 0;
#endif
}

double CarteInfo::GetCourse()
{
#ifdef GPS_ON
  return gps.GetCourse();
#else
  return 0;
#endif
}

double CarteInfo::GetSpeed()
{
#ifdef GPS_ON
  return gps.GetSpeed();
#else
  return 0;
#endif
}

double CarteInfo::GetHDOP()
{
#ifdef GPS_ON
  return gps.GetHDOP();
#else
  return 0;
#endif
}

uint32_t CarteInfo::GetSatellites()
{
#ifdef GPS_ON
  return gps.GetSatellites();
#else
  return 0;
#endif
}

uint32_t CarteInfo::GetGPSTime()
{
#ifdef GPS_ON
  return gps.GetTime();
#else
  return 0;
#endif
}

void CarteInfo::SendCommands() // pour les tests
{
  if (Serial.available())
  {
    int commande = Serial.read();
    switch (commande)
    {
    case '0':
      Seq_out.decollage = !Seq_out.decollage;
      Serial.print("Décollage : ");
      Serial.println(Seq_out.decollage);
      break;
    case '1':
      Seq_out.separation = !Seq_out.separation;
      Serial.print("Séparation : ");
      Serial.println(Seq_out.separation);
      break;
    case '2':
      Seq_out.mise_a_feu = !Seq_out.mise_a_feu;
      Serial.print("Mise à feu : ");
      Serial.println(Seq_out.mise_a_feu);
      break;
    case '3':
      Seq_out.ouverture_porte = !Seq_out.ouverture_porte;
      Serial.print("Ouverture porte : ");
      Serial.println(Seq_out.ouverture_porte);
      break;
    case '4':
      Seq_out.au_sol = !Seq_out.au_sol;
      Serial.print("Au sol : ");
      Serial.println(Seq_out.au_sol);
      break;
    }
    ET_Seq_out.sendData();
  }
}

void CarteInfo::Setup()
{
  pinMode(13, OUTPUT); // allumer la led de la teensy pour savoir quand elle est allumée
  digitalWrite(13, HIGH);

  bool camInitSucess = false;
  bool screenInitSuccess = false;
  bool loraInitSuccess = false;
  bool gpsInitSuccess = false;
  bool barometerInitSuccess = false;
  bool pitotInitSuccess = false;
  bool gsmInitSuccess = false;

#ifdef SERIAL_ON
  Serial.begin(115200);
#endif
  EXPSERIAL.begin(4800);
  SEQSERIAL.begin(4800);
  superDelay(1000);
  Serial.println("Serial OK");

#ifdef SDCARD_ON
  char filename[] = "data_info";
  if (!sdcard.Init(sizeof(datrec), true, filename))
  { // try starting SD Card and file system
    // initialize SD Card failed
    Serial.println("SD Card initialization failed");
    sdInitSuccess = false;
  }
  uint32_t bufflen = mydl.InitializeBuffer(sizeof(datrec), SD_SAMPLERATE, SD_BUFFERMSEC, bufferexp);
  uint8_t *bufferend = bufferexp + bufflen;
  Serial.printf("End of buffer at %p\n", bufferend);
  if ((bufflen == 0) || (bufflen > MAXBUFFER))
  {
    Serial.println("Not enough buffer space!  Reduce buffer time or sample rate.");
    sdInitSuccess = false;
  }
  if (sdInitSuccess)
  {
    mydl.AttachCollector(&myCollector);
    mydl.AttachDisplay(&myBinaryDisplay, 5000);
    Serial.println("SD Card initialized.");
  }
#endif

#ifdef CAMERA_ON
  Serial5.setRX(20);
  Serial5.setTX(21);
  camInitSucess = cam.Init(&Serial5, 115200);
  Serial.println("Cameras initialized");
  Serial.print("Camera 1: ");
  Serial.println(camInitSucess);
#endif
#ifdef ECRAN_ON
  screenInitSuccess = screen.Init();
#endif
#ifdef LORA_ON
#ifdef IS_BETA
  loraInitSuccess = Lora_emitter.Init(115200, 868.438);
#else
  loraInitSuccess = Lora_emitter.Init(115200, 868.313);
#endif
  delay(100);
  // Serial.println();
  // Lora_emitter.Update();
  Serial.println("lora setup done");
  Serial.println("LoRa Mode : ");
  Serial.println(Lora_emitter.get_mode());
#endif
#ifdef GPS_ON
  gpsInitSuccess = gps.Init(&Serial8, 9600);
#endif
#ifdef BAROMETRE
  barometerInitSuccess = barometer.Init();
  Seq_out.barometreOK = barometerInitSuccess;
#endif
#ifdef PITOT_ON
  pitotInitSuccess = pitot.Init();
#endif
// on init le gsm seulement après le vol pour pas emmetre avant et aussi se mettre en communication avec la bonne antenne gsm
// #ifdef GSM_ON
//   gsmInitSuccess = gsm.Init();
//   t_dernier_sms_millis = getMicros();
// #endif
#ifdef ECRAN_ON

  std::vector<std::pair<String, bool>> initStatus;
  initStatus.push_back(std::make_pair(" Cam: ", camInitSucess));
  initStatus.push_back(std::make_pair(" LoRa: ", loraInitSuccess));
  initStatus.push_back(std::make_pair(" GPS: ", gpsInitSuccess));
  initStatus.push_back(std::make_pair(" Barometer: ", barometerInitSuccess));
  initStatus.push_back(std::make_pair(" Pitot: ", pitotInitSuccess));
  initStatus.push_back(std::make_pair(" GSM: ", gsmInitSuccess));
  initStatus.push_back(std::make_pair(" SD: ", sdInitSuccess));
  screen.SetInitTextToPrint(initStatus);
#endif

  // Baromètre intérieur
  pinMode(barometer_int_pin, INPUT);
  // initialise les communications serial avec les autres cartes (Seq et Exp)
  // ET_Seq_in.begin(details(Seq_in), &SEQSERIAL);
  ET_Seq_in.begin(details(Seq_in), &SEQSERIAL);
  ET_Seq_out.begin(details(Seq_out), &SEQSERIAL);
  ET_Exp_in.begin(details(Exp_in), &EXPSERIAL);
  ET_Exp_out.begin(details(Exp_out), &EXPSERIAL);
  delay(1000);
  Serial.println("Carte info setup done");
}

/*void CarteInfo::OldLoop()
{
  // mySerial.print("AT+");
  // mySerial.write("AT+TEST=TXLRPKT, 00 AA 11 BB 22 CC");
  // delay(2000);
  updateSendReceiveSerialCommunications();

#ifdef ECRAN_ON

  String etat = "None";
  //Serial.print("state seq ");
  //Serial.println(Seq_in.state_seq);
  switch (Seq_in.state_seq)
  {
  case 0:
    etat = "On the ground";
    break;
  case 1:
    etat = "Took off";
    break;
  case 2:
    etat = "Separation order";
    break;
  case 3:
    etat = "Separation effective";
    break;
  case 4:
    etat = "Separation not effective";
    break;
  case 5:
    etat = "Ignition";
    break;
  case 6:
    etat = "Parachute deployed";
    break;
  case 7:
    etat = "Landed";
    break;
  case 8:
    etat = "No ignition";
    break;
  }


  screen.SetLoopTextToPrint(
      String(" Pint: ") + String(GetPInt()) + String("\n Alpha: ") + String(RAD_TO_DEG * Exp_in.alpha) + String("\n Theta: ") + String(RAD_TO_DEG * Exp_in.theta) + String("\n Etat: ") + String(etat));
  screen.Update();


#endif

#ifdef LORA_ON
  if (millis() - t_last_lora_send_alpha_beta > TIME_BETWEEN_LORA_SEND)
  {
    updateLoRaData();
    t_last_lora_send_alpha_beta = millis();
    Lora_emitter.send_data((uint8_t *)&LoRa_data_alpha_beta, sizeof(LoRa_data_alpha_beta));
    Serial.println();
    Lora_emitter.Update();
#ifdef SERIAL_ON
    Serial.println();
    Serial.print("Size : ");
    Serial.println(2 * sizeof(LoRa_data_beta) + 1);
    Serial.println("Data sent");
    Lora_emitter.Update();
#endif
  }
#ifdef IS_BETA
  if (millis() - t_last_lora_send_beta > TIME_BETWEEN_LORA_SEND && millis() - t_last_lora_send_alpha_beta > TIME_BETWEEN_LORA_SEND / 2)
  {
    t_last_lora_send_beta = millis();
    Lora_emitter.send_data((uint8_t *)&LoRa_data_beta, sizeof(LoRa_data_beta));
    Serial.println();
    Lora_emitter.Update();
#ifdef SERIAL_ON
    Serial.println();
    Serial.print("Size : ");
    Serial.println(2 * sizeof(LoRa_data_beta) + 1);
    Serial.println("Data sent");
    Lora_emitter.Update();
#endif
  }
#endif

#endif

#ifdef GPS_ON
  gps.Update();
#ifdef SERIAL_ON
  // Serial.print("Latitude : ");
  // Serial.print(gps.GetLatitude(), 6);
  // Serial.print(" - Longitude : ");
  // Serial.print(gps.GetLongitude(), 6);
  // Serial.print(" - Satellites : ");
  // Serial.print(gps.GetSatellites());
  // Serial.print(" - Temps : ");
  // Serial.print(gps.GetTime());
  // Serial.println();
#endif
#endif

#ifdef BAROMETRE
  barometer.Update();
#endif

#ifdef GSM_ON
  if (getMicros() - t_dernier_sms_millis > TIME_BETWEEN_GSM_SEND)
  {
    t_dernier_sms_millis = getMicros();
    gsm.SendSMS("Hello");
  }
  gsm.PrintSignalStrength();
#endif
#ifdef PITOT_ON
  if (pitot.Read())
  {
    Serial.print("Pression : ");
    Serial.print(pitot.pres_pa(), 6);
    Serial.print(" - Vitesse : ");
    Serial.println(pitot.vitesse_ms(), 6);
  }
#endif
#ifdef PINT_ON
  UpdatePInt();
#endif

  SendCommands();

#ifdef CAMERA_ON
  if (Seq_out.decollage)
  {
    cam.Power_on();
    // Serial.println("Camera power on");
  }
  if (Seq_out.au_sol || (!Seq_out.decollage))
  {
    cam.Power_off();
    // Serial.println("Camera power off");
  }
#endif
}

*/

void CarteInfo::EmittingThings()
{
  if (!veille)
  {
#ifdef LORA_ON
    if (millis() - t_last_lora_send_alpha_beta > TIME_BETWEEN_LORA_SEND)
    {
      updateLoRaData();
      t_last_lora_send_alpha_beta = millis();
      Lora_emitter.send_data((uint8_t *)&LoRa_data_alpha_beta, sizeof(LoRa_data_alpha_beta));
      Serial.println();
      Lora_emitter.Update();
#ifdef SERIAL_ON
      Serial.println();
      Serial.print("Size : ");
      Serial.println(2 * sizeof(LoRa_data_beta) + 1);
      Serial.println("Data sent");
      Lora_emitter.Update();
#endif
    }
#ifdef IS_BETA
    if (millis() - t_last_lora_send_beta > TIME_BETWEEN_LORA_SEND && millis() - t_last_lora_send_alpha_beta > TIME_BETWEEN_LORA_SEND / 2)
    {
      t_last_lora_send_beta = millis();
      Lora_emitter.send_data((uint8_t *)&LoRa_data_beta, sizeof(LoRa_data_beta));
      Serial.println();
      Lora_emitter.Update();
#ifdef SERIAL_ON
      Serial.println();
      Serial.print("Size : ");
      Serial.println(2 * sizeof(LoRa_data_beta) + 1);
      Serial.println("Data sent");
      Lora_emitter.Update();
#endif
    }
#endif
#endif
  }
  if (veille) // pour les tests
  {
#ifdef LORA_ON
    if (millis() - t_last_lora_send_alpha_beta > TIME_BETWEEN_LORA_SEND)
    {
      // update LoRa_data_recuperation
      LoRa_data_recuperation.num_satellites = fromUint4ToUint8(gps.GetSatellites());
      LoRa_data_recuperation.gpstime = fromFloatToUint32_t(GetGPSTime());
      LoRa_data_recuperation.latitude = fromFloatToUint32_t(gps.GetLatitude());
      LoRa_data_recuperation.longitude = fromFloatToUint32_t(gps.GetLongitude());
      t_last_lora_send_alpha_beta = millis();
      Lora_emitter.send_data((uint8_t *)&LoRa_data_recuperation, sizeof(LoRa_data_recuperation));
      Serial.println();
      Lora_emitter.Update();
    }
#endif
#ifdef GSM_ON
    if (millis() - t_dernier_sms_millis > TIME_BETWEEN_GSM_SEND)
    {
      t_dernier_sms_millis = millis();
      // gsm.SendSMS("Hello");
      // send sms with gps coordinates
      char message[100];
#ifdef GPS_ON
      sprintf(message, "Satellites : %d, Latitude : %f, Longitude : %f", gps.GetSatellites(), gps.GetLatitude(), gps.GetLongitude());
#endif
      gsm.SendSMS(message);
    }
    // gsm.PrintSignalStrength();
#endif
  }
}

float calculateTheta(float *q)
{
  float qi = q[0];
  float qj = q[1];
  return M_PI_2 - acos(1.0 - 2.0 * (qi * qi + qj * qj));
}

// Calcul de l'azimuth
void CarteInfo::calculateAlpha(float *q)
{
  float qi = q[0];
  float qj = q[1];
  float qk = q[2];
  float qw = q[3];
  float y = -2.0 * (qj * qw + qi * qk);
  float x = 2.0 * (qi * qw - qj * qk);
  alpha = atan2(y, x);
}

float calculate_continuous_azimuth(float previous_azimuth, float new_azimuth)
{
  float delta = new_azimuth - previous_azimuth;

  // Adjust delta to the range [-180, 180] to handle wrap-around
  if (delta > M_PI)
  {
    delta -= M_TWOPI;
  }
  else if (delta < -M_PI)
  {
    delta += M_TWOPI;
  }

  // Update the continuous azimuth
  return previous_azimuth + delta;
}

void CarteInfo::Loop()
{ // Mise en veille
  if (!veille && millis() - t_millis_decollage > t_millis_veille && a_decolle)
  {
    Serial.println("Veille");
    veille = true;
#ifdef CAMERA_ON
    cam.Power_off();
#endif
#ifdef ECRAN_ON
    screen.TurnOff();
#endif
#ifdef SDCARD_ON
    sdcard.QuitLogging();
    SD.sdfs.end();
#endif
#ifdef BAROMETRE
    barometer.shutdown();
#endif
    EXPSERIAL.end();
    SEQSERIAL.end();
#ifdef GPS_ON
    gps.set_mode_sol();
#endif
#ifdef GSM_ON
    gsm.Init();
    t_dernier_sms_millis = millis();
#endif
    Wire2.end(); // pitot
#ifdef IS_BETA
    LoraSerial.write("AT+TEST=RFCFG,868.438,SF12,125,8,8,14,ON,OFF,OFF\n"); // range maximum
#else
    LoraSerial.write("AT+TEST=RFCFG,868.313,SF12,125,8,8,14,ON,OFF,OFF\n"); // range maximum
#endif
    delay(200);
  }
  if (!veille)
  {
#ifdef CAMERA_ON
    if (digitalRead(switchpin) == HIGH)
    {
      cam.Power_off();
      // Serial.println("Cam off");
    }
#endif
#ifdef GPS_ON
    gps.Update();
#endif
    updateSendReceiveSerialCommunications(false);
#ifdef SDCARD_ON
    if (sdInitSuccess)
    {
      if (!stocking)
      {
        stocking = true;
        // mydl.StartLogger("data_exp1.bin", 1000, &LoggerISR);
        sdcard.StartLogging();
      }
      if (a_decolle)
      {
        sdcard.CheckLogger(false);
      }
      else
      {
        sdcard.CheckLogger(true);
      }
    }
#endif
    if (Seq_in.telemON || a_decolle)
    {
      EmittingThings();
    }
#ifdef ECRAN_ON

    String etat = "None";
    switch (Seq_in.state_seq)
    {
    case 0:
      etat = "Au sol";
      break;
    case 1:
      etat = "A décollé";
      break;
    case 2:
      etat = "Ordre séparation";
      break;
    case 3:
      etat = "Séparation effective";
      break;
    case 4:
      etat = "Séparation non effective";
      break;
    case 5:
      etat = "Ordre de mise à feu";
      break;
    case 6:
      etat = "Mise à feu";
      break;
    case 7:
      etat = "Para déployé";
      break;
    case 8:
      etat = "A atterri";
      break;
    case 9:
      etat = "Non feu";
      break;
    }
    float last_azimuth = alpha;
    calculateAlpha(Exp_in.quaternion);
    float continuous_alpha = calculate_continuous_azimuth(last_azimuth, alpha);
    alpha = continuous_alpha;
    screen.SetLoopTextToPrint(
        String(" Pint: ") + String(GetPInt()) + String("\n Alpha: ") + String(RAD_TO_DEG * continuous_alpha) + String("\n Theta: ") + String(RAD_TO_DEG * calculateTheta(Exp_in.quaternion)) + String("\n Nb sat: ") + String(gps.GetSatellites()) + String("\n Temps GPS: ") + String(gps.GetTime()));
    // String("\n Long/Lat: ") + String(gps.GetLongitude()) + String("/") + String(gps.GetLatitude()) + String("\n t GPS: ") + String(gps.GetTime()) + String("\n Etat: ") + etat
    screen.Update();

#endif
    // Pour les tests
    SendCommands();
  }
  if (veille)
  {
    EmittingThings();
#ifdef GPS_ON
    gps.Update();
#endif
  }
}

void CarteInfo::updateSensors()
{
#ifdef BAROMETRE
  barometer.Update();
#endif

#ifdef GPS_ON
  gps.Update();
// #ifdef SERIAL_ON
// Serial.print("Latitude : ");
// Serial.print(gps.GetLatitude(), 6);
// Serial.print(" - Longitude : ");
// Serial.print(gps.GetLongitude(), 6);
// Serial.print(" - Satellites : ");
// Serial.print(gps.GetSatellites());
// Serial.print(" - Temps : ");
// Serial.print(gps.GetTime());
// Serial.println();
// #endif
#endif

#ifdef PITOT_ON
  // if (pitot.Read())
  // {
  //   // Serial.print("Pression : ");
  //   // Serial.print(pitot.pres_pa(), 6);
  //   // Serial.print(" - Vitesse : ");
  //   // Serial.println(pitot.vitesse_ms(), 6);
  // }
#endif
#ifdef PINT_ON
  UpdatePInt();
#endif
}

void splitDoubleToFloats(double value, volatile float &float1, volatile float &float2)
{
  // Obtain the byte representation of the double value
  byte *byteArr = (byte *)&value;

  // Extract the bytes for the first float (4 bytes)
  byte float1Bytes[4];
  for (int i = 0; i < 4; i++)
  {
    float1Bytes[i] = byteArr[i];
  }

  // Extract the bytes for the second float (4 bytes)
  byte float2Bytes[4];
  for (int i = 0; i < 4; i++)
  {
    float2Bytes[i] = byteArr[i + 4];
  }

  // Reassemble the bytes into floats
  float1 = *((float *)float1Bytes);
  float2 = *((float *)float2Bytes);
}

bool CarteInfo::MafDone()
{
#ifdef BAROMETRE
  return (barometer.GetPressure() > seuil_pression_maf);
#else
  return true;
#endif
}

bool CarteInfo::ApogeeDetectee()
{
  return pressureParachuteFlag;
}