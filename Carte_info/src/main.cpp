#include <Arduino.h>
#include "carteInfo.h"

CarteInfo carteInfo;

#ifdef SDCARD_ON
#define MAXCHUNKS 3 // c'est sensé être définie dans la classe DataLogger, mais je le définie aussi pour pouvoir le changer dans info et expe
DataLogger mydl(MAXCHUNKS);
void LoggerISR(void)
{ // call the data logger collection ISR handler
  // Serial.println("here logger isr");
  mydl.TimerChore();
}
uint8_t bufferexp[MAXBUFFER] DMAMEM;

void myCollector(void *vdp)
{
  // elapsedMicros em = 0;
  // Serial.println("here collector");
  volatile struct datrec *dp;
  dp = (volatile struct datrec *)vdp;
  dp->microstime = micros();
  carteInfo.updateSensors();
  dp->pression = carteInfo.GetPressionStatique();
  dp->temperature = carteInfo.GetTemperature();
  dp->altitude = carteInfo.GetPressionAltitude();
  dp->pint = carteInfo.GetPInt();
  dp->pitot = carteInfo.GetPitot();
  splitDoubleToFloats(carteInfo.GetLatitude(), dp->lat1, dp->lat2);
  splitDoubleToFloats(carteInfo.GetLongitude(), dp->lon1, dp->lon2);
  splitDoubleToFloats(carteInfo.GetAltitudeGPS(), dp->alt1, dp->alt2);
  splitDoubleToFloats(carteInfo.GetCourse(), dp->course1, dp->course2);
  splitDoubleToFloats(carteInfo.GetSpeed(), dp->speed1, dp->speed2);
  splitDoubleToFloats(carteInfo.GetHDOP(), dp->hdop1, dp->hdop2);
  dp->satellites = carteInfo.GetSatellites();
  dp->gpstime = carteInfo.GetGPSTime();
  dp->apogee_detectee = carteInfo.ApogeeDetectee();
}

void myBinaryDisplay(void *vdp)
{
  Serial.println("here binary display");
  struct datrec *dp;
  dp = (struct datrec *)vdp;
  // TLoggerStat *tsp;
  // tsp =  mydl.GetStatus(); // updates values collected at interrupt time

  Serial.printf("%8d,  ", dp->microstime);
  // Serial.println(expe.GetTheta9());
}

#endif

void setup()
{
  carteInfo.Setup();
}

void loop()
{
  carteInfo.Loop();
}

// /*
//   Software serial multple serial test

//  Receives from the hardware serial, sends to software serial.
//  Receives from software serial, sends to hardware serial.

//  The circuit:
//  * RX is digital pin 7 (connect to TX of other device)
//  * TX is digital pin 8 (connect to RX of other device)

//  created back in the mists of time
//  modified 25 May 2012
//  by Tom Igoe
//  based on Mikal Hart's example

//  This example code is in the public domain.
//  */

// //#include <SoftwareSerial.h>
// #include <Arduino.h>
// // Best for Teensy LC & 3.2
// //SoftwareSerial mySerial(0, 1); // RX,TX
// //SoftwareSerial mySerial(7, 8);
// //SoftwareSerial mySerial(9, 10);
// #define mySerial Serial3
// // Best for Teensy 3.5 & 3.6
// //SoftwareSerial mySerial(0, 1); // RX,TX
// //SoftwareSerial mySerial(7, 8);
// //SoftwareSerial mySerial(9, 10);
// //SoftwareSerial mySerial(31, 32);
// //SoftwareSerial mySerial(34, 33);
// //SoftwareSerial mySerial(47, 48);

// // Best for Teensy 4.1
// //SoftwareSerial mySerial(0, 1); // RX,TX
// //SoftwareSerial mySerial(7, 8);
// //SoftwareSerial mySerial(15, 14);
// //SoftwareSerial mySerial(16, 17);
// //SoftwareSerial mySerial(21, 20);
// //SoftwareSerial mySerial(25, 24);
// //SoftwareSerial mySerial(28, 29);
// //SoftwareSerial mySerial(34, 35);

// void setup()
// {
//   // Open serial communications and wait for port to open:
//   Serial.begin(115200);
//   while (!Serial) {
//     ; // wait for serial port to connect. Needed for Leonardo only
//   }

//   //Serial.println("Goodnight moon!");

//   // set the data rate for the SoftwareSerial port
//   mySerial.begin(115200);
//   //mySerial.println("Hello, world?");
// }

// void loop() // run over and over
// {
//   if (mySerial.available())
//     Serial.write(mySerial.read());
//   if (Serial.available())
//     mySerial.write(Serial.read());
// }