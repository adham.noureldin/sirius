#include <Arduino.h>

#define IS_BETA

#define GPS_ON
#define LORA_ON
#define GSM_ON
#define PITOT_ON
#define BAROMETRE
#define SERIAL_ON
#define SDCARD_ON

#ifndef IS_BETA
#define CAMERA_ON
#else
#define ECRAN_ON
#define PINT_ON
#endif

// #define PRINT_PINT // Pour Nicolas

#ifdef ECRAN_ON
#define TFT_MISO 12
#define TFT_LED 30
#define TFT_SCK 13
#define TFT_MOSI 11
#define TFT_DC 26
#define TFT_RESET 27
#define TFT_CS 10

// Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCK, TFT_RESET, TFT_MISO);
#endif

#define EXPSERIAL Serial5
#define SEQSERIAL Serial7

#define TIME_BETWEEN_SERIAL_COMMUNICATION 1000 // send and receive message every 1 s // on devrait calculer jusqu'à quelle délais on peut descendre
#define TIME_BETWEEN_LORA_SEND 2000            // send message every 2 s
#define TIME_BETWEEN_GSM_SEND 1 * 60 * 1000    // send message every minute