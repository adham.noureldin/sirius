#include "defines.h"
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <EasyTransfer.h>

#include "GPS.h"
#include "screen.h"
#include "barometer.h"
#include "LoRa.h"
#include "gsm.h"
#include "Pitot.h"
#include "InternCom.h"
#include "Camera.h"

#define SD_SAMPLERATE 150.0
#define SD_BUFFERMSEC 2560.0
#define MAXBUFFER 40720

#include "SDCard.h" // important que ce soit après la def de MAXCHUNKS

void splitDoubleToFloats(double value, volatile float &float1, volatile float &float2);

#ifdef SDCARD_ON
extern void myCollector(void *vdp);
extern void myBinaryDisplay(void *vdp);
extern void LoggerISR(void);
extern DataLogger mydl;
extern uint8_t bufferexp[MAXBUFFER];

struct datrec
{
    uint32_t microstime;
    float pression;
    float temperature;
    float altitude;
    float pint;
    float pitot;
    // GPS ...
    float lat1; // two floats to store in double precision.
    float lat2;
    float lon1;
    float lon2;
    float alt1;
    float alt2;
    float course1;
    float course2;
    float speed1;
    float speed2;
    float hdop1;
    float hdop2;
    uint32_t satellites;
    uint32_t gpstime;
    uint32_t apogee_detectee;
};
#endif
// int msg = -1;
// struct __attribute__((packed)) LoRa_data_beta
// {                   // ne pas oublier d'utiliser les fonctions de conversion voir LoRa.h
//                     // uint8_t padding:6;
//     uint8_t AB : 4; // envoyer 1111 pour beta et 0000 pour alpha. permet de les distinguer et verifier si le packet est corrompu
//     uint8_t separation_state : 1;
//     uint8_t seq_state_beta : 1;
//     uint8_t info_state_beta : 1;
//     uint8_t exp_state_beta : 1;
//     uint16_t normAcc : 16;              // moyénné sur 1s par exemple ou moins (convertis de float en float16 puis cast en uint16_t)
//     uint16_t normV : 16;                // grâce à l'integration des imus (convertis de float en float16 puis cast en uint16_t)
//     uint16_t q0 : 16;                   // possible plutot d'envoyer des angles d'euler, ce qui fait economiser 16bits mais possible perte en precision
//     uint16_t q1 : 16;                   // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
//     uint16_t q2 : 16;                   // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
//     uint16_t q3 : 16;                   // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
//     uint16_t px : 16;                   // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
//     uint16_t py : 16;                   // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
//     uint16_t pz : 16;                   // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
//     uint16_t pressure : 16;             // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
//     uint16_t pressure_reservoir : 16;   // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
//     uint32_t latitude_beta : 32;        // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
//     uint32_t longitude_beta : 32;       // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
//     uint8_t num_satellites_beta : 4;    // between 0 and 15
//     uint8_t time_since_launch_beta : 7; // between 0s to 127s
//     uint8_t temperature : 7;            // uint between 0 and 127
//     bool parachute_state : 1;

// }; // mettre SF10 et 125kHz de bande passante et un envoie par seconde

// struct __attribute__((packed)) LoRa_data_alpha
// {
//     uint8_t AB : 4; // envoyer 1111 pour beta et 0000 pour alpha. permet de les distinguer et verifier si le packet est corrompu
//     uint8_t seq_state_alpha : 1;
//     uint8_t info_state_alpha : 1;
//     uint32_t latitude_alpha : 32;        // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
//     uint32_t longitude_alpha : 32;       // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
//     uint8_t num_satellites_alpha : 4;    // between 0 and 15
//     uint8_t time_since_launch_alpha : 7; // between 0s to 127s
//     bool parachute_state : 1;
// }; // mettre SF10 et 125kHz de bande passante et un envoie par seconde

struct __attribute__((packed)) LoRa_data_beta // pour beta seulement
{                                             // ne pas oublier d'utiliser les fonctions de conversion voir LoRa.h
                                              // uint8_t padding:6;
    uint8_t count : 4;                        // permet de savoir exactement quels packets on a reçu
    uint8_t separation_state : 1;
    uint8_t exp_state : 1;
    uint16_t normAcc : 16;          // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t normV : 16;            // grâce à l'integration des imus (convertis de float en float16 puis cast en uint16_t)
    uint16_t q0 : 16;               // possible plutot d'envoyer des angles d'euler, ce qui fait economiser 16bits mais possible perte en precision
    uint16_t q1 : 16;               // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t q2 : 16;               // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t q3 : 16;               // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t px : 16;               // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t py : 16;               // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t pz : 16;               // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint8_t pressure_reservoir : 7; // uint between 0 and 127 (=58 -> 5.8 bar)
    bool allumage_moteur : 1;
}; // mettre SF10 et 125kHz de bande passante et un envoie par seconde

struct __attribute__((packed)) LoRa_data_alpha_beta // pour les deux
{
    uint8_t count : 4; // permet de savoir exactement quels packets on a reçu
    uint8_t seq_state : 1;
    uint8_t info_state : 1;
    uint32_t latitude : 32;        // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint32_t longitude : 32;       // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint8_t num_satellites : 4;    // between 0 and 15
    uint32_t gpstime : 32;         // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint8_t time_since_launch : 7; // between 0s to 127s
    uint16_t pressure : 16;        // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint8_t temperature : 7;       // uint between 0 and 127
    bool parachute_state : 1;
#ifndef IS_BETA
    uint8_t padding_to_discern_alpha_beta : 8; // la taille des messages sera differents pour alpha et beta
#endif
}; // mettre SF10 et 125kHz de bande passante et un envoie par seconde

struct __attribute__((packed)) LoRa_data_recuperation
{                               // ne pas oublier d'utiliser les fonctions de conversion voir LoRa.h
    uint8_t num_satellites : 4; // between 0 and 15
    uint32_t gpstime : 32;      // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint32_t latitude : 32;     // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint32_t longitude : 32;    // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
#ifndef IS_BETA
    uint8_t padding_to_discern_alpha_beta : 8; // la taille des messages sera differents pour alpha et beta
#endif
}; // mettre SF10 et 125kHz de bande passante et un envoie par seconde

// function to convert a bitfield to a hex string
void toHex(uint8_t *data, size_t len, char *out);

// Infos en plus de la station sol et qu'on peut déduire :
// altitude pression, vitesse de chute sous parachut avec la pression
// temps de reception de chaque message

class CarteInfo
{
#ifdef SDCARD_ON
    SDCard sdcard = SDCard(SD_SAMPLERATE, SD_BUFFERMSEC);
    bool stocking = false; // indique si on est en train de stocker les données sur la carte SD
#endif
    bool sdInitSuccess = true;

    // Pression système pneumatique
    const int switchpin = 5;
    const int barometer_int_pin = 19;                 // pin du baromètre du système pneumatique
    const float pression_slope = 0.03156588964121951; // Seuil de pression en dessous duquel on déclenche le parachute
    const float pression_offset = -4.729308651638099;
    float pint = 0;                              // Pression dans le système pneumatique
    float perte_pression = 0;                    // perte de pression par seconde
    long unsigned int temps_update_pression = 0; // temps de la dernière mesure de pression
    long unsigned int temps_entre_2_mesures_de_pression = 6000000;
    float dernieres_mesures_pint[10] = {};
    int index_derniere_mesure_pint = 0;
    float pintmin = 5; // pression minimale pour le décollage
#ifdef CAMERA_ON
    Camera cam;
#endif
#ifdef LORA_ON
    const int Lora_baud = 115200;
    Lora Lora_emitter;
    unsigned long t_last_lora_send_alpha_beta = 0; // in ms
#ifdef IS_BETA
    unsigned long t_last_lora_send_beta = 0; // in ms
#endif

    LoRa_data_alpha_beta LoRa_data_alpha_beta;
#ifdef IS_BETA
    LoRa_data_beta LoRa_data_beta;
#endif
    LoRa_data_recuperation LoRa_data_recuperation;
#endif

#ifdef GPS_ON
    GPS gps;
#endif

#ifdef ECRAN_ON
    Screen screen;
#endif

#ifdef BAROMETRE
    Barometer barometer;
    float seuil_pression_maf = 800; // Seuil de pression au-dessus duquel on considère que la mise à feu du moteur beta a eu lieu
#endif
#ifdef GSM_ON
    GSM gsm;
    unsigned long t_dernier_sms_millis; // Pour les tests
#endif
#ifdef PITOT_ON
    Pitot pitot;
#endif

    EasyTransfer ET_Seq_in, ET_Seq_out, ET_Exp_in, ET_Exp_out;
    Seq_to_info_struct Seq_in;  // structure qu'on recoit de Seq
    Info_to_seq_struct Seq_out; // structure qu'on envoie à Seq
    XP_to_info_struct Exp_in;   // structure qu'on recoit de XP
    Info_to_xp_struct Exp_out;  // structure qu'on envoie à XP
    uint32_t lastSerialCommTime = 0;

    bool pressureParachuteFlag = false;

    bool a_decolle = false;
    uint32_t t_millis_decollage = 0; // sert à mettre en veille la carte après un certain temps apres le décollage
    uint32_t t_millis_veille = 60000;

    bool telemON = true;
    bool veille = false;

    uint8_t alpha_lora_count = 0;
    uint8_t beta_lora_count = 0;

    bool mafdone = false;

    float alpha = 0;

public:
    CarteInfo();
    unsigned long long getMicros();
    void updateSendReceiveSerialCommunications(bool now = false);
    bool UpdatePressureParachuteFlag();
    void updateLoRaData();
    void superDelay(unsigned long microDelay);
    void UpdatePInt();
    float GetPInt();
    float GetPertePInt();
    float GetPressionStatique();
    float GetPressionAltitude();
    float GetTemperature();
    float GetPitot();
    double GetLatitude();
    double GetLongitude();
    double GetAltitudeGPS();
    double GetCourse();
    double GetSpeed();
    double GetHDOP();
    uint32_t GetSatellites();
    uint32_t GetGPSTime();
    void SendCommands();
    void Setup();
    void OldLoop();

    void Loop();
    void EmittingThings();

    void updateSensors();
    bool MafDone();

    void calculateAlpha(float *q);
    bool ApogeeDetectee();
};
