#define BAROMETER_PIN 19

float pth[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
float pmes[10] = {182, 214, 246, 275, 305, 342, 370, 405, 435};

float Calib(float p)
{
  float pcal = 0;
  for (int i = 0; i < 9; i++)
  {
    if (p >= pmes[i] && p <= pmes[i + 1])
    {
      pcal = pth[i] + (p - pmes[i]) * (pth[i + 1] - pth[i]) / (pmes[i + 1] - pmes[i]);
    }
  }
  return pcal;
}

void setup()
{
  Serial.begin(9600);
  delay(1000);
  pinMode(BAROMETER_PIN, INPUT);
}

void loop()
{
  Serial.println(Calib(analogRead(BAROMETER_PIN)));
}
