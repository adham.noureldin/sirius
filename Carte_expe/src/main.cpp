#include "Expe.h"
#ifdef MICRO_ON
AudioInputI2S_F32 i2s1;      // xy=231.25,278.25
AudioRecordQueue_F32 queue1; // xy=471.25,262.25
AudioRecordQueue_F32 queue2; // xy=483.25,306.25
AudioConnection_F32 patchCord1(i2s1, 0, queue1, 0);
AudioConnection_F32 patchCord2(i2s1, 1, queue2, 0);

Expe expe(&queue1, &queue2);
#else
Expe expe;
#endif

#ifdef SDCARD_ON
#define MAXCHUNKS 12
DataLogger mydl(MAXCHUNKS);
void LoggerISR(void)
{ // call the data logger collection ISR handler
  // Serial.println("here logger isr");
  mydl.TimerChore();
}
uint8_t bufferexp[MAXBUFFER] DMAMEM;
// uint8_t bufferexp[MAXBUFFER];

void myCollector(void *vdp)
{
  // elapsedMicros em = 0;
  // Serial.println("here collector");
  volatile struct datrec *dp;
  dp = (volatile struct datrec *)vdp;
  dp->microstime = micros();
  expe.ActualiserDonnees();
  dp->acc9[0] = expe.GetAcc9()[0];
  dp->acc9[1] = expe.GetAcc9()[1];
  dp->acc9[2] = expe.GetAcc9()[2];
  dp->gyr9[0] = expe.GetGyr9()[0];
  dp->gyr9[1] = expe.GetGyr9()[1];
  dp->gyr9[2] = expe.GetGyr9()[2];
  dp->acc37[0] = expe.GetAcc37()[0];
  dp->acc37[1] = expe.GetAcc37()[1];
  dp->acc37[2] = expe.GetAcc37()[2];
  dp->gyr37[0] = expe.GetGyr37()[0];
  dp->gyr37[1] = expe.GetGyr37()[1];
  dp->gyr37[2] = expe.GetGyr37()[2];
  dp->quat9[0] = expe.GetQuat9()[0];
  dp->quat9[1] = expe.GetQuat9()[1];
  dp->quat9[2] = expe.GetQuat9()[2];
  dp->quat9[3] = expe.GetQuat9()[3];
  // dp->quat37[0] = expe.GetQuat37()[0];
  // dp->quat37[1] = expe.GetQuat37()[1];
  // dp->quat37[2] = expe.GetQuat37()[2];
  // dp->quat37[3] = expe.GetQuat37()[3];
  dp->vit9[0] = expe.GetVit9()[0];
  dp->vit9[1] = expe.GetVit9()[1];
  dp->vit9[2] = expe.GetVit9()[2];
  // dp->vit37[0] = expe.GetVit37()[0];
  // dp->vit37[1] = expe.GetVit37()[1];
  // dp->vit37[2] = expe.GetVit37()[2];
  dp->pos9[0] = expe.GetPos9()[0];
  dp->pos9[1] = expe.GetPos9()[1];
  dp->pos9[2] = expe.GetPos9()[2];
  // dp->pos37[0] = expe.GetPos37()[0];
  // dp->pos37[1] = expe.GetPos37()[1];
  // dp->pos37[2] = expe.GetPos37()[2];
  // Serial.printf("millis: %lu, data1: %f, data2: %f\n", dp->millistime, dp->data1, dp->data2);
  // Serial.println(em);
}

void myBinaryDisplay(void *vdp)
{
  // Serial.println("here binary display");
  struct datrec *dp;
  dp = (struct datrec *)vdp;
  // TLoggerStat *tsp;
  // tsp =  mydl.GetStatus(); // updates values collected at interrupt time

  // Serial.printf("%8d,  ", dp->microstime);
  // Serial.printf("gyro : %f, %f, %f", expe.GetGyr9()[0], expe.GetGyr9()[1], expe.GetGyr9()[2]);
  Serial.printf("alpha: %f\n", expe.GetAlpha9() * RAD_TO_DEG);
}

#endif

void setup()
{
  delay(5000);
  expe.Init();
}

void loop()
{
  expe.Loop();
  // Serial.print("Theta9: ");
  // Serial.print(expe.GetTheta9() * RAD_TO_DEG);
  // Serial.print(" - Theta37: ");
  // Serial.print(expe.GetTheta37() * RAD_TO_DEG);
  // Serial.print(" - Alpha9: ");
  // Serial.print(expe.GetAlpha9() * RAD_TO_DEG);
  // Serial.print(" - Alpha37: ");
  // Serial.print(expe.GetAlpha37() * RAD_TO_DEG);
  // Serial.print(" - AnglesOK: ");
  // Serial.print(expe.AnglesOK());
  // Serial.print(" - Quat: ");
  // for (int i = 0; i < 4; i++)
  // {
  //   Serial.print(expe.GetQuat()[i]);
  //   Serial.print(" ");
  // }
  // Serial.println();
  // delay(100);
}