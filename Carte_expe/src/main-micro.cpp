#include <Arduino.h>
#include "Micro.h"
#include "EEPROM.h"

AudioInputI2S_F32            i2s1;           //xy=231.25,278.25
AudioRecordQueue_F32         queue1;         //xy=471.25,262.25
AudioRecordQueue_F32         queue2;         //xy=483.25,306.25
AudioConnection_F32          patchCord1(i2s1, 0, queue1, 0);
AudioConnection_F32          patchCord2(i2s1, 1, queue2, 0);

Micro micro(&Wire2, &queue1, &queue2, 0);

uint8_t nb_startups = 0;

SdFs sdx;

void setup() {
  nb_startups = EEPROM.read(0);
  EEPROM.write(0, nb_startups + 1);

  Serial.begin(115200);
  while (!Serial){};
  Serial.println("Start");
  if (!sdx.begin(SdioConfig(FIFO_SDIO))) {
		Serial.printf("\nSD File  system initialization failed.\n");
	}
  Serial.println("SD File system initialized.");
  delay(200);
  micro.Init(nb_startups);
  micro.start_audio();

}

void loop() {
  // put your main code here, to run repeatedly:
  micro.save_audio(false);
}
