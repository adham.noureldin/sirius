#include "defines.h"
#include "imu.h"
#include "Camera.h"
// #include "IntervalTimerEx.h"
#include <EasyTransfer.h>
#include "InternCom.h"
#include "SDCard.h"

// #include "Micro.h"

// #define MICRO_ON

#define TIME_BETWEEN_SERIAL_COMMUNICATION 1000 // send and receive message every 1 s // on devrait calculer jusqu'à quelle délais on peut descendre

#define SD_SAMPLERATE 25600 // 30 kHz (moins que 32 kHz pour être sûr que les imus aient des données)
#define SD_BUFFERMSEC 60
#define MAXBUFFER 150000

#define SLOW_SAMPLERATE 20
#define SLOW_BUFFERMSEC 2000
#define SLOW_BUFFERSIZE SLOW_SAMPLERATE *SLOW_BUFFERMSEC / 1000

#ifdef SDCARD_ON
extern void myCollector(void *vdp);
extern void myBinaryDisplay(void *vdp);
extern void LoggerISR(void);
extern DataLogger mydl;
extern uint8_t bufferexp[MAXBUFFER];

struct SLOWrec
{
    float gyr9[3] = {0, 0, 0};
    float acc9[3] = {0, 0, 0};
};

struct datrec
{
    uint32_t microstime;
    float acc9[3];
    float gyr9[3];
    float acc37[3];
    float gyr37[3];
    float quat9[4];
    // float quat37[4];
    float vit9[3];
    // float vit37[3];
    float pos9[3];
    // float pos37[2]; // j'ai enlevé pos37z pour avoir une puissance de 2 pour la taille de la structure et que c'est inutile
};
#endif

class Expe
{
private:
#ifdef SDCARD_ON
    SDCard sdcard = SDCard(SD_SAMPLERATE, SD_BUFFERMSEC);
    bool stocking = false; // indique si on est en train de stocker les données sur la carte SD
#endif
    int switchpin = 4;
    int cspin9 = 9;
    int cspin37 = 37;
    IMU imu9 = IMU();
    IMU imu37 = IMU();

    Camera cam1;
    Camera cam2;
    Camera cam3;
    Camera cam4;

    const int flaggyro = 5;
    bool telemON = true; // pour les caméras
    bool shunttelemtire = true;
    const uint32_t fin_camera = 60000000;
    // const uint32_t fin_imu = 5 * 60000000;
    bool a_decolle = 0;
    unsigned long t_millis_dec = 0;
    unsigned long t_millis_veille = 60000; // 1 minute après le décollage
    int t_dernier_stockage = 0;

    float theta9;
    float theta37;
    float alpha9;
    float alpha37;
    float pos9[3] = {0, 0, 0};
    float pos37[3] = {0, 0, 0};
    float vit9[3] = {0, 0, 0};
    float vit37[3] = {0, 0, 0};
    float quat9[4] = {0, 0, 0, 1}; // quaternion de la carte IMU
    float quat37[4] = {0, 0, 0, 1};

    bool sdInitSuccess = true;

    float theta_ref;                             // Angle de référence theta
    float alpha_ref;                             // Angle de référence alpha
    float last_theta_ref;                        // Angle de référence theta mesurée 1s avant
    float last_alpha_ref;                        // Angle de référence alpha mesurée 1s avant
    uint32_t t_millis_derniere_mesure_angle = 0; // Temps de la dernière mesure des angles pour la référence
    uint32_t new_t_millis_derniere_mesure_angle;
    const uint32_t dt_mesure_angle_ref = 1000; // Temps entre chaque mesure angle référence

    enum Etat
    {
        AU_SOL,
        EN_VOL,
        VEILLE
    };
    Etat etat = AU_SOL;

    EasyTransfer ET_Info_in, ET_Info_out;
    Info_to_xp_struct Info_in;  // structure qu'on recoit de Info
    XP_to_info_struct Info_out; // structure qu'on envoie à Info
    uint32_t lastSerialCommTime = 0;

    uint32_t tshunttelemtire = 0;

    float moy_glissante_gyr[3] = {0, 0, 0};
    float moy_glissante_acc[3] = {0, 0, 0};

    uint32_t slow_buffer_index = 0;

    SLOWrec slow_buffer[SLOW_BUFFERSIZE];
    uint32_t last_slow_measure_millis = 0;
    uint32_t last_gyro_calib = 0;

    int imu_grounded_count = 0;
    float avg_acc0[3] = {0, 0, 0};

#ifdef MICRO_ON
    Micro micro;
#endif

public:
#ifdef MICRO_ON
    Expe(AudioRecordQueue_F32 *queue1ptr, AudioRecordQueue_F32 *queue2ptr);
#else
    Expe();
#endif
    void Init();
    void Loop();
    void updateSendReceiveSerialCommunications(bool now = false);
    float *GetAcc9();
    float *GetAcc37();
    float *GetGyr9();
    float *GetGyr37();
    float GetTheta9();
    float GetTheta37();
    float GetAlpha9();
    float GetAlpha37();
    float *GetPos9();
    float *GetPos37();
    float *GetVit9();
    float *GetVit37();
    float *GetQuat9();
    float *GetQuat37();
    // void StockTheta();
    void ActualiserDonnees();
    bool AnglesOK();
    bool AlphaOK();
    bool ThetaOK();
    void InitIMUs();
    bool ShuntTelemTire();
    void TestCams(); // Pour tester les caméras

    void OldLoop(); // Temporaire
    void AuSol();
    void EnVol();
    void Veille();
    void UpdateEtat();
    void UpdateFlagGyro();
    bool ADecolle();
};
