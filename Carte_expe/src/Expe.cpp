#include "Expe.h"

#ifdef MICRO_ON
Expe::Expe(AudioRecordQueue_F32 *queue1ptr, AudioRecordQueue_F32 *queue2ptr)
    : micro(&Wire, queue1ptr, queue2ptr, 0)
#else
Expe::Expe()
#endif
{
#ifdef CAMERA_ON
    cam1 = Camera();
    cam2 = Camera();
    cam3 = Camera();
    cam4 = Camera();
#endif
}

void Expe::updateSendReceiveSerialCommunications(bool now = false)
{
    bool received_info = ET_Info_in.receiveData();
    if (millis() - lastSerialCommTime > TIME_BETWEEN_SERIAL_COMMUNICATION || now)
    {
        // Serial.println("sending serial data");
        lastSerialCommTime = millis();

        Info_out.alphaOk = AlphaOK();
        Info_out.thetaOk = ThetaOK();

        Info_out.state_xp = 0;
        if (imu9.IsON())
        {
            memcpy(Info_out.quaternion, quat9, sizeof(quat9));
            memcpy(Info_out.position, pos9, sizeof(pos9));
            Info_out.normAcc = sqrt(imu9.GetAcc()[0] * imu9.GetAcc()[0] + imu9.GetAcc()[1] * imu9.GetAcc()[1] + imu9.GetAcc()[2] * imu9.GetAcc()[2]);
            Info_out.normV = sqrt(imu9.Getv()[0] * imu9.Getv()[0] + imu9.Getv()[1] * imu9.Getv()[1] + imu9.Getv()[2] * imu9.Getv()[2]);
            // Info_out.alpha = alpha9;
            // Info_out.theta = theta9;
        }
        else if (imu37.IsON())
        {
            memcpy(Info_out.quaternion, quat37, sizeof(quat37));
            memcpy(Info_out.position, pos37, sizeof(pos37));
            Info_out.normAcc = sqrt(imu37.GetAcc()[0] * imu37.GetAcc()[0] + imu37.GetAcc()[1] * imu37.GetAcc()[1] + imu37.GetAcc()[2] * imu37.GetAcc()[2]);
            Info_out.normV = sqrt(imu37.Getv()[0] * imu37.Getv()[0] + imu37.Getv()[1] * imu37.Getv()[1] + imu37.Getv()[2] * imu37.Getv()[2]);
            // Info_out.alpha = alpha37;
            // Info_out.theta = theta37;
        }
        // Serial.println(Info_out.alpha);
        ET_Info_out.sendData();
        // Serial.println("serial data sent");
    }
}

void Expe::ActualiserDonnees()
{
    if (imu9.IsON())
    {
        if (a_decolle)
        {
            imu9.ActualiserDonnees(true);
        }
        else
        {
            imu9.ActualiserDonnees(false);
        }
    }
    if (imu37.IsON())
    {
        if (a_decolle)
        {
            imu37.ActualiserDonnees(true);
        }
        else
        {
            imu37.ActualiserDonnees(false);
        }
    }
    theta9 = imu9.GetTheta();
    theta37 = imu37.GetTheta();
    alpha9 = imu9.GetAlpha();
    alpha37 = imu37.GetAlpha();
    float *q9 = imu9.Getq();
    float *q37 = imu37.Getq();
    for (int i = 0; i < 4; i++)
    {
        quat9[i] = q9[i];
        quat37[i] = q37[i];
    }
    float *p9 = imu9.Getp();
    float *p37 = imu37.Getp();
    for (int i = 0; i < 3; i++)
    {
        pos9[i] = p9[i];
        pos37[i] = p37[i];
    }
    float *v9 = imu9.Getv();
    float *v37 = imu37.Getv();
    for (int i = 0; i < 3; i++)
    {
        vit9[i] = v9[i];
        vit37[i] = v37[i];
    }
}

void Expe::InitIMUs()
{
#if PRINT_SERIAL_MODE
    Serial.println("Initializing IMUs");
#endif

    // On envoie un message à Info pour lui dire qu'on est pas prêt
    Info_out.imus_prets = false;
    updateSendReceiveSerialCommunications(true);
    imu9.Init(cspin9);   // On initialise les IMUs
    imu37.Init(cspin37); // On initialise les IMUs
    if (imu9.IsON())
    {
        digitalWrite(36, LOW);
        imu9.Calibrate();
    }
    if (imu37.IsON())
    {
        imu37.Calibrate();
    }

    if (imu9.IsON())
    {
        imu9.Calculateq0();
    }
    if (imu37.IsON())
    {
        imu37.Calculateq0();
    }

    ActualiserDonnees();
    delay(100);

    Info_out.imus_prets = true;
#if PRINT_SERIAL_MODE
    Serial.println("IMUs Initialized");
#endif
}

float *Expe::GetAcc9()
{
    return imu9.GetAcc();
}

float *Expe::GetAcc37()
{
    return imu37.GetAcc();
}

float *Expe::GetGyr9()
{
    return imu9.GetGyr();
}

float *Expe::GetGyr37()
{
    return imu37.GetGyr();
}

float Expe::GetTheta9()
{
    return theta9;
}

float Expe::GetTheta37()
{
    return theta37;
}

float Expe::GetAlpha9()
{
    return alpha9;
}

float Expe::GetAlpha37()
{
    return alpha37;
}

float *Expe::GetPos9()
{
    return pos9;
}

float *Expe::GetPos37()
{
    return pos37;
}

float *Expe::GetVit9()
{
    return vit9;
}

float *Expe::GetVit37()
{
    return vit37;
}

float *Expe::GetQuat9()
{
    return quat9;
}

float *Expe::GetQuat37()
{
    return quat37;
}

bool Expe::AnglesOK()
{
    // if (theta0 - dtheta < theta && theta < theta0 + dtheta && alpha0 - dalpha < alpha && alpha < alpha0 + dalpha)
    // {
    //     return 1;
    // }
    // else
    // {
    //     return 0;
    // }

    // if (imu9.IsON() && imu37.IsON())
    // {
    //     return imu9.AnglesOK() && imu37.AnglesOK();
    // }
    // else if (imu9.IsON())
    // {
    //     return imu9.AnglesOK();
    // }
    // else if (imu37.IsON())
    // {
    //     return imu37.AnglesOK();
    // }

    // l'imu9 est beaucoup plus fiable d'après mes tests
    if (imu9.IsON())
    {
        return imu9.AnglesOK();
    }
    else if (imu37.IsON())
    {
        return imu37.AnglesOK();
    }
    else
    {
        return 0;
    }
}

bool Expe::ThetaOK()
{
    if (imu9.IsON())
    {
        return imu9.ThetaOK();
    }
    else if (imu37.IsON())
    {
        return imu37.ThetaOK();
    }
    else
    {
        return 0;
    }
}

bool Expe::AlphaOK()
{
    if (imu9.IsON())
    {
        return imu9.AlphaOK();
    }
    else if (imu37.IsON())
    {
        return imu37.AlphaOK();
    }
    else
    {
        return 0;
    }
}

// void Expe::StockTheta()
// {
// #ifdef SD_CARD_ON
//     sdcard.handleSensorData("theta.bin", GetTheta() * RAD_TO_DEG);
// #if PRINT_SERIAL_MODE
//     Serial.println("Stockage sur carte SD");
// #endif
// #endif
// }

// bool Expe::ShuntTelemTire()
// {
//     Serial.println(Info_in.telemON);
//     Serial.println(telemON);
//     if (Info_in.telemON && !shunttelemtire)
//     {
//         shunttelemtire = true;
//         tshunttelemtire = millis();
//     }
//     if (!Info_in.telemON)
//     {
//         shunttelemtire = false;
//         telemON = false;
//     }
//     if (Info_in.telemON && millis() - tshunttelemtire > 1000 && !telemON)
//     {
//         telemON = true;
//         return true;
//     }
//     return false;
// }

bool Expe::ShuntTelemTire()
{
    if (Info_in.telemON && !telemON)
    {
        telemON = true;
        return true;
    }
    if (!Info_in.telemON)
    {
        telemON = false;
    }
    return false;
}

void Expe::Init()
{
    pinMode(13, OUTPUT); // allumer la led de la teensy pour savoir quand elle est allumée
    pinMode(switchpin, INPUT);
    digitalWrite(13, HIGH);
#ifdef SDCARD_ON
    char filename[64] = "data_exp";
    if (!sdcard.Init(sizeof(datrec), true, filename))
    { // try starting SD Card and file system
        // initialize SD Card failed
        Serial.println("SD Card initialization failed");
        sdInitSuccess = false;
    }
    uint32_t bufflen = mydl.InitializeBuffer(sizeof(datrec), SD_SAMPLERATE, SD_BUFFERMSEC, bufferexp);
    uint8_t *bufferend = bufferexp + bufflen;
    Serial.printf("End of buffer at %p\n", bufferend);
    if ((bufflen == 0) || (bufflen > MAXBUFFER))
    {
        Serial.println("Not enough buffer space!  Reduce buffer time or sample rate.");
        sdInitSuccess = false;
    }
    mydl.AttachCollector(&myCollector);
    mydl.AttachDisplay(&myBinaryDisplay, 5000);
    Serial.println("SD Card initialized.");
#ifdef MICRO_ON
    micro.Init();
    micro.start_audio();
#endif
#endif

    pinMode(cspin9, OUTPUT);
    pinMode(cspin37, OUTPUT);
    pinMode(flaggyro, OUTPUT);
    pinMode(9, OUTPUT);
    pinMode(10, OUTPUT);

    digitalWrite(36, HIGH);
    digitalWrite(9, HIGH);
    digitalWrite(10, HIGH);

    digitalWrite(37, HIGH);

    digitalWrite(cspin9, HIGH);
    digitalWrite(cspin37, HIGH);
    delay(100);
    INFSERIAL.begin(4800);
#if PRINT_SERIAL_MODE
    Serial.begin(115200);
#endif
    delay(1000);
    Serial.println("Serial OK");

// Camera
#ifdef CAMERA_ON
    cam1.Init(&Serial3, 115200);
    cam2.Init(&Serial4, 115200);
    cam3.Init(&Serial7, 115200);
    cam4.Init(&Serial8, 115200);
    Serial.println("Cameras initialized");
#endif

    ET_Info_in.begin(details(Info_in), &INFSERIAL);
    ET_Info_out.begin(details(Info_out), &INFSERIAL);

    // IMU
    InitIMUs();
    delay(100);
    Serial.println("Init OK");
}

// void Expe::OldLoop()
// { // Détection du décollage
//     if (Info_in.a_decolle && !a_decolle)
//     {
//         a_decolle = true;
//         t_millis_dec = millis();
//     }

//     // IMUs
//     updateSendReceiveSerialCommunications();
//     ActualiserDonnees();

//     if (ShuntTelemTire())
//     {
//         Serial.println("Shunt Telem tiré");
//         InitIMUs();
// #ifdef CAMERA_ON
//         cam1.Power_on();
//         cam2.Power_on();
//         cam3.Power_on();
//         cam4.Power_on();
// #endif
//     }
//     if (AnglesOK() == 1)
//     {
//         digitalWrite(flaggyro, HIGH);
//     }
//     else
//     {
//         digitalWrite(flaggyro, LOW);
//     }

// // Cameras
// #ifdef CAMERA_ON
//     if (millis() - t_millis_dec > fin_camera)
//     {
//         // Éteindre les caméras
//         cam1.Power_off();
//         cam2.Power_off();
//         cam3.Power_off();
//         cam4.Power_off();
//     }
// #endif
// }

void Expe::UpdateFlagGyro()
{
    if (AnglesOK() == 1)
    {
        digitalWrite(flaggyro, HIGH);
    }
    else
    {
        digitalWrite(flaggyro, LOW);
    }
}

void Expe::UpdateEtat()
{
    if (Info_in.a_decolle && !a_decolle)
    {
        Serial.println("Décollage");
        a_decolle = true;
        t_millis_dec = millis();
        etat = EN_VOL;
    }
    if (a_decolle && millis() - t_millis_dec > t_millis_veille)
    {
        Serial.println("Mise en veille");
        etat = VEILLE;
    }
}

void Expe::Loop()
{
    switch (etat)
    {
    case AU_SOL:
        AuSol();
        break;
    case EN_VOL:
        EnVol();
        break;
    case VEILLE:
        Veille();
        break;
    }
    UpdateEtat();
}
#define printf Serial.printf
#if ARDUINO_TEENSY41
extern "C" uint8_t external_psram_size;
#endif
void memInfo()
{
    constexpr auto RAM_BASE = 0x2020'0000;
    constexpr auto RAM_SIZE = 512 << 10;
    constexpr auto FLASH_BASE = 0x6000'0000;
#if ARDUINO_TEENSY40
    constexpr auto FLASH_SIZE = 2 << 20;
#elif ARDUINO_TEENSY41
    constexpr auto FLASH_SIZE = 8 << 20;
#endif

    // note: these values are defined by the linker, they are not valid memory
    // locations in all cases - by defining them as arrays, the C++ compiler
    // will use the address of these definitions - it's a big hack, but there's
    // really no clean way to get at linker-defined symbols from the .ld file

    extern char _stext[], _etext[], _sbss[], _ebss[], _sdata[], _edata[],
        _estack[], _heap_start[], _heap_end[], _itcm_block_count[], *__brkval;

    auto sp = (char *)__builtin_frame_address(0);

    printf("_stext        %08x\n", _stext);
    printf("_etext        %08x +%db\n", _etext, _etext - _stext);
    printf("_sdata        %08x\n", _sdata);
    printf("_edata        %08x +%db\n", _edata, _edata - _sdata);
    printf("_sbss         %08x\n", _sbss);
    printf("_ebss         %08x +%db\n", _ebss, _ebss - _sbss);
    printf("curr stack    %08x +%db\n", sp, sp - _ebss);
    printf("_estack       %08x +%db\n", _estack, _estack - sp);
    printf("_heap_start   %08x\n", _heap_start);
    printf("__brkval      %08x +%db\n", __brkval, __brkval - _heap_start);
    printf("_heap_end     %08x +%db\n", _heap_end, _heap_end - __brkval);
#if ARDUINO_TEENSY41
    extern char _extram_start[], _extram_end[], *__brkval;
    printf("_extram_start %08x\n", _extram_start);
    printf("_extram_end   %08x +%db\n", _extram_end,
           _extram_end - _extram_start);
#endif
    printf("\n");

    printf("<ITCM>  %08x .. %08x\n",
           _stext, _stext + ((int)_itcm_block_count << 15) - 1);
    printf("<DTCM>  %08x .. %08x\n",
           _sdata, _estack - 1);
    printf("<RAM>   %08x .. %08x\n",
           RAM_BASE, RAM_BASE + RAM_SIZE - 1);
    printf("<FLASH> %08x .. %08x\n",
           FLASH_BASE, FLASH_BASE + FLASH_SIZE - 1);
#if ARDUINO_TEENSY41
    if (external_psram_size > 0)
        printf("<PSRAM> %08x .. %08x\n",
               _extram_start, _extram_start + (external_psram_size << 20) - 1);
#endif
    printf("\n");

    auto stack = sp - _ebss;
    printf("avail STACK %8d b %5d kb\n", stack, stack >> 10);

    auto heap = _heap_end - __brkval;
    printf("avail HEAP  %8d b %5d kb\n", heap, heap >> 10);

#if ARDUINO_TEENSY41
    auto psram = _extram_start + (external_psram_size << 20) - _extram_end;
    printf("avail PSRAM %8d b %5d kb\n", psram, psram >> 10);
#endif
}

void Expe::AuSol()
{
    // Serial.println();
    // memInfo();
    updateSendReceiveSerialCommunications(false);

    new_t_millis_derniere_mesure_angle = millis() - t_millis_derniere_mesure_angle;
    if (t_millis_derniere_mesure_angle == 0)
    {
        theta_ref = GetTheta9();
        alpha_ref = GetAlpha9();
        t_millis_derniere_mesure_angle = new_t_millis_derniere_mesure_angle;
    }
    else if (new_t_millis_derniere_mesure_angle > dt_mesure_angle_ref)
    {
        last_theta_ref = theta_ref;
        last_alpha_ref = alpha_ref;
        theta_ref = GetTheta9();
        alpha_ref = GetAlpha9();
        t_millis_derniere_mesure_angle = new_t_millis_derniere_mesure_angle;
    }
#ifdef SDCARD_ON
    if (!stocking)
    {
        stocking = true;
        // mydl.StartLogger("data_exp1.bin", 1000, &LoggerISR);
        sdcard.StartLogging();
    }

    if (millis() - last_slow_measure_millis > 1.0f / SLOW_SAMPLERATE)
    {
        last_slow_measure_millis = millis();
        // moy_glissante_gyr[0] -= slow_buffer[slow_buffer_index].gyr9[0]/(SLOW_BUFFERSIZE/2);
        // moy_glissante_gyr[1] -= slow_buffer[slow_buffer_index].gyr9[1]/(SLOW_BUFFERSIZE/2);
        // moy_glissante_gyr[2] -= slow_buffer[slow_buffer_index].gyr9[2]/(SLOW_BUFFERSIZE/2);

        slow_buffer[slow_buffer_index].gyr9[0] = GetGyr9()[0];
        slow_buffer[slow_buffer_index].gyr9[1] = GetGyr9()[1];
        slow_buffer[slow_buffer_index].gyr9[2] = GetGyr9()[2];
        slow_buffer[slow_buffer_index].acc9[0] = GetAcc9()[0];
        slow_buffer[slow_buffer_index].acc9[1] = GetAcc9()[1];
        slow_buffer[slow_buffer_index].acc9[2] = GetAcc9()[2];

        // moy_glissante_gyr[0] += slow_buffer[(slow_buffer_index - SLOW_BUFFERSIZE/2) % SLOW_BUFFERSIZE].gyr9[0]/(SLOW_BUFFERSIZE/2);
        // moy_glissante_gyr[1] += slow_buffer[(slow_buffer_index - SLOW_BUFFERSIZE/2) % SLOW_BUFFERSIZE].gyr9[1]/(SLOW_BUFFERSIZE/2);
        // moy_glissante_gyr[2] += slow_buffer[(slow_buffer_index - SLOW_BUFFERSIZE/2) % SLOW_BUFFERSIZE].gyr9[2]/(SLOW_BUFFERSIZE/2);

        slow_buffer_index = (slow_buffer_index + 1) % SLOW_BUFFERSIZE;

        // Serial.printf("gyro moyenne glissante: %f, %f, %f\n", moy_glissante_gyr[0], moy_glissante_gyr[1], moy_glissante_gyr[2]);
        // Serial.printf("gyro : %f, %f, %f\n", GetGyr9()[0], GetGyr9()[1], GetGyr9()[2]);

        // avg_acc0[0] += imu9.GetAcc()[0];
        // avg_acc0[1] += imu9.GetAcc()[1];
        // avg_acc0[2] += imu9.GetAcc()[2];
        // imu_grounded_count++;

        if (millis() - last_gyro_calib > 60000)
        {
            last_gyro_calib = millis();
            moy_glissante_gyr[0] = 0;
            moy_glissante_gyr[1] = 0;
            moy_glissante_gyr[2] = 0;
            moy_glissante_acc[0] = 0;
            moy_glissante_acc[1] = 0;
            moy_glissante_acc[2] = 0;
            for (int i = slow_buffer_index; i < slow_buffer_index + SLOW_BUFFERSIZE / 2; i++)
            {
                moy_glissante_gyr[0] += slow_buffer[i % SLOW_BUFFERSIZE].gyr9[0];
                moy_glissante_gyr[1] += slow_buffer[i % SLOW_BUFFERSIZE].gyr9[1];
                moy_glissante_gyr[2] += slow_buffer[i % SLOW_BUFFERSIZE].gyr9[2];
                moy_glissante_acc[0] += slow_buffer[i % SLOW_BUFFERSIZE].acc9[0];
                moy_glissante_acc[1] += slow_buffer[i % SLOW_BUFFERSIZE].acc9[1];
                moy_glissante_acc[2] += slow_buffer[i % SLOW_BUFFERSIZE].acc9[2];
            }
            moy_glissante_gyr[0] /= SLOW_BUFFERSIZE / 2;
            moy_glissante_gyr[1] /= SLOW_BUFFERSIZE / 2;
            moy_glissante_gyr[2] /= SLOW_BUFFERSIZE / 2;
            moy_glissante_acc[0] /= SLOW_BUFFERSIZE / 2;
            moy_glissante_acc[1] /= SLOW_BUFFERSIZE / 2;
            moy_glissante_acc[2] /= SLOW_BUFFERSIZE / 2;
            imu9.setGyroBias(moy_glissante_gyr, moy_glissante_acc);

            // avg_acc0[0] /= imu_grounded_count;
            // avg_acc0[1] /= imu_grounded_count;
            // avg_acc0[2] /= imu_grounded_count;
            // imu9.SetAcc0(avg_acc0);
            // imu_grounded_count = 0;

            imu9.Calculateq0(); // Pour avoir les valeurs des angles en statique, quand la fusée est immobile
        }
    }
    sdcard.CheckLogger(true);
#ifdef MICRO_ON
    micro.save_audio(true);
#endif

#endif
    UpdateFlagGyro();

    if (Info_in.telemON && !telemON)
    {
        Serial.println("Shunt Telem tiré");
        telemON = true;
        sdcard.QuitLogging();
#ifdef MICRO_ON
        micro.end_audio();
#endif
        delay(1);
        InitIMUs();
        sdcard.StartLogging();
#ifdef MICRO_ON
        micro.start_audio();
#endif
#ifdef CAMERA_ON
        cam1.Power_on();
        cam2.Power_on();
        cam3.Power_on();
        cam4.Power_on();
#endif
    }
    if (!Info_in.telemON && telemON)
    {
        Serial.println("Shunt Telem remis");
        telemON = false;
#ifdef CAMERA_ON
        cam1.Power_off();
        cam2.Power_off();
        cam3.Power_off();
        cam4.Power_off();
#endif
    }
    if (digitalRead(switchpin) == LOW)
    {
        cam1.Power_off();
        cam2.Power_off();
        cam3.Power_off();
        cam4.Power_off();
        // Serial.println("Cam off");
    }
}

void Expe::EnVol()
{
    // Info_out.last_alpha_ref = last_alpha_ref;
    // Info_out.last_theta_ref = last_theta_ref;
    updateSendReceiveSerialCommunications(false);
#ifdef SDCARD_ON
    sdcard.CheckLogger(false);
#ifdef MICRO_ON
    micro.save_audio(false);
#endif
#endif
    UpdateFlagGyro();
}

void Expe::Veille()
{
    Serial.println("Flight ended");
#ifdef CAMERA_ON
    cam1.Power_off();
    cam2.Power_off();
    cam3.Power_off();
    cam4.Power_off();
#endif
#ifdef SDCARD_ON
#ifdef MICRO_ON
    micro.shutoff();
#endif
    sdcard.QuitLogging();

    SD.sdfs.end();
#endif
    imu9.shutdown();
    imu37.shutdown();

    SPI.end();
    Serial.end();
    INFSERIAL.end();
    while (1) // boucle infinie pour arreter le programme
    {
    }
}

void Expe::TestCams()
{
    cam1.Power_on();
    cam2.Power_on();
    cam3.Power_on();
    cam4.Power_on();
    delay(10000);
    cam1.Power_off();
    cam2.Power_off();
    cam3.Power_off();
    cam4.Power_off();
    delay(10000);
}