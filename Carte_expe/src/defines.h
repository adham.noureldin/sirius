// Contient les #include et les #define communs à plusieurs fichiers

#include <Arduino.h>

// Choix d'afficher ou pas les données sur le Serial
#define PRINT_SERIAL_MODE true // Si true, on affiche les données sur le Serial

#define INFSERIAL Serial1

#define CAMERA_ON
#define SDCARD_ON

// unsigned long long getMicros()
// {
//     static unsigned long long time = (unsigned long long)micros();
//     static unsigned long long lastCall = micros();
//     unsigned long long newCall = micros();

//     if (newCall > lastCall)
//     {
//         time += newCall - lastCall;
//     }
//     else
//     {
//         time += newCall + (1ULL << 32 - 1 - lastCall);
//     }
//     lastCall = newCall;

//     return time;
// }