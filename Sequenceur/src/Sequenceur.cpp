#include "Sequenceur.h"

Sequenceur::Sequenceur()
{
}

// unsigned long long Sequenceur::getMicros()
// {
//     static unsigned long long time = (unsigned long long)micros();
//     static unsigned long long lastCall = micros();
//     unsigned long long newCall = micros();

//     if (newCall > lastCall)
//     {
//         time += newCall - lastCall;
//     }
//     else
//     {
//         time += newCall + ((1ULL << 32) - 1 - lastCall);
//     }
//     lastCall = newCall;

//     return time;
// }

// unsigned long long Sequenceur::getMicros()
// {
//     unsigned long currentMicros = micros();

//     // Si currentMicros est inférieur à previousMicros, alors il y a eu un débordement
//     if (currentMicros < previousMicros)
//     {
//         overflowCount++;
//     }

//     previousMicros = currentMicros;

//     // Calculer le temps total en microsecondes en tenant compte des débordements
//     return currentMicros + (overflowCount * 4294967296);
// }

void Sequenceur::Setup()
{
    pinMode(5, OUTPUT); // allumer la led de la teensy pour savoir quand elle est allumée
    digitalWrite(5, HIGH);

    pyro.Init();

    pinMode(switchPin, INPUT);
    pinMode(led1Pin, OUTPUT);
    pinMode(led2Pin, OUTPUT);
    pinMode(jackpin, INPUT_PULLUP);
    pinMode(detectionseppin, INPUT);
    pinMode(servopin, OUTPUT);
    pinMode(shunttelempin, INPUT_PULLUP);
    servo.attach(servopin); // attaches the servo on pin 18 to the servo object
                            // using default min/max of 1000us and 2000us
                            // different servos may require different min/max settings
                            // for an accurate 0 to 180 sweep
    servo.write(angle_ferme);
    Info_out.para = false;
#ifdef IS_BETA
    pinMode(seppin, OUTPUT);
    digitalWrite(seppin, LOW);
    pinMode(flaggyro, INPUT);
    pinMode(mafpin, OUTPUT);
    digitalWrite(mafpin, LOW);
    pinMode(buzzerPin, OUTPUT);
#endif // IS_BETA

    Serial.begin(9600);
    INFSERIAL.begin(4800);
    delay(1000);
    ET_Info_in.begin(details(Info_in), &INFSERIAL);
    ET_Info_out.begin(details(Info_out), &INFSERIAL);

    etatswitch = digitalRead(switchPin);
#ifdef SERIAL_ON
    Serial.println("Setup OK");
#endif
}

void Sequenceur::TestSwitch()
{
    if (digitalRead(switchPin) == HIGH)
    {
        digitalWrite(led1Pin, HIGH);
        digitalWrite(led2Pin, LOW);
#ifdef SERIAL_ON
        Serial.println("Switch is ON");
#endif
    }
    else
    {
        digitalWrite(led1Pin, LOW);
        digitalWrite(led2Pin, HIGH);
#ifdef SERIAL_ON
        Serial.println("Switch is OFF");
#endif
    }
}

void Sequenceur::updateSendReceiveSerialCommunications(bool now)
{
    bool received_info = ET_Info_in.receiveData();
    if (millis() - lastSerialCommTime > TIME_BETWEEN_SERIAL_COMMUNICATION || now)
    {
        lastSerialCommTime = millis();

        Info_out.a_decolle = etat != AU_SOL;
        Info_out.state_seq = (int)etat;
        Info_out.telemON = TelemON();
        ET_Info_out.sendData();
    }
}

bool Sequenceur::DecollageDetecte()
{
    if (digitalRead(jackpin) == HIGH && !jack_decollage_branche)
    {
        jack_decollage_branche = true;
        t_jack_decollage_branche = millis();
    }
    return (millis() - t_jack_decollage_branche > 1000 && jack_decollage_branche && digitalRead(jackpin) == LOW); // On return true si le jack a été branché il y a plus d'1 seconde puis débranché
}

bool Sequenceur::SepEffective()
{
    return digitalRead(detectionseppin);
}

bool Sequenceur::IMUs_prets()
{
    return Info_in.imus_prets;
}

bool Sequenceur::TmafOK()
{
    int t = millis() - tdec;
    return t > tsepeffect + dtmaf && t < tmafmax;
}

bool Sequenceur::AnglesOK()
{
    return digitalRead(flaggyro);
    // return true; // Pour les tests
}

bool Sequenceur::TelemON()
{
    if (digitalRead(shunttelempin) == HIGH && !shunttelemtire)
    {
        shunttelemtire = true;
        tshunttelemtire = millis();
    }
    if (digitalRead(shunttelempin) == LOW && shunttelemtire)
    {
        shunttelemtire = false;
        tshunttelemremis = millis();
        Serial.println("shunt remis");
    }
    if (shunttelemtire && millis() - tshunttelemtire > 100)
    {
        previous_shunttelemtire = true;
    }
    if (!shunttelemtire && millis() - tshunttelemremis > 100)
    {
        previous_shunttelemtire = false;
    }
    return previous_shunttelemtire;
}

void Sequenceur::UpdateLeds()
{
    if (etat != A_ATTERRI) // A changer pour éteindre les leds quand on est au sol
    {
#ifdef IS_BETA
        if (IMUs_prets())
        {
            pyro.Light(led_imusprets, 'g');
        }
        else
        {
            pyro.Light(led_imusprets, 'b');
        }

#ifdef TEST_COMMANDE_PAR_INFO
        if (Info_in.mise_a_feu)
        {
            pyro.Light(led_tmafOK, 'g');
        }
        else
        {
            pyro.Light(led_tmafOK, 'b');
        }
#else
        if (TmafOK())
        {
            pyro.Light(led_tmafOK, 'g');
        }
        else
        {
            pyro.Light(led_tmafOK, 'b');
        }
#endif // TEST_COMMANDE_PAR_INFO
        if (AnglesOK())
        {
            pyro.Light(led_anglesOK, 'g');
        }
        else if (Info_in.alphaOk && !Info_in.thetaOk)
        {
            pyro.Light(led_anglesOK, 'p');
        }
        else if (!Info_in.alphaOk && Info_in.thetaOk)
        {
            pyro.Light(led_anglesOK, 'b');
        }
        else
        {
            pyro.Light(led_anglesOK, 'y');
        }
        if (Info_in.pintOK)
        {
            pyro.Light(led_pintOK, 'g');
        }
        else
        {
            pyro.Light(led_pintOK, 'b');
        }
#else
        if (Info_in.gpsOK)
        {
            pyro.Light(led_gpsOK, 'g');
        }
        else
        {
            pyro.Light(led_gpsOK, 'b');
        }
        if (Info_in.barometreOK)
        {
            pyro.Light(led_barometreOK, 'g');
        }
        else
        {
            pyro.Light(led_barometreOK, 'b');
        }
#endif // IS_BETA
        if (SepEffective())
        {
            pyro.Light(led_jacksep, 'b');
        }
        else
        {
            pyro.Light(led_jacksep, 'g');
        }
        if (TelemON())
        {
            pyro.Light(led_telemON, 'g');
        }
        else
        {
            pyro.Light(led_telemON, 'b');
        }
        if (digitalRead(jackpin) == HIGH)
        {
            pyro.Light(led_jackdecollage, 'g');
        }
        else
        {
            pyro.Light(led_jackdecollage, 'r');
        }
    }
}

void Sequenceur::Loop()
{
    // Mise à jour état fusée
    switch (etat)
    {
    case AU_SOL:
        if (digitalRead(switchPin) == HIGH)
        {
            servo.write(angle_ouvert);
            Info_out.para = true;
            Serial.println("Ouverture porte");
        }
        else
        {
            servo.write(angle_ferme);
            Info_out.para = false;
            Serial.println("Fermeture porte");
        }
        if (DecollageDetecte())
        {
            tdec = millis() - dtdec;
            etat = A_DECOLLE;
            updateSendReceiveSerialCommunications(true);
#ifdef SERIAL_ON
            Serial.println("Décollage");
#endif
        }
        break;
    case A_DECOLLE:
        pyro.Light(led_decolle, 'g');
        if (tsep - 1000 < millis() - tdec && millis() - tdec < tsep && SepEffective()) // Si la séparation a lieu avant l'ordre
                                                                                       // on regarde après tsep-1000000 à cause des vibrations pendant la propulsion
        {
            tsepeffect = millis() - tdec;
#ifdef IS_BETA
            // BETA: Si la sep a lieu trop tôt, on actionne les vérins pour vrmt séparer, et on n'allume pas le moteur
            digitalWrite(seppin, HIGH);
            pyro.Light(led_sepeffective, 'g'); // Normalement on fait ça dans l'état SEPARATION_EFFECTIVE, mais comme on ne passe pas par cet état, on allume la led ici
            etat = NON_FEU;
#else
            etat = SEPARATION_EFFECTIVE;
#endif // IS_BETA
#ifdef SERIAL_ON
            Serial.println("Separation effective trop tot");
#endif
        }
        if (millis() - tdec > tsep)
        {
#ifdef IS_BETA
            digitalWrite(seppin, HIGH);
#endif // IS_BETA
            etat = ORDRE_SEPARATION;
#ifdef SERIAL_ON
            Serial.println("Ordre de séparation");
#endif
        }
        break;
    case ORDRE_SEPARATION:
#ifdef IS_BETA
        pyro.Light(led_ordresep, 'g');
#endif // IS_BETA
        if (SepEffective())
        {
            tsepeffect = millis() - tdec;
            etat = SEPARATION_EFFECTIVE;
#ifdef SERIAL_ON
            Serial.println("Séparation effective");
#endif
            Info_out.a_separe = true;
        }
        // Cas dégradé: Pas de séparation efective
        else if (millis() - tdec > tsepmax)
        {
            etat = SEPARATION_NON_EFFECTIVE;
#ifdef SERIAL_ON
            Serial.println("Séparation non effective");
#endif
        }
        break;
    case SEPARATION_NON_EFFECTIVE:
#ifdef IS_BETA
        if (millis() - tdec > tparamin1 && Info_in.flag_pression_parachute_beta)
        {
            OuverturePorte();
        }
        if (millis() - tdec > tparamax1)
        {
            OuverturePorte();
        }
#else
        if (SepEffective())
        {
            tsepeffect = millis() - tdec;
            if ((millis() - tdec > tservomin && Info_in.flag_pression_parachute_beta) || millis() - tdec > tservomax)
            {
                Serial.print("Séparation effective : ");
                Serial.println(millis());
                OuverturePorte();
            }
        }
#endif // IS_BETA

        // // Pour les tests
        // etat = SEPARATION_EFFECTIVE;
        // Serial.println("Séparation effective");
        // //
        break;
    case SEPARATION_EFFECTIVE:
        pyro.Light(led_sepeffective, 'g');
#ifdef IS_BETA
        if (TmafOK() && AnglesOK())
        {
            digitalWrite(mafpin, HIGH);
            Info_out.ordre_maf = true;
#ifndef BIETAGE
            pyro.Light(led_allumage, 'g');
#endif
#ifdef VOL_PASSIF
            etat = NON_FEU;
#else
#ifdef DETECTION_MAF
            etat = ORDRE_MAF;
#else
            etat = MISE_A_FEU;
#endif // DETECTION_MAF
#endif // VOL_PASSIF

#ifdef SERIAL_ON
            Serial.println("Ordre de mise à feu envoyé");
#endif // SERIAL_ON
        }
        // Cas dégradé: Pas de mise à feu du second moteur car theta pas bon
        if (millis() - tdec > tmafmax)
        {
            etat = NON_FEU;
            Serial.println("Pas de mise à feu du second moteur car theta pas bon");
        }
#else
        etat = MISE_A_FEU; // Si on est dans alpha, on attend direct qu'on puisse déployer le para
#endif // IS_BETA
        break;
    case ORDRE_MAF: // Exclusif à beta et au vol actif
        if (Info_in.mafdone)
        {
            etat = MISE_A_FEU;
#ifdef SERIAL_ON
            Serial.println("Mise à feu effective");
#endif // SERIAL_ON
        }
        if (millis() - tdec > tmafmax)
        {
            etat = NON_FEU;
            Serial.println("Non-feu");
        }
    case MISE_A_FEU:
#ifdef IS_BETA
        pyro.Light(led_allumage, 'g');
#endif // IS_BETA
        if (millis() - tdec > tservomin && Info_in.flag_pression_parachute_beta)
        {
            OuverturePorte();
        }
        if (millis() - tdec > tservomax)
        {
            OuverturePorte();
        }
        break;
    case PARA_DEPLOYE:
        pyro.Light(led_porte, 'g');
        digitalWrite(mafpin, LOW);
        if (millis() - tdec > tbuzzer)
        {
#ifdef IS_BETA
            BuzzerBlink();
#endif // IS_BETA
            etat = A_ATTERRI;
            pyro.Clear();
#ifdef SERIAL_ON
            Serial.println("Au sol, Buzzer ON");
#endif // SERIAL_ON
        }
        break;
    case A_ATTERRI:
        BuzzerBlink();
        break;
    case NON_FEU: // exclusif à beta
#ifdef IS_BETA
        if (millis() - tdec > tservomin_nonfeu && Info_in.flag_pression_parachute_beta)
        {
            OuverturePorte();
        }
        if (millis() - tdec > tservomax_nonfeu)
        {
            OuverturePorte();
        }
#endif // IS_BETA
        break;
    }
    UpdateLeds();
    updateSendReceiveSerialCommunications();
}

void Sequenceur::BuzzerBlink()
{
    if (millis() - t_millis_dernier_declenchement_buzzer > dt_cycle_buzzer)
    {
        t_millis_dernier_declenchement_buzzer = millis();
        digitalWrite(buzzerPin, HIGH);
    }
    else if (millis() - t_millis_dernier_declenchement_buzzer > dt_buzzer_ON)
    {
        digitalWrite(buzzerPin, LOW);
    }
}

void Sequenceur::OuverturePorte()
{
    servo.write(angle_ouvert);
    etat = PARA_DEPLOYE;
    Info_out.para = true;
#ifdef SERIAL_ON
    Serial.println("Ouverture porte");
#endif
}

///////////////////////////////////
/// Test avec le Serial de Info ///
///////////////////////////////////

// void Sequenceur::LoopTestTotalSerial()
// { // Mise à jour état fusée
//     switch (etat)
//     {
//     case AU_SOL:
//         if (Info_in.decollage)
//         {
//             tdec = millis();
//             etat = A_DECOLLE;
// #ifdef SERIAL_ON
//             Serial.println("Décollage");
// #endif
//         }
//         break;
//     case A_DECOLLE:
//         pyro.Light(led_decolle, 'g');
// #ifdef IS_BETA
//         if (Info_in.separation)
//         {
//             digitalWrite(seppin, HIGH);
//             etat = ORDRE_SEPARATION;
// #ifdef SERIAL_ON
//             Serial.println("Début Séparation");
// #endif // SERIAL_ON
//         }
// #else
//         etat = ORDRE_SEPARATION; // Si on est dans alpha, on attend direct que la séparation soit effective
// #endif // IS_BETA
//         break;
//     case ORDRE_SEPARATION:
//         if (SepEffective())
//         {
//             etat = SEPARATION_EFFECTIVE;
// #ifdef SERIAL_ON
//             Serial.println("Séparation effective");
// #endif
//         }
//         // // Pour les tests
//         // etat = SEPARATION_EFFECTIVE;
//         // Serial.println("Séparation effective");
//         // //
//         break;
//     case SEPARATION_EFFECTIVE:
//         pyro.Light(led_sepeffective, 'g');
// #ifdef IS_BETA
//         if (Info_in.mise_a_feu && AnglesOK())
//         {
//             digitalWrite(mafpin, HIGH);
//             etat = MISE_A_FEU;
// #ifdef SERIAL_ON
//             Serial.println("Ordre de mise à feu envoyé");
// #endif // SERIAL_ON
//         }
//         // Cas dégradé: Pas de mise à feu du second moteur car theta pas bon
//         // if (millis() - tdec > tmafmax)
//         // {
//         //     etat = NON_FEU;
//         //     Serial.println("Pas de mise à feu du second moteur car theta pas bon");
//         // }
// #else
//         etat = MISE_A_FEU; // Si on est dans alpha, on attend direct qu'on puisse déployer le para
// #endif // IS_BETA
//         break;
//     case MISE_A_FEU:
//         pyro.Light(led_allumage, 'g');
//         if (Info_in.ouverture_porte)
//         {
//             servo.write(angle_ouvert);
//             etat = PARA_DEPLOYE;
//             Info_out.para = true;
// #ifdef SERIAL_ON
//             Serial.println("Ouverture porte");
// #endif
//         }
//         break;
//     case PARA_DEPLOYE:
// #ifdef IS_BETA
//         pyro.Light(led_porte, 'g');
//         digitalWrite(mafpin, LOW);
//         if (Info_in.au_sol)
//         {
//             digitalWrite(buzzerPin, HIGH);
//             etat = A_ATTERRI;
//             pyro.Clear();
// #ifdef SERIAL_ON
//             Serial.println("Au sol, Buzzer ON");
// #endif // SERIAL_ON
//         }
// #else
//         etat = A_ATTERRI;
// #endif // IS_BETA
//         break;
//     case NON_FEU:
//         if (millis() - tdec > tservomin_nonfeu)
//         {
//             servo.write(angle_ouvert);
//             etat = PARA_DEPLOYE;
//             Info_out.para = true;
// #ifdef SERIAL_ON
//             Serial.println("Ouverture porte");
// #endif
//         }
//         break;
//     }
//     UpdateLeds();
//     updateSendReceiveSerialCommunications();
// }

// void Sequenceur::LoopTestIsoleSerial()
// {
//     if (Info_in.decollage)
//     {
//         pyro.Light(led_decolle, 'g');
//         Serial.println("Décollage");
//     }
//     else
//     {
//         pyro.Light(led_decolle, 'b');
//         Serial.println("Pas de décollage");
//     }
//     if (Info_in.separation)
//     {
//         digitalWrite(seppin, HIGH);
//         pyro.Light(led_sepeffective, 'g');
//         Serial.println("Séparation");
//     }
//     else
//     {
//         digitalWrite(seppin, LOW);
//         pyro.Light(led_sepeffective, 'b');
//         Serial.println("Pas de séparation");
//     }
//     if (Info_in.mise_a_feu)
//     {
//         digitalWrite(mafpin, HIGH);
//         pyro.Light(led_allumage, 'g');
//         Serial.println("Mise à feu");
//     }
//     else
//     {
//         digitalWrite(mafpin, LOW);
//         pyro.Light(led_allumage, 'b');
//         Serial.println("Pas de mise à feu");
//     }
//     if (Info_in.ouverture_porte)
//     {
//         servo.write(angle_ouvert);
//         Info_out.para = true;
//         pyro.Light(led_porte, 'g');
//         Serial.println("Ouverture porte");
//     }
//     else
//     {
//         servo.write(angle_ferme);
//         Info_out.para = false;
//         pyro.Light(led_porte, 'b');
//         Serial.println("Fermeture porte");
//     }
//     if (Info_in.au_sol)
//     {
//         digitalWrite(buzzerPin, HIGH);
//         pyro.Clear();
//         etat = A_ATTERRI; // Pour pas que UpdateLeds rallume les leds
//         Serial.println("Au sol, Buzzer ON");
//     }
//     else
//     {
//         digitalWrite(buzzerPin, LOW);
//         Serial.println("Pas au sol");
//     }
//     UpdateLeds();
//     updateSendReceiveSerialCommunications();
// }

////////////////////////////////
/// Test avec l'interrupteur ///
////////////////////////////////

// bool Sequenceur::SwitchActivated()
// {
//     if (digitalRead(switchPin) != etatswitch)
//     {
//         etatswitch = digitalRead(switchPin);
//         return true;
//     }
//     else
//     {
//         return false;
//     }
// }

// void Sequenceur::LoopTestSwitch()
// {

//     // Mise à jour état fusée
//     switch (etat)
//     {
//     case AU_SOL:
//         if (DecollageDetecte()) // L'interrupteur c'est pour les tests
//         {
//             tdec = millis();
//             etat = A_DECOLLE;
// #ifdef SERIAL_ON
//             Serial.println("Décollage");
// #endif
//         }
//         break;
//     case A_DECOLLE:
//         pyro.Light(led_decolle, 'g');
// #ifdef IS_BETA
//         if (SwitchActivated())
//         {
//             digitalWrite(seppin, HIGH);
//             etat = ORDRE_SEPARATION;
// #ifdef SERIAL_ON
//             Serial.println("Début Séparation");
// #endif // SERIAL_ON
//         }
// #else
//         etat = ORDRE_SEPARATION; // Si on est dans alpha, on attend direct que la séparation soit effective
// #endif // IS_BETA
//         break;
//     case ORDRE_SEPARATION:
//         if (SepEffective())
//         {
//             etat = SEPARATION_EFFECTIVE;
// #ifdef SERIAL_ON
//             Serial.println("Séparation effective");
// #endif
//         }
//         // // Pour les tests
//         // etat = SEPARATION_EFFECTIVE;
//         // Serial.println("Séparation effective");
//         // //
//         break;
//     case SEPARATION_EFFECTIVE:
//         pyro.Light(led_sepeffective, 'g');
// #ifdef IS_BETA
//         if (SwitchActivated() && AnglesOK())
//         {
//             digitalWrite(mafpin, HIGH);
//             etat = MISE_A_FEU;
// #ifdef SERIAL_ON
//             Serial.println("Ordre de mise à feu envoyé");
// #endif // SERIAL_ON
//         }
//         // Cas dégradé: Pas de mise à feu du second moteur car theta pas bon
//         // if (millis() - tdec > tmafmax)
//         // {
//         //     etat = NON_FEU;
//         //     Serial.println("Pas de mise à feu du second moteur car theta pas bon");
//         // }
// #else
//         etat = MISE_A_FEU; // Si on est dans alpha, on attend direct qu'on puisse déployer le para
// #endif // IS_BETA
//         break;
//     case MISE_A_FEU:
//         pyro.Light(led_allumage, 'g');
//         if (SwitchActivated())
//         {
//             servo.write(angle_ouvert);
//             etat = PARA_DEPLOYE;
//             Info_out.para = true;
// #ifdef SERIAL_ON
//             Serial.println("Ouverture porte");
// #endif
//         }
//         break;
//     case PARA_DEPLOYE:
// #ifdef IS_BETA
//         pyro.Light(led_porte, 'g');
//         digitalWrite(mafpin, LOW);
//         if (SwitchActivated())
//         {
//             digitalWrite(buzzerPin, HIGH);
//             etat = A_ATTERRI;
//             pyro.Clear();
// #ifdef SERIAL_ON
//             Serial.println("Au sol, Buzzer ON");
// #endif // SERIAL_ON
//         }
// #else
//         etat = A_ATTERRI;
// #endif // IS_BETA
//         break;
//     case NON_FEU:
//         if (millis() - tdec > tservomin_nonfeu)
//         {
//             servo.write(angle_ouvert);
//             etat = PARA_DEPLOYE;
//             Info_out.para = true;
// #ifdef SERIAL_ON
//             Serial.println("Ouverture porte");
// #endif
//         }
//         break;
//     }
//     UpdateLeds();
// }
