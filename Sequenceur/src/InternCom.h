// definition of the serial communication structs for all the cards

// communication Seq <-> Info

struct Seq_to_info_struct
{
    // put your variable definitions here for the data you want to send
    // THIS MUST BE EXACTLY THE SAME ON THE OTHER ARDUINO
    bool a_decolle = false;
    bool a_separe = false;
    bool ordre_maf = false;
    bool apogee_detectee = false;
    bool para = false;
    bool telemON = true; // false quand shunt en place, true quand shunt retiré
    int state_seq = -1;  // a changer si nécessaire
};

struct Info_to_seq_struct
{
    // put your variable definitions here for the data you want to send
    // THIS MUST BE EXACTLY THE SAME ON THE OTHER ARDUINO
    bool flag_pression_parachute_beta = false;
    bool imus_prets = false;
    bool pintOK = false;      // Indique si la pression interne est suffisamment élevée
    bool mafdone = false;     // Vaut true si la pression indique que la mise à feu a eu lieu
    bool gpsOK = false;       // Indique si le GPS est prêt (Alpha)
    bool barometreOK = false; // Indique si le baromètre est prêt (Alpha)
    bool alphaOk = false;
    bool thetaOk = false;

    // Pour les tests
    bool decollage = false;
    bool separation = false;
    bool mise_a_feu = false;
    bool ouverture_porte = false;
    bool au_sol = false;
};

// communication XP <-> Info

struct XP_to_info_struct
{
    // put your variable definitions here for the data you want to send
    // THIS MUST BE EXACTLY THE SAME ON THE OTHER ARDUINO
    bool imus_prets = false;
    int state_xp = -1; // a changer si nécessaire // par exemple mettre l'etat de chaque imu et du micro aussi
    float quaternion[4] = {0, 0, 0, 1};
    // float velocity[3] = {0, 0, 0};
    float position[3] = {0, 0, 0};
    float normAcc = 0;
    float normV = 0;
    // float alpha = 0;
    // float theta = 0;
    bool alphaOk = false;
    bool thetaOk = false;
    // float last_alpha_ref = 0;
    // float last_theta_ref = 0;
    // je met que ça pour l'instant mais on peut rajouter des trucs
};

struct Info_to_xp_struct
{
    // put your variable definitions here for the data you want to send
    // THIS MUST BE EXACTLY THE SAME ON THE OTHER ARDUINO
    bool a_decolle = false;
    bool telemON = true; // false quand shunt en place, true quand shunt retiré
};
