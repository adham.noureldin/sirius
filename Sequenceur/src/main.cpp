#include "Sequenceur.h"

// void playMelody()
// {
//   int melody[] = {262, 294, 330, 349, 392, 440, 494, 523}; // liste des fréquences de la mélodie
//   int noteDuration = 500;                                  // durée des notes

//   for (int i = 0; i < 8; i++) // on joue les notes une par une
//   {
//     tone(buzzerPin, melody[i], noteDuration);
//     delay(noteDuration);
//   }
// }

Sequenceur seq;

void setup()
{
  seq.Setup();
}

void loop()
{
  seq.Loop();
  // seq.LoopTestIsoleSerial();
}