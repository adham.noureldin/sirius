#include "defines.h"
#include "Pyro.h"
#include <ESP32Servo.h>
#include <EasyTransfer.h>
#include "InternCom.h"

#define TIME_BETWEEN_SERIAL_COMMUNICATION 1000 // send and receive message every 1 s // on devrait calculer jusqu'à quelle délais on peut descendre

// #define TEST_COMMANDE_PAR_INFO

class Sequenceur
{
private:
    Pyro pyro;
    Servo servo;
    enum Etat
    {
        AU_SOL,
        A_DECOLLE,
        ORDRE_SEPARATION,
        SEPARATION_EFFECTIVE,
        SEPARATION_NON_EFFECTIVE,
        ORDRE_MAF,
        MISE_A_FEU,
        PARA_DEPLOYE,
        A_ATTERRI,
        NON_FEU
    };
    // Leds pyro;
    const int led_decolle = 4; // CdC: "Sequenceur actif, ligne de maf active"
    const int led_ordresep = 3;
    const int led_sepeffective = 2;
    const int led_allumage = 1;
    const int led_porte = 0; // CdC: "actionneur actif"
    const int led_imusprets = 6;
    const int led_tmafOK = 7;
    const int led_anglesOK = 8;
    const int led_telemON = 12;
    const int led_pintOK = 9;
    const int led_jackdecollage = 11;
    const int led_jacksep = 10;
#ifndef IS_BETA
    const int led_gpsOK = 6;
    const int led_barometreOK = 7;
#endif

    // const

    Etat etat = AU_SOL;
    const int buzzerPin = 2;  // Pin du buzzer
    const int switchPin = 19; // Pin de l'interrupteur
    const int led1Pin = 4;    // Pin de la LED 1
    const int led2Pin = 13;   // Pin de la LED 2
    const int jackpin = 25;
    const int detectionseppin = 33; // Pin pour détecter la séparation
    const int shunttelempin = 14;   // Pin du shunt telem
    const int seppin = 22;          // Pin pour déclencher la séparation
    const int flaggyro = 26;        // il y a du courant dans ce pin quand theta est ok pour l'allumage du moteur
    const int mafpin = 32;          // pin pour l'ordre de mise à feu
    const int servopin = 15;        // Ouverture porte
#ifdef IS_BETA
    const int angle_ferme = 65;   // Angle porte fermée
    const int angle_ouvert = 180; // Angle porte ouverte
#else
    const int angle_ferme = 32; // Angle porte fermée
    const int angle_ouvert = 0; // Angle porte ouverte
#endif
    uint32_t tdec;               // Temps décollage
    const uint32_t dtdec = 300;  // Ecart entre le décollage et la détection du décollage (0.3 s)
    const uint32_t dtpara = 500; // Temps de déploiement des paras (0.5 s)
    uint32_t tsepeffect;         // Temps où la séparation est détectée effective

#ifdef BIETAGE
    const uint32_t tsep = 5800;                       // Temps séparation
    const uint32_t tsepmax = 8000;                    // Temps séparation max
    const uint32_t tmafmin = 6300;                    // Temps mise à feu 2ème moteur minimal. TODO: à changer ?
    const uint32_t tmafmax = 11000;                   // Temps mise à feu 2ème moteur maximal
    const uint32_t tservomin_nonfeu = 14300 - dtpara; // Temps ouverture porte minimmal dans le cas où le moteur ne s'allume pas
    const uint32_t tservomax_nonfeu = 17500 - dtpara; // Temps ouverture porte maximal dans le cas où le moteur ne s'allume pas
#else                                                 // TODO: demander ces temps au stabtraj
    const uint32_t tsep = 4400;                       // Temps séparation
    const uint32_t tsepmax = 8000;                    // Temps séparation max
    const uint32_t tmafmin = 4900;                    // Temps mise à feu 2ème moteur minimal. TODO: à changer ?
    const uint32_t tmafmax = 8000;                    // Temps mise à feu 2ème moteur maximal
    const uint32_t tservomin_nonfeu = 11100 - dtpara; // Temps ouverture porte minimmal dans le cas où le moteur ne s'allume pas
    const uint32_t tservomax_nonfeu = 13500 - dtpara; // Temps ouverture porte maximal dans le cas où le moteur ne s'allume pas
#endif                                                // BIETAGE

    const uint32_t dtmaf = 500;                     // Temps entre la détection de séparation effective et ordre maf
    const uint32_t tparamin1 = tservomin_nonfeu;    // Temps ouverture poorte minimal quand pas de sep
    const uint32_t tparamax1 = tservomax_nonfeu;    // Temps ouverture poorte maximal quand pas de sep
    const uint32_t tbuzzer = 60 * 60000;            // Temps allumage buzzer
    uint32_t t_millis_dernier_declenchement_buzzer; // temps du dernier déclenchement du buzzer
    const uint32_t dt_buzzer_ON = 5000;             // Durée pendant laquelle le buzzer est allumé
    const uint32_t dt_cycle_buzzer = 30000;         // Durée d'un cycle de clignotement du buzzer (allumé-éteint)
    bool buzzerstate = 0;                           // 0 : ON, 1 : OFF
#ifdef IS_BETA
#ifdef VOL_PASSIF
    const int tservomin = 13500 - dtpara; // Temps ouverture porte minimal
    const int tservomax = 17000 - dtpara; // Temps ouverture porte maximal
#else
    const int tservomin = 10000 - dtpara; // Temps ouverture porte minimal. TODO: changer les temps
    const int tservomax = 12000 - dtpara; // Temps ouverture porte maximal. TODO: changer les temps
#endif // VOL_PASSIF
#else
    const int tservomin = 13100 - dtpara; // Temps ouverture porte minimal
    const int tservomax = 15870 - dtpara; // Temps ouverture porte maximal
#endif // IS_BETA

    EasyTransfer ET_Info_in, ET_Info_out;
    Seq_to_info_struct Info_out; // structure qu'on envoie à Info
    Info_to_seq_struct Info_in;  // structure qu'on reçoit de Info
    uint32_t lastSerialCommTime = 0;

    bool etatswitch = 0; // Etat de l'interrupteur, pour les tests
    bool jack_decollage_branche = false;
    uint32_t t_jack_decollage_branche = 0;

    unsigned long long previousMicros = 0;
    int overflowCount = 0; // Nombre de fois que le compteur de micros a overflow

    bool shunttelemtire = false;
    uint32_t tshunttelemtire = 0;
    uint32_t tshunttelemremis = 0;
    bool previous_shunttelemtire = false;

public:
    Sequenceur();
    unsigned long long getMicros();
    void Setup();
    void updateSendReceiveSerialCommunications(bool now = false);
    void TestSwitch();
    bool SepEffective();
    bool TelemON();
    bool IMUs_prets();
    bool AnglesOK();
    bool TmafOK();
    bool DecollageDetecte();
    void UpdateLeds(); // Met à jour l'état des leds ne dépendant pas de l'état de la fusée
    void Loop();
    // bool SwitchActivated();
    // void LoopTestSwitch();
    // int GetInfoOrder();
    // void LoopTestTotalSerial();
    // void LoopTestIsoleSerial();
    void BuzzerBlink();
    void OuverturePorte();
};