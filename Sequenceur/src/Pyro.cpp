#include "Pyro.h"

Pyro::Pyro()
{
}

void Pyro::Init()
{
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS); // GRB ordering is assumed
    FastLED.setBrightness(BRIGHTNESS);
    FastLED.clear();
    for (int i = 0; i < NUM_LEDS; i++)
    {
        Light(i, 'b');
    }
    Light(led_seqON, 'g');
    Light(10, 'o');
    Light(11, 'o');

#ifndef IS_BETA
    Light(1, 'o');
    Light(3, 'o');
    Light(8, 'o');
    Light(9, 'o');
#endif

    FastLED.show();
}

void Pyro::Light(int led, char color)
{
    switch (color)
    {
    case 'r':
        leds[led] = CRGB::Red;
        break;
    case 'g':
        leds[led] = CRGB::Green;
        break;
    case 'b':
        leds[led] = CRGB::Blue;
        break;
    case 'p':
        leds[led] = CRGB::Purple;
        break;
    case 'y':
        // leds[led] = CRGB(255, 165, 0); // Orange
        // leds[led] = CHSV(HUE_ORANGE, 255, 255); // Orange
        leds[led] = CRGB::Orange;
        break;
    case 'o':
        leds[led] = CRGB::Black;
        break;
    default:
        leds[led] = CRGB::Black;
        break;
    }
    FastLED.show();
}

void Pyro::Clear()
{
    FastLED.clear();
    FastLED.show();
}