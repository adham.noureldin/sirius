#include "Arduino.h"
#include <ESP32Servo.h>
#include <esp_now.h>
#include <WiFi.h>

// A changer pour chaque carte ---------------------------------
// #define EMITTER
// #define RECEIVER_ALPHA
#define RECEIVER_BETA
//--------------------------------------------------------------
#if defined(RECEIVER_ALPHA) || defined(RECEIVER_BETA)
#define RECEIVER
#endif

#ifdef RECEIVER_BETA
const int angle_ferme = 45;   // Angle porte fermée
const int angle_ouvert = 180; // Angle porte ouverte
#elif defined(RECEIVER_ALPHA)
const int angle_ferme = 75;  // Angle porte fermée
const int angle_ouvert = 27; // Angle porte ouverte
#endif

#ifdef RECEIVER
const int seppin = 22;
const int servopin = 15;
const int buzzerpin = 2;
Servo servo;
#endif

#ifdef EMITTER
// REPLACE WITH THE MAC Address of your receiver
uint8_t broadcastAddress_alpha[] = {0xD4, 0x8A, 0xFC, 0xA1, 0x83, 0xB4};
uint8_t broadcastAddress_beta[] = {0xD4, 0x8A, 0xFC, 0xA0, 0x2D, 0xE8};
; // TODO
// Variable to store if sending data was successful
String success;
#endif

bool para_alpha = false;
bool para_beta = false;
bool sep = false;
bool buzzer_beta = false;

// Structure example to send data
// Must match the receiver structure
typedef struct struct_message
{
  bool para_alpha;
  bool para_beta;
  bool sep;
  bool buzzer_beta;
} struct_message;

// struct to send readings
struct_message DataToSend;

// Create a struct_message to hold incoming sensor readings
struct_message incomingReadings;

esp_now_peer_info_t peerInfo;

#ifdef EMITTER
// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status)
{
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  if (status == 0)
  {
    success = "Delivery Success :)";
  }
  else
  {
    success = "Delivery Fail :(";
  }
}
#endif
#ifdef RECEIVER
// Callback when data is received
void OnDataRecv(const uint8_t *mac, const uint8_t *incomingData, int len)
{
  memcpy(&incomingReadings, incomingData, sizeof(incomingReadings));
  Serial.print("Bytes received: ");
  Serial.println(len);
  para_alpha = incomingReadings.para_alpha;
  para_beta = incomingReadings.para_beta;
  sep = incomingReadings.sep;
  buzzer_beta = incomingReadings.buzzer_beta;
  Serial.print("Para alpha: ");
  Serial.println(para_alpha);
  Serial.print("Para beta: ");
  Serial.println(para_beta);
  Serial.print("Sep: ");
  Serial.println(sep);
  Serial.print("Buzzer beta: ");
  Serial.println(buzzer_beta);
}
#endif
void setup()
{
  // Init Serial Monitor
  Serial.begin(115200);

  delay(1000);
  Serial.println("Begin");
#ifdef RECEIVER
  pinMode(servopin, OUTPUT);
  servo.attach(servopin);
  servo.write(angle_ferme);

  pinMode(seppin, OUTPUT);
  digitalWrite(seppin, LOW);

  pinMode(buzzerpin, OUTPUT);
  digitalWrite(buzzerpin, LOW);
#endif

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK)
  {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

// Once ESPNow is successfully Init, we will register for Send CB to
// get the status of Trasnmitted packet
#ifdef EMITTER
  esp_now_register_send_cb(OnDataSent);

  // Register peer
  memcpy(peerInfo.peer_addr, broadcastAddress_alpha, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  // Add peer
  if (esp_now_add_peer(&peerInfo) != ESP_OK)
  {
    Serial.println("Failed to add peer alpha");
    return;
  }

  memcpy(peerInfo.peer_addr, broadcastAddress_beta, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  // Add peer
  if (esp_now_add_peer(&peerInfo) != ESP_OK)
  {
    Serial.println("Failed to add peer beta");
    return;
  }
#else
  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);
#endif
}

#ifdef EMITTER
void getReadings()
{
  esp_err_t result;
  if (Serial.available())
  {
    char temp = Serial.read();
    if (temp == 'P')
    {
      DataToSend.para_beta = !DataToSend.para_beta;
      Serial.println("Send para beta activated");
      result = esp_now_send(broadcastAddress_beta, (uint8_t *)&DataToSend, sizeof(DataToSend));
    }
    else if (temp == 'S')
    {
      DataToSend.sep = !DataToSend.sep;
      Serial.println("Send separation");
      result = esp_now_send(broadcastAddress_beta, (uint8_t *)&DataToSend, sizeof(DataToSend));
    }
    else if (temp == 'p')
    {
      DataToSend.para_alpha = !DataToSend.para_alpha;
      Serial.println("Send para alpha activated");
      result = esp_now_send(broadcastAddress_alpha, (uint8_t *)&DataToSend, sizeof(DataToSend));
    }
    else if (temp == 'B')
    {
      DataToSend.buzzer_beta = !DataToSend.buzzer_beta;
      Serial.println("Send buzzer beta activated");
      result = esp_now_send(broadcastAddress_beta, (uint8_t *)&DataToSend, sizeof(DataToSend));
    }
    else
    {
      Serial.println("Invalid Input");
    }
    if (result == ESP_OK)
    {
      Serial.println("Sent with success");
    }
    else
    {
      Serial.println("Error sending the data");
    }
    // print state of the struct
    Serial.print("para beta: ");
    Serial.println(DataToSend.para_beta);
    Serial.print("para alpha: ");
    Serial.println(DataToSend.para_alpha);
    Serial.print("separation: ");
    Serial.println(DataToSend.sep);
    Serial.print("buzzer beta: ");
    Serial.println(DataToSend.buzzer_beta);
  }
}
#endif

#ifdef RECEIVER
void activateSepParaBuzzer()
{
#ifdef RECEIVER_ALPHA
  if (para_alpha)
  {
    Serial.println("Para is activated");
    servo.write(angle_ouvert);
  }
  else
  {
    Serial.println("Para is closed");
    servo.write(angle_ferme);
  }
#endif
#ifdef RECEIVER_BETA
  if (para_beta)
  {
    Serial.println("Para is activated");
    servo.write(angle_ouvert);
  }
  else
  {
    Serial.println("Para is closed");
    servo.write(angle_ferme);
  }
  if (sep)
  {
    Serial.println("Sep is activated");
    digitalWrite(seppin, HIGH);
  }
  else
  {
    Serial.println("Sep is closed");
    digitalWrite(seppin, LOW);
  }
  if (buzzer_beta)
  {
    Serial.println("Buzzer is activated");
    digitalWrite(buzzerpin, HIGH);
  }
  else
  {
    Serial.println("Buzzer is not activated");
    digitalWrite(buzzerpin, LOW);
  }
#endif
}
#endif

void loop()
{
#ifdef EMITTER
  getReadings();
#endif
#ifdef RECEIVER
  activateSepParaBuzzer();
#endif
}
