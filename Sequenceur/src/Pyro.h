#include "defines.h"

#include <FastLED.h>

#define NUM_LEDS 13 // Nombre de LEDs
#define DATA_PIN 27

#define BRIGHTNESS 255

class Pyro
{
private:
    CRGB leds[NUM_LEDS];
    const int led_seqON = 5;

public:
    Pyro();
    void Init();
    void Light(int led, char color);
    void Clear();
};