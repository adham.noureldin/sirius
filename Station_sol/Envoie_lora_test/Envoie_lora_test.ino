uint8_t reverse_bits(uint8_t byte) {
    byte = (byte & 0xF0) >> 4 | (byte & 0x0F) << 4; // Swap nibbles
    byte = (byte & 0xCC) >> 2 | (byte & 0x33) << 2; // Swap pairs of bits
    byte = (byte & 0xAA) >> 1 | (byte & 0x55) << 1; // Swap individual bits
    return byte;
}

// Function to reverse the bits of each element in the array
void reverse_bits_in_array(uint8_t* array, size_t length) {
    for (size_t i = 0; i < length; i++) {
        array[i] = reverse_bits(array[i]);
    }
}

uint8_t fromUint7ToUint8(uint8_t data) {
    // reverse the bits of the data
    data = reverse_bits(data);
    // shift the data to the right by 1 bit
    data >>= 1;
    return data;
}

uint8_t fromUint4ToUint8(uint8_t data) {
    // reverse the bits of the data
    data = reverse_bits(data);
    // shift the data to the right by 1 bit
    data >>= 4;
    return data;
}

uint16_t fromFloatToUint16_t(float data) {
    union {
        float f;
        uint32_t u;
    } f2u = { .f = data };

    uint16_t sign = (f2u.u >> 31) & 0x0001;
    uint16_t exponent = (f2u.u >> 23) & 0x00FF;
    uint32_t mantissa = f2u.u & 0x007FFFFF;

    uint16_t hfloat = 0;
    if (exponent == 0 && mantissa == 0) {
        // Zero
        hfloat = sign << 15;
    } else if (exponent == 0xFF) {
        // Infinity or NaN
        hfloat = (sign << 15) | 0x7C00 | (mantissa >> 13);
    } else {
        // Normalized number
        exponent -= 127;
        exponent += 15;
        if (exponent >= 31) {
            // Overflow, return infinity
            hfloat = (sign << 15) | 0x7C00;
        } else if (exponent <= 0) {
            // Underflow
            if (exponent < -10) {
                // Too small for subnormal representation, return zero
                hfloat = sign << 15;
            } else {
                mantissa |= 0x00800000;
                uint32_t shift = 14 - exponent;
                hfloat = (sign << 15) | (mantissa >> shift);
                if ((mantissa >> (shift - 1)) & 0x0001)
                    hfloat += 1;
            }
        } else {
            hfloat = (sign << 15) | (exponent << 10) | (mantissa >> 13);
            if (mantissa & 0x00001000)
                hfloat += 1;
        }
    }
    reverse_bits_in_array((uint8_t *)&hfloat, sizeof(hfloat));
    return hfloat;
}

uint32_t fromFloatToUint32_t(float data){
union {
        float f;
        uint32_t u;
    } f2u = { .f = data };
    reverse_bits_in_array((uint8_t *)&f2u.u, sizeof(f2u.u));
return f2u.u;
}

void toHex(void *data, size_t len, char *out) {
    static const char hex_table[] = "0123456789ABCDEF";  // Lookup table for hex digits
    
    uint8_t *byteData = (uint8_t *)data;
    for (size_t i = 0; i < len; i++) {
        uint8_t byte = byteData[i];

        // Convert the most significant nibble (4 bits) to hex
        *out++ = hex_table[(byte >> 4) & 0xF];

        // Convert the least significant nibble (4 bits) to hex
        *out++ = hex_table[byte & 0xF];
    }

    // Add null terminator for the string
    *out = '\0';
}



void toBin(void *data, size_t len, char *out) {
    static const char bin_table[] = "01";  // Lookup table for binary digits

    uint8_t *byteData = (uint8_t *)data;
    for (size_t i = 0; i < len; i++) {
        uint8_t byte = byteData[i];  // Access the byte in the natural order

        // Convert each bit to its binary representation
        for (int j = 7; j >= 0; j--) {
            *out++ = bin_table[(byte >> j) & 0x1];
        }
    }

    // Add null terminator for the string
    *out = '\0';
}

struct __attribute__((packed)) LoRa_data_beta {
  //uint8_t padding:6;
    uint8_t AB:4; // envoyer 1111 pour beta et 0000 pour alpha. permet de les distinguer et verifier si le packet est corrompu
    uint8_t separation_state : 1;
    uint8_t seq_state_beta : 1;
    uint8_t info_state_beta : 1;
    uint8_t exp_state_beta : 1;
    uint16_t normAcc:16; // moyénné sur 1s par exemple ou moins (convertis de float en float16 puis cast en uint16_t)
    uint16_t normV:16; // grâce à l'integration des imus (convertis de float en float16 puis cast en uint16_t)
    uint16_t q0:16; // possible plutot d'envoyer des angles d'euler, ce qui fait economiser 16bits mais possible perte en precision
    uint16_t q1:16; // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t q2:16; // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t q3:16; // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t px:16; // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t py:16; // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t pz:16; // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t pressure:16; // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint16_t pressure_reservoir:16; // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
    uint32_t latitude_beta : 32; // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint32_t longitude_beta : 32; // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint8_t num_satellites_beta : 4; // between 0 and 15
    uint8_t time_since_launch_beta : 7; // between 0s to 127s
    uint8_t temperature: 7; // uint between 0 and 127
    //uint8_t padding:6;
    
}; // number of bits : 266. mettre SF11 et 125kHz de bande passante et un envoie par seconde

struct __attribute__((packed)) LoRa_data_alpha {
    uint8_t AB:4; // envoyer 1111 pour beta et 0000 pour alpha. permet de les distinguer et verifier si le packet est corrompu
    uint8_t seq_state_alpha : 1;
    uint8_t info_state_alpha : 1;
    uint32_t latitude_alpha : 32; // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint32_t longitude_alpha : 32; // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
    uint8_t num_satellites_alpha : 4; // between 0 and 15
    uint8_t time_since_launch_alpha : 7; // between 0s to 127s
};


LoRa_data_beta data_beta;
LoRa_data_alpha data_alpha;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  data_beta.AB = fromUint4ToUint8(0b1111);
  data_beta.separation_state = 0;
  data_beta.seq_state_beta = 0;
  data_beta.info_state_beta = 0;
  data_beta.exp_state_beta = 1;
  data_beta.normAcc = fromFloatToUint16_t(10.20);
  data_beta.normV = fromFloatToUint16_t(11.20);
  data_beta.q0 = fromFloatToUint16_t(0.20/4.069397989875161);
  data_beta.q1 = fromFloatToUint16_t(1.20/4.069397989875161);
  data_beta.q2 = fromFloatToUint16_t(2.20/4.069397989875161);
  data_beta.q3 = fromFloatToUint16_t(3.20/4.069397989875161);
  data_beta.px = fromFloatToUint16_t(4.20);
  data_beta.py = fromFloatToUint16_t(5.20);
  data_beta.pz = fromFloatToUint16_t(6.20);
  data_beta.pressure = fromFloatToUint16_t(7.20);
  data_beta.pressure_reservoir = fromFloatToUint16_t(8.20);
  data_beta.latitude_beta = fromFloatToUint32_t(9.20);
  data_beta.longitude_beta = fromFloatToUint32_t(10.20);
  data_beta.num_satellites_beta = fromUint4ToUint8(10);
  data_beta.time_since_launch_beta = fromUint7ToUint8(11);
  data_beta.temperature = fromUint7ToUint8(25);

//   data_beta.AB = 0b1111;
//   data_beta.separation_state = 0b1;
//   data_beta.seq_state_beta = 0b1;
//   data_beta.info_state_beta = 0b1;
//   data_beta.exp_state_beta = 0b1;
//   data_beta.normAcc = 0b1111111111111111;
//   data_beta.normV = 0b1111111111111111;
//   data_beta.q0 = 0b1111111111111111;
//   data_beta.q1 = 0b1111111111111111;
//   data_beta.q2 = 0b1111111111111111;
//   data_beta.q3 = 0b1111111111111111;
//   data_beta.px = 0b1111111111111111;
//   data_beta.py = 0b1111111111111111;
//   data_beta.pz = 0b1111111111111111;
//   data_beta.pressure = 0b1111111111111111;
//   data_beta.pressure_reservoir = 0b1111111111111111;
//   data_beta.latitude_beta = 0b11111111111111111111111111111111;
//   data_beta.longitude_beta = 0b11111111111111111111111111111111;
//   data_beta.num_satellites_beta = 0b1111;
//   data_beta.time_since_launch_beta = 0b1111111;
//   data_beta.temperature = 0b1111111;
  //data_beta.padding = 0b000000;


  data_alpha.AB = 0b0000;
  data_alpha.seq_state_alpha = 0;
  data_alpha.info_state_alpha = 0;
  data_alpha.latitude_alpha = fromFloatToUint32_t(12.20);
  data_alpha.longitude_alpha = fromFloatToUint32_t(13.20);
  data_alpha.num_satellites_alpha = 1;
  data_alpha.time_since_launch_alpha = 1;

  
  //Serial3.begin(115200);  // initialize the LoRa serial interface
  //delay(200);
  //Serial3.write("AT+MODE=TEST\r\n"); // test mode
  //delay(200);
  // AT+TEST=RFCFG,[FREQUENCY],[SF],[BANDWIDTH],[TX PR],[RX PR],[TX POWER],[CRC],[IQ],[NET]
  //Ici, on vient définir nos paramètres d'émission, tel que la fréquence (868MHz), 
  //le Spreading Factor SF (allant de SF7 à SF12, plus il est haut, plus on a de portée,
  //mais moins on a de bitrate), la bande passante (plus elle est grande, plus on a de
  // bitrate) mais moins on a de portée. La bande passante va de
  //125kHz à 500kHz. Le reste on touche pas.
  // Serial3.println("AT+TEST=RFCFG," LORA_FREQ ",SF" LORA_SF "," LORA_BW ",8,8,12,ON,OFF,OFF\r\n");
  //Serial3.write("AT+TEST=RFCFG,868,SF10,125,8,8,12,ON,OFF,OFF\r\n");
  //delay(200);
    // put your main code here, to run repeatedly:
  // Serial.println("Hello World");
  // delay(1000);
  reverse_bits_in_array((uint8_t *)&data_beta, sizeof(data_beta));
char hex[100];
  toHex(&data_beta, sizeof(data_beta), hex);

char bin[800];
    toBin((uint8_t*)&data_beta, sizeof(data_beta), bin);
  //Serial3.print("AT+TEST=TXLRPKT,");
  //Serial3.print(hex);
  //Serial3.print("\r\n");
Serial.println(hex);
    Serial.println(bin);
  //delay(1000);

  

}

void loop() {

  
}
