import numpy as np
import regex
import struct

def p_to_alt(pressure):
    return (1-pow((pressure/1013.25),0.190284))*145366.45*.3048

def type_to_value(type, bits_line, current_bit):
    if type == "uint4_t":
        return int(bits_line[current_bit:current_bit+4], 2)
    elif type == "bool":
        return bool(int(bits_line[current_bit:current_bit+1], 2))
    elif type == "float16":
        return struct.unpack('e', struct.pack('H', int(bits_line[current_bit:current_bit+16][::-1], 2)))[0]
    elif type == "float32":
        # print("float32 ",bytes.fromhex(hex(int(bits_line[current_bit:current_bit+32], 2))[2:]))
        bits = bits_line[current_bit:current_bit+32]
        bits = bits[16:32][::-1] + bits[0:16][::-1] # fuck les conventions, ils peuvent pas tous se mettre d'accord ptn
        return struct.unpack('f', struct.pack('I', int(bits, 2)))[0]
    elif type == "uint7_t":
        return int(bits_line[current_bit:current_bit+7], 2)
    elif type == "uint16_t":
        return int(bits_line[current_bit:current_bit+16], 2)
    elif type == "uint32_t":
        return int(bits_line[current_bit:current_bit+32], 2)
    elif type == "uint8_t":
        return int(bits_line[current_bit:current_bit+8], 2)
    else:
        return None



def type_to_bits(type):
    if type == "uint4_t":
        return 4
    elif type == "bool":
        return 1
    elif type == "float16":
        return 16
    elif type == "float32":
        return 32
    elif type == "uint7_t":
        return 7
    elif type == "uint16_t":
        return 16
    elif type == "uint32_t":
        return 32
    elif type == "uint8_t":
        return 8
    else:
        return None    

def length_message_in_bits(bytes_line):
    return len(bytes_line)*8

# Function to convert raw serial data to a dictionary of values
def parse_serial_data_test(line):
    # line = line.decode("utf-8")  # convert bytes array to string
    # example : line = "+TEST: RX "0F001A499A494A2AB83453384A3A33443345334633471A4833331341333323410A0B1900"

# struct __attribute__((packed)) LoRa_data_alpha_beta // pour les deux
# {
#     uint8_t count : 4; // permet de savoir exactement quels packets on a reçu
#     uint8_t seq_state : 1;
#     uint8_t info_state : 1;
#     uint32_t latitude : 32;        // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
#     uint32_t longitude : 32;       // utiliser fromFloatToUint32_t pour convertir les float en uint32_t
#     uint8_t num_satellites : 4;    // between 0 and 15
#     uint8_t time_since_launch : 7; // between 0s to 127s
#     uint16_t pressure : 16;         // utiliser fromFloatToUint16_t pour convertir les float en uint16_t
#     uint8_t temperature : 7;        // uint between 0 and 127
#     bool parachute_state : 1;
# #ifndef IS_BETA
#     uint8_t padding_to_discern_alpha_beta : 8; // la taille des messages sera differents pour alpha et beta
# #endif
# }; // mettre SF10 et 125kHz de bande passante et un envoie par secondee SF10 et 125kHz de bande passante et un envoie par seconde

    types_beta = ["uint4_t","bool","bool",
                    "float16","float16","float16","float16","float16","float16",
                    "float16","float16","float16",
                    "uint7_t","bool"]
    types_both = ["uint4_t","bool","bool","float32","float32","uint4_t","float32","uint7_t","float16","uint7_t","bool"]
    types_both_alpha = types_both + ["uint8_t"]

    length_beta= sum([type_to_bits(type) for type in types_beta])
    length_both_beta = sum([type_to_bits(type) for type in types_both])
    length_both_alpha = sum([type_to_bits(type) for type in types_both_alpha])

    # process data
    if line[:11] == "+TEST: RX \"":
        line = line.strip()[11:-1]
        # print(line)
        values_both_keys = ['count','seq_state', 'info_state', 'latitude', 'longitude', 'number_satellites', 'gpstime', 'time_since_launch','pressure','temperature','parachute_state']
        values_beta_keys = ['count','separation_state', 'exp_state',
                        'normAcc', 'normV',
                        'q0', 'q1', 'q2', 'q3',
                        'px','py','pz',
                        'pressure_reservoir','allumage_moteur']


        # print((length_beta+(8-length_beta%8))/8)
        # print((length_both_beta+(8-length_both_beta%8))/8)
        # print((length_both_alpha+(8-length_both_alpha%8))/8)

        # print(len(line))

        values = {}

        # check if message is alpha_beta_alpha, alpha_beta_beta or beta
        if (length_beta+(8-length_beta%8))/8*2 == len(line):
            types = types_beta
            values_keys = values_beta_keys
            values['alpha_or_beta'] = "beta"
        elif (length_both_beta+(8-length_both_beta%8))/8*2 == len(line):
            types = types_both
            values_keys = values_both_keys
            values['alpha_or_beta'] = "both_beta"
        elif (length_both_alpha+(8-length_both_alpha%8))/8*2 == len(line):
            types = types_both
            values_keys = values_both_keys
            values['alpha_or_beta'] = "alpha"
        else:
            print("WTF")

        # print(values['alpha_or_beta'])


        bits_line = ''.join([format(int(c, 16), '04b') for c in line])
        # print(bits_line)
        # print(bytes_line)

        current_bit = 0
        for i in range(len(types)):
            type_i = types[i]
            bits = type_to_bits(type_i)
            value = type_to_value(type_i, bits_line, current_bit)
            values[values_keys[i]] = value
            # if values_keys[i-1]=="pressure":
            #     print(bits_line[current_bit:current_bit+16])
            # if values_keys[i-1]=="temperature":
            #     print(bits_line[current_bit:current_bit+7])
            current_bit += bits
    
        if values['alpha_or_beta'] == "beta":
            values["assiette"] = 180/np.pi * (np.pi/2-np.arccos(1.0 - 2.0 * (values["q0"]*values["q0"] + values["q1"]*values["q1"])))      
            values["azimuth"] = 180/np.pi * np.arctan2(-2.0*(values["q1"]*values["q3"]+values["q2"]*values["q0"]),2.0*(values["q2"]*values["q3"]-values["q1"]*values["q0"]))
        if values['alpha_or_beta'] != "beta":
            # from pressure and atmospheric model, get altitude
            values["altitude"] = p_to_alt(values["pressure"])
            
            # gpstime to hour minute second
            # char sz[32];
            # sprintf(sz, "%02d:%02d:%02d ", t.hour(), t.minute(), t.second());
            # Serial.print(sz);
            values["gpstime_hour"] = values["gpstime"]//3600
            values["gpstime_minute"] = (values["gpstime"]%3600)//60
            values["gpstime_second"] = values["gpstime"]%60
    elif(line[:11] == "+TEST: LEN:"):
        # # get len, rssi and snr using a regex expression to get all numbers. numbers could be either positive or negative
        line = line.strip()[11:]
        ints = [int(i) for i in regex.findall(r'[+-]?\d+', line)]
        values = {}
        values['len'] = ints[0]

        if (length_beta+(8-length_beta%8))/8 == values['len']:
            values['alpha_or_beta'] = "beta"
        elif (length_both_beta+(8-length_both_beta%8))/8 == values['len']:
            values['alpha_or_beta'] = "both_beta"
        elif (length_both_alpha+(8-length_both_alpha%8))/8 == values['len']:
            values['alpha_or_beta'] = "alpha"
        values['rssi'] = ints[1]
        values['snr'] = ints[2]
    else:
        values = None
    return values

        
# if main
if __name__ == "__main__":
    line = "+TEST: RX \"8C56AD090B2CF540094009E31F8000\""
    values = parse_serial_data_test(line)
    print(values)
