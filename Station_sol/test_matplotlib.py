import serial
import multiprocessing
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np
import meshio
from scipy.spatial.transform import Rotation as R
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import sys
import time
import mplcyberpunk
import serial.tools.list_ports
import matplotlib.gridspec as gridspec
from parse_lora import *
import time
from datetime import datetime
import traceback

buffer_time = 30  # in seconds. période temporel à afficher sur les graphes

WITH_SERIAL = True

speed_multiplier_for_file = 5.0  # vitesse de replay du fichier
filename = "data_lora/lora_data_2024_07_11_13_15_39.txt"
# noms des ports serial, à remplacer par "COM3" par exemple
serial_port1 = "COM16"  # beta
serial_port2 = "/dev/ttyUSB1"  # alpha

# TODO :
# pouvoir choisir la direction initiale de la fusée (pas besoin)
# check pourquoi il y'a un giga decalage
# temps au lieu de N_data

plt.style.use("cyberpunk")
# plt.style.use('qb-dark.mplstyle')
mpl.rcParams["toolbar"] = "None"


def OpenSerialPort(port=""):
    print("Open port %s" % port)

    fio2_ser = None

    try:
        fio2_ser = serial.Serial(
            port,
            baudrate=115200,
            # timeout=1
        )

    except serial.SerialException as msg:
        print("Error opening serial port %s" % msg)

    except:
        exctype, errorMsg = sys.exc_info()[:2]
        print("%s  %s" % (errorMsg, exctype))

    return fio2_ser


# Function to convert raw serial data to a dictionary of values
def read_from_lora(serial_ports, queues):
    # Open serial ports
    ser1 = serial.Serial(serial_ports[0], 115200)

    # flush ser1
    while ser1.in_waiting > 0:
        ser1.readline()

    ser1.write(b"AT+MODE=TEST\n")
    time.sleep(0.3)
    ser1.write(b"AT+TEST=RFCFG,868.438,SF10,125,8,8,8,ON,OFF,OFF\n")
    time.sleep(0.3)
    ser1.write(b"AT+TEST=RXLRPKT\n")
    try:
        ser2 = serial.Serial(serial_ports[1], 115200)

        # flush ser2
        while ser2.in_waiting > 0:
            ser2.readline()

        ser2.write(b"AT+MODE=TEST\n")
        time.sleep(0.3)
        ser2.write(b"AT+TEST=RFCFG,868.313,SF10,125,8,8,8,ON,OFF,OFF\n")
        time.sleep(0.3)
        ser2.write(b"AT+TEST=RXLRPKT\n")

        sers = [ser1, ser2]
    except:
        sers = [ser1]

    # Create a file with the current date and time
    current_time = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    filename = f"data_lora/lora_data_{current_time}.txt"
    with open(filename, "w") as file:
        # write start time
        file.write(f"Start time: {current_time}\n")
        file.flush()
        while True:
            for ser in sers:
                try:
                    if ser.in_waiting > 0:
                        try:
                            line = ser.readline().decode()
                            # Write line to file with timestamp
                            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")[
                                :-3
                            ]
                            file.write(f"{timestamp} - {line}")
                            file.flush()
                            print(line)
                        except Exception as e:
                            exctype, errorMsg = sys.exc_info()[:2]
                            print(f"Error reading port - {errorMsg}")
                            continue

                        print("passed line read")

                        if not line:
                            continue

                        print("passed line not empty")

                        values = parse_serial_data_test(line)
                        print(values)
                        if values is None:
                            print(line)
                            print("None")
                            continue
                        else:
                            values["time"] = time.time()
                            print(values)
                            if "len" in values:
                                queues["snr"].put(values)
                            elif values["alpha_or_beta"] == "beta":
                                queues["beta"].put(values)
                            elif values["alpha_or_beta"] == "both_beta":
                                queues["both_beta"].put(values)
                            elif values["alpha_or_beta"] == "alpha":
                                queues["alpha"].put(values)
                            else:
                                print("WTF: data alpha or beta")
                except Exception as e:
                    print(f"Error reading data - {e}")
                    print(print(traceback.format_exc()))
                    continue


# Function to convert raw serial data to a dictionary of values
def read_from_file(filename, queues):
    time_last = None
    global time_init
    global time_init_real
    with open(filename, "r") as file:
        for line in file:
            if not line:
                continue

            # split line on " - "
            split_line = line.split(" - ")
            if len(split_line) < 2:
                continue
            time_line = split_line[0]  # format 2024-07-07 13:55:34.309
            # convert to seconds
            time_line_seconds = time.mktime(
                datetime.strptime(time_line, "%Y-%m-%d %H:%M:%S.%f").timetuple()
            )
            if time_last is None:
                time_last = time_line_seconds
                time_init = time_line_seconds
                time_init_real = time.time()

            # delay by time_line_seconds - time_last
            time.sleep((time_line_seconds - time_last) / speed_multiplier_for_file)
            time_last = time_line_seconds
            line = split_line[1]
            values = parse_serial_data_test(line)
            if values is None:
                print(line)
                print("None")
                continue
            else:
                values["time"] = time_line_seconds - time_init
                if "len" in values:
                    queues["snr"].put(values)
                elif values["alpha_or_beta"] == "beta":
                    queues["beta"].put(values)
                elif values["alpha_or_beta"] == "both_beta":
                    queues["both_beta"].put(values)
                elif values["alpha_or_beta"] == "alpha":
                    queues["alpha"].put(values)
                else:
                    print("WTF: data alpha or beta")


# Function to read from the serial port
# def read_from_serial(serial_port, queues):
#     ser = serial.Serial(serial_port, 115200)  # Adjust baud rate as necessary
#     while True:
#         if ser.in_waiting > 0:
#             try:
#                 lines = ser.read(ser.in_waiting)
#                 lines = lines.split(b'\r\n')

#             except:
#                 exctype, errorMsg = sys.exc_info()[:2]
#                 print(f"Error reading port - {errorMsg}")
#             if len(lines) == 0:
#                 continue

#             for line in lines[:-1]:
#                 if len(line) > 0:
#                     values = line.decode().split(',')
#                     if len(values) != 4:
#                         print("data length not 4")
#                         continue
#                     queue.put(values)
#                 else:
#                     print("line empty")


# Function to convert raw serial data to a dictionary of values
def read_fix(queues):

    lines = ['+TEST: RX "F49C4B90A8BA735B34F271DB363C9AA03C2FE843191990"\r\n']

    for line in lines:
        if line is None:
            continue
        if len(line) == 0:
            continue
        # if line[:10] == "+TEST: LEN":
        values = parse_serial_data_test(line)
        # print(values)
        if values is None:
            print(line)
            print("None")
            continue
        else:
            values["time"] = time.time() - init_time
            values["q0"] = 0
            values["q1"] = 0
            values["q2"] = 0
            values["q3"] = 1
            # print(values)
            if "len" in values:
                queues["snr"].put(values)
            elif values["alpha_or_beta"] == "beta":
                queues["beta"].put(values)
            elif values["alpha_or_beta"] == "both_beta":
                queues["both_beta"].put(values)
            elif values["alpha_or_beta"] == "alpha":
                queues["alpha"].put(values)
            else:
                print("WTF: data alpha or beta")


# test when not using serial
# def read_random(queue):
#     while True:
#         values = np.random.uniform(-1, 1, size=4)
#         # normalize values
#         values /= np.linalg.norm(values)
#         queue.put(values)
#         time.sleep(0.1)


def reajust_xlimits(ax, fixed_xlim):
    pass
    # # Get the current limits
    # current_xlim = ax.get_xlim()
    # # Set the new limits, ensuring they do not exceed the fixed limits
    # new_xlim = (max(fixed_xlim[0], current_xlim[0]), min(fixed_xlim[1], current_xlim[1]))
    # ax.set_xlim(new_xlim)


# Function to rotate the mesh
def rotate_mesh(vertices, q):
    return R.from_quat(q).apply(vertices)


# Function to update the plot
def update(
    frame,
    axs,
    mesh,
    queues,
    datas,
    face_collection,
    lines_axs,
    projection_collection,
    texts_collection,
):
    max_points = (
        10  # Adjust this value to limit the number of data points processed per frame
    )
    new_data_snr_alpha = False
    new_data_snr_both_beta = False
    new_data_snr_beta = False
    new_data_alpha = False
    new_data_both_beta = False
    new_data_beta = False

    for _ in range(max_points):
        try:
            # check si on a reçu un message de type  "+TEST: LEN:15, RSSI:-78, SNR:9"
            if not queues["snr"].empty():
                values_snr = queues["snr"].get()
                # check si c'est la première fois qu'on reçoit un message de ce type, auqeul cas on initialize le dict de buffers datas['snr']
                if "len" not in datas["snr"]:
                    for (
                        key
                    ) in (
                        values_snr
                    ):  # les clés de datas['snr'] sont les mêmes que celles de values_snr
                        datas["snr"][key] = []
                for key in values_snr:  # on ajoute les valeurs reçues dans les buffers
                    datas["snr"][key].append(values_snr[key])
                    # if len(datas['snr'][key]) > N_data: # on garde que les N_data dernières valeurs
                    #     datas['snr'][key].pop(0)

                if datas["snr"]["time"][-1] - datas["snr"]["time"][0] >= buffer_time:
                    for key in datas["snr"]:
                        datas["snr"][key].pop(0)

                # on check si on a reçu un message de type alpha ou beta ou both_beta
                if values_snr["alpha_or_beta"] == "alpha":
                    new_data_snr_both_alpha = True
                elif values_snr["alpha_or_beta"] == "beta":
                    new_data_snr_beta = True
                elif values_snr["alpha_or_beta"] == "both_beta":
                    new_data_snr_both_beta = True
                else:
                    print("WTF: snr alpha or beta")

            # on check si on a reçu un message de type "+TEST: RX \"8C56AD090B2CF540094009E31F8000\""
            # qui contient les mêmes données pour les deux étages
            if not queues["alpha"].empty():
                values_alpha = queues["alpha"].get()
                # check si c'est la première fois qu'on reçoit un message de ce type, auquel cas on initialize le dict de buffers datas['alpha']
                if "count" not in datas["alpha"]:
                    for (
                        key
                    ) in (
                        values_alpha
                    ):  # les clés de datas['alpha'] sont les mêmes que celles de values_alpha
                        datas["alpha"][key] = []
                    datas["alpha"]["v_pression"] = []
                for (
                    key
                ) in values_alpha:  # on ajoute les valeurs reçues dans les buffers
                    datas["alpha"][key].append(values_alpha[key])
                    # if len(datas['alpha'][key]) > N_data: # on garde que les N_data dernières valeurs
                    #     datas['alpha'][key].pop(0)

                if len(datas["alpha"]["altitude"]) > 1:
                    datas["alpha"]["v_pression"].append(
                        (
                            datas["alpha"]["altitude"][-1]
                            - datas["alpha"]["altitude"][-2]
                        )
                        / (datas["alpha"]["time"][-1] - datas["alpha"]["time"][-2])
                    )
                else:
                    datas["alpha"]["v_pression"].append(0)
                # if len(datas['alpha']["v_pression"]) > N_data:
                #     datas['alpha']["v_pression"].pop(0)
                if (
                    datas["alpha"]["time"][-1] - datas["alpha"]["time"][0]
                    >= buffer_time
                ):
                    for key in datas["alpha"]:
                        datas["alpha"][key].pop(0)
                new_data_alpha = True

            # on check si on a reçu un message de type "+TEST: RX \"8C56AD090B2CF540094009E31F8000\""
            # qui contient les mêmes données pour les deux étages
            if not queues["both_beta"].empty():
                values_both_beta = queues["both_beta"].get()
                # check si c'est la première fois qu'on reçoit un message de ce type, auquel cas on initialize le dict de buffers datas['both_beta']
                if "count" not in datas["both_beta"]:
                    for (
                        key
                    ) in (
                        values_both_beta
                    ):  # les clés de datas['both_beta'] sont les mêmes que celles de values_both_beta
                        datas["both_beta"][key] = []
                    datas["both_beta"]["v_pression"] = []
                for (
                    key
                ) in values_both_beta:  # on ajoute les valeurs reçues dans les buffers
                    datas["both_beta"][key].append(values_both_beta[key])
                    # if len(datas['both_beta'][key]) > N_data: # on garde que les N_data dernières valeurs
                    #     datas['both_beta'][key].pop(0)

                if len(datas["both_beta"]["altitude"]) > 1:
                    datas["both_beta"]["v_pression"].append(
                        (
                            datas["both_beta"]["altitude"][-1]
                            - datas["both_beta"]["altitude"][-2]
                        )
                        / (
                            datas["both_beta"]["time"][-1]
                            - datas["both_beta"]["time"][-2]
                        )
                    )
                else:
                    datas["both_beta"]["v_pression"].append(0)
                # if len(datas['both_beta']["v_pression"]) > N_data:
                #     datas['both_beta']["v_pression"].pop(0)
                if (
                    datas["both_beta"]["time"][-1] - datas["both_beta"]["time"][0]
                    >= buffer_time
                ):
                    for key in datas["both_beta"]:
                        datas["both_beta"][key].pop(0)
                new_data_both_beta = True

            # on check si c'est un message de données unique à beta de type "+TEST: RX \"F49C4B90A8BA735B34F271DB363C9AA03C2FE843191990\""
            if not queues["beta"].empty():
                values_beta = queues["beta"].get()
                # check si c'est la première fois qu'on reçoit un message de ce type, auqeul cas on initialize le dict de buffers datas['beta']
                if "alpha_or_beta" not in datas["beta"]:
                    for key in values_beta:
                        datas["beta"][key] = []
                for key in values_beta:
                    datas["beta"][key].append(values_beta[key])
                    # if len(datas['beta'][key]) > N_data:
                    #     datas['beta'][key].pop(0)
                if datas["beta"]["time"][-1] - datas["beta"]["time"][0] >= buffer_time:
                    for key in datas["beta"]:
                        datas["beta"][key].pop(0)
                new_data_beta = True
        except ValueError:
            print("Error reading data")
            pass  # Handle the exception if the line is not as expected

    # if not (new_data_snr_both_alpha or new_data_snr_beta or new_data_snr_both_beta or new_data_alpha or new_data_both_beta or new_data_beta):
    #     return []  # No new data, do not update the figure

    artists = []

    if new_data_beta:
        quats = np.array(
            [
                datas["beta"]["q0"][-1],
                datas["beta"]["q1"][-1],
                datas["beta"]["q2"][-1],
                datas["beta"]["q3"][-1],
            ]
        )
        mesh_rotated = rotate_mesh(mesh.points, quats)

        for i, cell in enumerate(mesh.cells):
            if cell.type == "hexahedron":
                hex_vertices = mesh_rotated[cell.data]
                face_collection[i].set_verts(hex_vertices)

        # Update axis limits dynamically
        # x_min, x_max = mesh_rotated[:, 0].min(), mesh_rotated[:, 0].max()
        # y_min, y_max = mesh_rotated[:, 1].min(), mesh_rotated[:, 1].max()
        # z_min, z_max = mesh_rotated[:, 2].min(), mesh_rotated[:, 2].max()

        artists.extend(face_collection)

        # Update the 3D projection
        # projection_collection.append(ax_3d.plot(mesh_points[:, 0], mesh_points[:, 1], zs=-100, zdir='z', color='r', alpha=0.5))  # xy plane
        # projection_collection.append(ax_3d.plot(mesh_points[:, 0], zs=-100, ys=mesh_points[:, 2], zdir='y', color='g', alpha=0.5))  # xz plane
        # projection_collection.append(ax_3d.plot(zs=-100, xs=mesh_points[:, 1], ys=mesh_points[:, 2], zdir='x', color='b', alpha=0.5))  # yz plane

        projection_collection[0][0].set_data(mesh_rotated[:, 0], mesh_rotated[:, 1])
        projection_collection[1][0].set_data(mesh_rotated[:, 0], mesh_rotated[:, 2])
        projection_collection[2][0].set_data(mesh_rotated[:, 1], mesh_rotated[:, 2])

        projection_collection[0][0].set_3d_properties(-100, zdir="z")
        projection_collection[1][0].set_3d_properties(+100, zdir="y")
        projection_collection[2][0].set_3d_properties(-100, zdir="x")

        artists.extend(projection_collection)

        # Update 2d plots
        # for ax_id in lines_axs:
        #     lines = lines_axs[ax_id]
        #     for i, line in enumerate(lines):
        #         line.set_data(range(len(data)), np.array(data)[:, i])
        #         artists.append(line)

        lines = lines_axs["acc_v"]
        line = lines[0]
        line.set_data(datas["beta"]["time"], datas["beta"]["normAcc"])
        artists.append(line)

        lines = lines_axs["pos"]
        line = lines[0]
        line.set_data(datas["beta"]["time"], datas["beta"]["px"])
        line = lines[1]
        line.set_data(datas["beta"]["time"], datas["beta"]["py"])
        line = lines[2]
        line.set_data(datas["beta"]["time"], datas["beta"]["pz"])
        artists.extend(lines)

        lines = lines_axs["assiette_azimuth"]
        line = lines[0]
        line.set_data(datas["beta"]["time"], datas["beta"]["assiette"])
        line = lines[1]
        line.set_data(datas["beta"]["time"], datas["beta"]["azimuth"])
        artists.extend(lines)

        # lines = lines_axs["v_pressure"]
        # line = lines[0]
        # line.set_data(datas['beta']["time"], datas['beta']["normV"])
        # line = lines[1]
        # line.set_data(datas['beta']["time"], datas['beta']["v_pression"])
        # artists.extend(lines)

        # lines = lines_axs["altitude"]
        # line = lines[0]
        # line.set_data(datas['beta']["time"], datas['beta']["altitude"])
        # artists.append(line)

        # Update axis scales
        axs["acc_v"].relim()
        axs["acc_v"].autoscale_view()
        reajust_xlimits(axs["acc_v"], fixed_xlims["acc_v"])

        axs["pos"].relim()
        axs["pos"].autoscale_view()
        reajust_xlimits(axs["pos"], fixed_xlims["pos"])

        axs["assiette_azimuth"].relim()
        axs["assiette_azimuth"].autoscale_view()

        # axs["v_pressure"].relim()
        # axs["v_pressure"].autoscale_view()

        # axs["altitude"].relim()
        # axs["altitude"].autoscale_view()

    lines = lines_axs["v_pressure"]
    if new_data_beta:
        line = lines[0]
        line.set_data(datas["beta"]["time"], datas["beta"]["normV"])
    if new_data_alpha:
        line = lines[1]
        line.set_data(datas["alpha"]["time"], datas["alpha"]["v_pression"])
    if new_data_both_beta:
        line = lines[2]
        line.set_data(datas["both_beta"]["time"], datas["both_beta"]["v_pression"])
    artists.extend(lines)

    lines = lines_axs["altitude"]
    if new_data_alpha:
        line = lines[0]
        line.set_data(datas["alpha"]["time"], datas["alpha"]["altitude"])
    if new_data_both_beta:
        line = lines[1]
        line.set_data(datas["both_beta"]["time"], datas["both_beta"]["altitude"])
    artists.extend(lines)

    axs["v_pressure"].relim()
    axs["v_pressure"].autoscale_view()
    reajust_xlimits(axs["v_pressure"], fixed_xlims["v_pressure"])

    axs["altitude"].relim()
    axs["altitude"].autoscale_view()
    reajust_xlimits(axs["altitude"], fixed_xlims["altitude"])

    # fixed_xlims["acc_v"] = [-25, 25]
    # fixed_xlims["pos"] = [-100, 3000]
    # fixed_xlims["v_pressure"] = [-20, 500]
    # fixed_xlims["altitude"] = [0, 3500]

    # texts

    # get last snr value for alpha
    if "alpha_or_beta" not in datas["snr"]:
        last_snr_alpha = None
    else:
        for i in range(len(datas["snr"]["alpha_or_beta"])):
            if datas["snr"]["alpha_or_beta"][-1 - i] == "alpha":
                last_snr_alpha = -1 - i
                break
        else:
            last_snr_alpha = None

    # get last snr value for beta
    if "alpha_or_beta" not in datas["snr"]:
        last_snr_beta = None
    else:
        for i in range(len(datas["snr"]["alpha_or_beta"])):
            if datas["snr"]["alpha_or_beta"][-1 - i] in ["beta", "both_beta"]:
                last_snr_beta = -1 - i
                break
        else:
            last_snr_beta = None

    if last_snr_alpha is not None and "time" in datas["alpha"]:
        # dernitexts_collectionère valeur
        texts_collection[21].set_text(
            f"{time.time()-datas['alpha']['time'][-1]- init_time:.1f} s"
        )

        # depuis décollage
        texts_collection[23].set_text(f"{datas['alpha']['time_since_launch'][-1]} s")
        # snr
        texts_collection[25].set_text(f"{datas['snr']['snr'][last_snr_alpha]}")
        # rssi
        texts_collection[27].set_text(f"{datas['snr']['rssi'][last_snr_alpha]}")
        # sequenceur
        if datas["alpha"]["seq_state"][-1]:
            texts_collection[29].set_text(f"Y")
            texts_collection[29].set_color("green")
        else:
            texts_collection[29].set_text(f"N")
            texts_collection[29].set_color("red")
        # info
        if datas["alpha"]["info_state"][-1]:
            texts_collection[31].set_text(f"Y")
            texts_collection[31].set_color("green")
        else:
            texts_collection[32].set_text(f"N")
            texts_collection[32].set_color("red")
        # exp

        # parachute
        if datas["alpha"]["parachute_state"][-1]:
            texts_collection[34].set_text(f"Y")
            texts_collection[34].set_color("green")
        else:
            texts_collection[34].set_text(f"N")
            texts_collection[34].set_color("red")
        # latitude, longitude, nombre de satellites
        texts_collection[41].set_text(f"{datas['alpha']['latitude'][-1]:.7f}")
        texts_collection[42].set_text(f"{datas['alpha']['longitude'][-1]:.7f}")
        texts_collection[43].set_text(f"{datas['alpha']['number_satellites'][-1]}")
        # texts_collection[44].set_text(f"{datas['alpha']['gpstime_hour'][-1]}h {datas['alpha']['gpstime_min'][-1]}m {datas['alpha']['gpstime_sec'][-1]}s")

        # température
        texts_collection[39].set_text(f"{datas['alpha']['temperature'][-1]} °C")

    if last_snr_beta is not None and new_data_both_beta:
        # dernière valeur
        texts_collection[22].set_text(
            f"{time.time()-datas['both_beta']['time'][-1]- init_time:.1f} s"
        )
        # depuis décollage
        texts_collection[24].set_text(
            f"{datas['both_beta']['time_since_launch'][-1]} s"
        )
        # snr
        texts_collection[26].set_text(f"{datas['snr']['snr'][last_snr_beta]}")
        # rssi
        texts_collection[28].set_text(f"{datas['snr']['rssi'][last_snr_beta]}")
        # sequenceur
        if datas["both_beta"]["seq_state"][-1]:
            texts_collection[30].set_text(f"Y")
            texts_collection[30].set_color("green")
        else:
            texts_collection[30].set_text(f"N")
            texts_collection[30].set_color("red")
        # info
        if datas["both_beta"]["info_state"][-1]:
            texts_collection[32].set_text(f"Y")
            texts_collection[32].set_color("green")
        else:
            texts_collection[32].set_text(f"N")
            texts_collection[32].set_color("red")
        # parachute
        if datas["both_beta"]["parachute_state"][-1]:
            texts_collection[35].set_text(f"Y")
            texts_collection[35].set_color("green")
        else:
            texts_collection[35].set_text(f"N")
            texts_collection[35].set_color("red")
        # latitude, longitude, nombre de satellites
        texts_collection[45].set_text(f"{datas['both_beta']['latitude'][-1]:.7f}")
        texts_collection[46].set_text(f"{datas['both_beta']['longitude'][-1]:.7f}")
        texts_collection[47].set_text(f"{datas['both_beta']['number_satellites'][-1]}")
        # texts_collection[48].set_text(f"{datas['both_beta']['gpstime'][-1]}h {datas['both_beta']['gpstime_min'][-1]}m {datas['both_beta']['gpstime_sec'][-1]}s")

        # temperature
        texts_collection[40].set_text(f"{datas['both_beta']['temperature'][-1]} °C")

    if new_data_beta:
        # exp
        if datas["beta"]["exp_state"][-1]:
            texts_collection[33].set_text(f"Y")
            texts_collection[33].set_color("green")
        else:
            texts_collection[33].set_text(f"N")
            texts_collection[33].set_color("red")
        # separation state
        if datas["beta"]["separation_state"][-1]:
            texts_collection[36].set_text(f"Y")
            texts_collection[36].set_color("green")
        else:
            texts_collection[36].set_text(f"N")
            texts_collection[36].set_color("red")
        # moteur
        if datas["beta"]["allumage_moteur"][-1]:
            texts_collection[37].set_text(f"Y")
            texts_collection[37].set_color("green")
        else:
            texts_collection[37].set_text(f"N")
            texts_collection[37].set_color("red")

        # reservoir pressure
        texts_collection[38].set_text(
            f"{datas['beta']['pressure_reservoir'][-1]/10.0:.2f} bar"
        )

    return artists


# Function to initialize the plot
def init_plot(mesh):
    fig = plt.figure(figsize=(14, 8))
    fig.canvas.manager.set_window_title("SIRIUS - Station Sol")

    axs = {}

    # 3D subplot
    ax_3d = plt.subplot2grid((3, 4), (0, 0), rowspan=2, colspan=2, projection="3d")
    face_collection = []
    for cell in mesh.cells:
        if cell.type == "hexahedron":
            hex_vertices = mesh.points[cell.data]
            collection = Poly3DCollection(hex_vertices, alpha=0.7)
            face_collection.append(collection)
            ax_3d.add_collection3d(collection)

    ax_3d.set_xlabel("X")
    ax_3d.set_ylabel("Y")
    ax_3d.set_zlabel("Z")
    # ax_3d.view_init(elev=0, azim=-90)

    x_min, x_max = -100, 100
    y_min, y_max = -100, 100
    z_min, z_max = -100, 100

    ax_3d.set_xlim3d(x_min, x_max)
    ax_3d.set_ylim3d(y_min, y_max)
    ax_3d.set_zlim3d(z_min, z_max)
    ax_3d.set_box_aspect([x_max - x_min, y_max - y_min, z_max - z_min])

    projection_collection = []
    # Plot projections on the xy, xz, and yz planes
    mesh_points = mesh.points
    projection_collection.append(
        ax_3d.plot(
            mesh_points[:, 0],
            mesh_points[:, 1],
            zs=-100,
            zdir="z",
            color="r",
            alpha=0.5,
        )
    )  # xy plane
    projection_collection.append(
        ax_3d.plot(
            mesh_points[:, 0],
            zs=+100,
            ys=mesh_points[:, 2],
            zdir="y",
            color="g",
            alpha=0.5,
        )
    )  # xz plane
    projection_collection.append(
        ax_3d.plot(
            zs=-100,
            xs=mesh_points[:, 1],
            ys=mesh_points[:, 2],
            zdir="x",
            color="b",
            alpha=0.5,
        )
    )  # yz plane

    axs["3d"] = ax_3d

    texts_collection = []
    # spans on 4 rows in the third and fourth column
    ax_text = plt.subplot2grid((3, 4), (0, 2), rowspan=3, colspan=2)
    ax_text.axis("off")

    # temps derniere valeur (alpha et beta)
    # temps depuis décollage
    # snr, rssi (alpha et beta)
    # etat sequenceur (alpha et beta)
    # etat info (alpha et beta)
    # etat exp (alpha et beta)
    # parachute (alpha et beta)
    # latitude, longitude, nombre de satellites (beta)

    # separation state
    # reservoir pressure
    # temperature

    # constant texts
    texts_collection.append(
        ax_text.text(0.8, 0.95, "α", fontsize=25, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.95, "β", fontsize=25, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(
            0.5, 0.9, f"dernière valeur: ", fontsize=13, ha="left", va="center"
        )
    )
    texts_collection.append(
        ax_text.text(
            0.5, 0.9, f"dernière valeur: ", fontsize=13, ha="left", va="center"
        )
    )
    texts_collection.append(
        ax_text.text(
            0.5, 0.85, f"depuis décollage: ", fontsize=13, ha="left", va="center"
        )
    )
    texts_collection.append(
        ax_text.text(0.5, 0.8, f"SNR: ", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.75, f"RSSI: ", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.7, f"Séquenceur: ", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.65, f"Info: ", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.6, f"Exp: ", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.55, f"Parachute: ", fontsize=13, ha="left", va="center")
    )

    texts_collection.append(
        ax_text.text(0.5, 0.45, f"Séparation: ", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.4, f"Moteur: ", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.35, f"Réservoirs: ", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.3, f"Température: ", fontsize=13, ha="left", va="center")
    )

    texts_collection.append(
        ax_text.text(0.5, 0.2, f"Lat α", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.17, f"Lon α", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.14, f"Sat α", fontsize=13, ha="left", va="center")
    )

    texts_collection.append(
        ax_text.text(0.5, 0.05, f"Lat β", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, 0.02, f"Lon β", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, -0.01, f"Sat β", fontsize=13, ha="left", va="center")
    )

    # dynamic texts
    # dernière valeur
    texts_collection.append(
        ax_text.text(0.8, 0.9, f"0 s", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.9, f"0 s", fontsize=13, ha="left", va="center")
    )
    # depuis décollage
    texts_collection.append(
        ax_text.text(0.8, 0.85, f"0 s", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.85, f"0 s", fontsize=13, ha="left", va="center")
    )
    # snr
    texts_collection.append(
        ax_text.text(0.8, 0.8, f"0", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.8, f"0", fontsize=13, ha="left", va="center")
    )
    # rssi
    texts_collection.append(
        ax_text.text(0.8, 0.75, f"0", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.75, f"0", fontsize=13, ha="left", va="center")
    )
    # sequenceur
    texts_collection.append(
        ax_text.text(0.8, 0.7, f"N", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.7, f"N", fontsize=13, ha="left", va="center")
    )
    # info
    texts_collection.append(
        ax_text.text(0.8, 0.65, f"N", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.65, f"N", fontsize=13, ha="left", va="center")
    )
    # exp
    texts_collection.append(
        ax_text.text(0.92, 0.6, f"N", fontsize=13, ha="left", va="center")
    )
    # parachute
    texts_collection.append(
        ax_text.text(0.8, 0.55, f"N", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.55, f"N", fontsize=13, ha="left", va="center")
    )

    # separation state
    texts_collection.append(
        ax_text.text(0.8, 0.45, f"N", fontsize=13, ha="left", va="center")
    )
    # moteur
    texts_collection.append(
        ax_text.text(0.8, 0.4, f"N", fontsize=13, ha="left", va="center")
    )
    # reservoir pressure
    texts_collection.append(
        ax_text.text(0.8, 0.35, f"0 bar", fontsize=13, ha="left", va="center")
    )
    # temperature
    texts_collection.append(
        ax_text.text(0.8, 0.3, f"0 °C", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.92, 0.3, f"0 °C", fontsize=13, ha="left", va="center")
    )

    # latitude, longitude, nombre de satellites (alpha)
    texts_collection.append(
        ax_text.text(0.8, 0.2, f"0", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.8, 0.17, f"0", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.8, 0.14, f"0", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.8, 0.11, f"0h 0m 0s", fontsize=13, ha="left", va="center")
    )

    # latitude, longitude, nombre de satellites (beta)
    texts_collection.append(
        ax_text.text(0.8, 0.05, f"0", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.8, 0.02, f"0", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.8, -0.01, f"0", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.8, -0.04, f"0h 0m 0s", fontsize=13, ha="left", va="center")
    )

    texts_collection.append(
        ax_text.text(0.5, 0.11, f"Tps α", fontsize=13, ha="left", va="center")
    )
    texts_collection.append(
        ax_text.text(0.5, -0.04, f"Tps β", fontsize=13, ha="left", va="center")
    )

    gs = gridspec.GridSpec(1, 2, width_ratios=[5, 5], wspace=0.3)

    # Raw data subplots
    lines_axs = {}

    # acceleration and velocity
    lines_axs["acc_v"] = []
    ax_v_acc = fig.add_subplot(3, 4, 3)
    axs["acc_v"] = ax_v_acc
    (line,) = ax_v_acc.plot([], [], label="Acc")
    lines_axs["acc_v"].append(line)
    ax_v_acc.set_xlabel("Temps (s)")
    ax_v_acc.set_ylabel("Accéleration (m/s²)")
    ax_v_acc.legend(bbox_to_anchor=(1.05, 1), loc="upper left", borderaxespad=0.0)

    # position
    lines_axs["pos"] = []
    ax_pos = fig.add_subplot(3, 4, 7)
    axs["pos"] = ax_pos
    (line,) = ax_pos.plot([], [], label="Px")
    lines_axs["pos"].append(line)
    (line,) = ax_pos.plot([], [], label="Py")
    lines_axs["pos"].append(line)
    (line,) = ax_pos.plot([], [], label="Pz")
    lines_axs["pos"].append(line)
    ax_pos.set_xlabel("Temps (s)")
    ax_pos.set_ylabel("Position (m)")
    ax_pos.legend(bbox_to_anchor=(1.05, 1), loc="upper left", borderaxespad=0.0)

    # assiette and azimuth
    lines_axs["assiette_azimuth"] = []
    ax_as_az = fig.add_subplot(3, 4, 9)
    axs["assiette_azimuth"] = ax_as_az
    (line,) = ax_as_az.plot([], [], label="Assiette")
    lines_axs["assiette_azimuth"].append(line)
    (line,) = ax_as_az.plot([], [], label="Azimuth")
    lines_axs["assiette_azimuth"].append(line)
    ax_as_az.set_xlabel("Temps (s)")
    ax_as_az.set_ylabel("Angle (°)")
    ax_as_az.legend(bbox_to_anchor=(1.05, 1), loc="upper left", borderaxespad=0.0)

    # Velocity and pressure velocity
    lines_axs["v_pressure"] = []
    ax_v_p = fig.add_subplot(3, 4, 10)
    axs["v_pressure"] = ax_v_p
    (line,) = ax_v_p.plot([], [], label="V_IMU")
    lines_axs["v_pressure"].append(line)
    (line,) = ax_v_p.plot([], [], label="V_P α")
    lines_axs["v_pressure"].append(line)
    (line,) = ax_v_p.plot([], [], label="V_P β")
    lines_axs["v_pressure"].append(line)
    ax_v_p.set_xlabel("Temps (s)")
    ax_v_p.set_ylabel("Vitesse (m/s)")
    ax_v_p.legend(bbox_to_anchor=(1.05, 1), loc="upper left", borderaxespad=0.0)

    # Altitude
    lines_axs["altitude"] = []
    ax_alt = fig.add_subplot(3, 4, 11)
    axs["altitude"] = ax_alt
    (line,) = ax_alt.plot([], [], label="Altitude α")
    lines_axs["altitude"].append(line)
    (line,) = ax_alt.plot([], [], label="Altitude β")
    lines_axs["altitude"].append(line)
    ax_alt.set_xlabel("Temps (s)")
    ax_alt.set_ylabel("Altitude (m)")
    ax_alt.legend(bbox_to_anchor=(1.05, 1), loc="upper left", borderaxespad=0.0)

    return fig, axs, face_collection, lines_axs, projection_collection, texts_collection


if __name__ == "__main__":
    global init_time
    init_time = time.time()

    if WITH_SERIAL:
        ports = list(serial.tools.list_ports.comports())
        # Ouverture du port série
        print("Liste des ports disponibles :")
        for p in ports:
            print(p)
        # serial_port = ports[0][0]
        serial_ports = [serial_port1, serial_port2]

    queue_snr = multiprocessing.Queue()
    queue_alpha = multiprocessing.Queue()
    queue_both_beta = multiprocessing.Queue()
    queue_beta = multiprocessing.Queue()
    queues = {
        "snr": queue_snr,
        "alpha": queue_alpha,
        "both_beta": queue_both_beta,
        "beta": queue_beta,
    }

    fixed_xlims = {}
    fixed_xlims["acc_v"] = [-25, 25]
    fixed_xlims["pos"] = [-100, 3000]
    fixed_xlims["v_pressure"] = [-20, 500]
    fixed_xlims["altitude"] = [0, 3500]

    # Read and normalize the mesh
    mesh = meshio.read("lowpolyrocket.e")
    # mesh.points[:, :] /= 3100/200
    mesh.points[:, :] /= 200 / 200

    sizex = mesh.points[:, 0].max() - mesh.points[:, 0].min()
    sizey = mesh.points[:, 1].max() - mesh.points[:, 1].min()
    sizez = mesh.points[:, 2].max() - mesh.points[:, 2].min()

    mesh.points[:, 0] += -mesh.points[:, 0].min() - sizex / 2
    mesh.points[:, 1] += -mesh.points[:, 1].min() - sizey / 2
    mesh.points[:, 2] += -mesh.points[:, 2].min() - sizez / 2

    # Create the process for reading the serial port
    if WITH_SERIAL:
        serial_process = multiprocessing.Process(
            target=read_from_lora, args=(serial_ports, queues)
        )
    else:
        serial_process = multiprocessing.Process(
            target=read_from_file, args=(filename, queues)
        )
    # else:
    # serial_process = multiprocessing.Process(target=read_random, args=(queues,))

    serial_process.start()

    # Initialize the plot
    fig, axs, face_collection, lines_axs, projection_collection, texts_collection = (
        init_plot(mesh)
    )

    # Data storage
    data_snr = {}
    data_alpha = {}
    data_both_beta = {}
    data_beta = {}
    datas = {
        "snr": data_snr,
        "alpha": data_alpha,
        "both_beta": data_both_beta,
        "beta": data_beta,
    }

    # Create the animation
    ani = FuncAnimation(
        fig,
        update,
        fargs=(
            axs,
            mesh,
            queues,
            datas,
            face_collection,
            lines_axs,
            projection_collection,
            texts_collection,
        ),
        interval=0,
        blit=False,
        cache_frame_data=False,  # Ajoutez cette ligne pour désactiver le caching des données de frame
    )

    # Show the plot
    plt.tight_layout()
    plt.show()

    # Terminate the serial process when done
    serial_process.terminate()
    serial_process.join()
