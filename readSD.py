import struct

# Chemin vers le fichier .bin
file_path = "theta.bin"

# Ouvrir le fichier en mode binaire pour la lecture
with open(file_path, "rb") as file:
    # Lire le contenu du fichier
    data = file.read()

# Afficher les données lues
# print("Données brutes :", data)


########### Entiers ###########
# # Convertir les données en une liste d'octets
# byte_list = list(data)

# # Afficher les octets
# print("Liste des octets :", byte_list)


########### Flottants ###########
# Nombre de flottants dans le fichier
num_floats = len(data) // 4  # Chaque flottant est représenté par 4 octets

# Convertir les données en une liste de flottants
floats = struct.unpack(
    "<" + "f" * num_floats, data
)  # '<' pour little-endian et 'f' pour flottant

# Afficher les flottants
# print("Liste des flottants :", floats)

import matplotlib.pyplot as plt

plt.plot(floats)
plt.show()
