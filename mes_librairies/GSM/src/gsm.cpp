#include "gsm.h"

GSM::GSM() {}

// Obtenir la réponse du module GSM
void GSM::GetRep()
{
#ifdef SERIAL_ON
    delay(100);                   // On laisse le temps au module GSM de lire la commannde qu'on lui a envoyé et d'écrire sa réponse
    while (gsmSerial.available()) // On lit ce que le module GSM envoie à la Teensy via gsmSerial (si il envoie qch)
    {
        Serial.write(gsmSerial.read());
    }
#endif
}

bool GSM::Init()
{
    gsmSerial.begin(115200);
    delay(100);
    gsmSerial.println("AT");
    delay(200);
    GetRep();
    gsmSerial.println("AT+CPIN?");
    delay(200);
    GetRep();
    gsmSerial.println("AT+CPIN=1234");
    delay(200);
    GetRep();
    gsmSerial.println("AT+CPIN?");
    delay(200);
    GetRep();
    gsmSerial.println("AT+CSQ"); // GSM signal strength. 0-31, 31 is the best
    delay(200);
    GetRep();
    gsmSerial.println("AT+CMGF=1");
    delay(200);
    GetRep();
    gsmSerial.println("AT+CMGS=\"+33787200814\""); // Mettre le numéro de téléphone auquel on veut envoyer un message
    delay(200);
    bool available = gsmSerial.available();
    GetRep();

    return available;
}

void GSM::SendSMS(String message)
{
    gsmSerial.println("AT+CMGF=1");
    GetRep();
    gsmSerial.println("AT+CMGS=\"+33787200814\""); // Mettre le numéro de téléphone auquel on veut envoyer un message
    GetRep();
    gsmSerial.print(message);
    gsmSerial.print(char(26));
    delay(100);
    gsmSerial.println();
#ifdef SERIAL_ON
    Serial.println("text sent");
#endif
}

void GSM::PrintSignalStrength()
{
    gsmSerial.println("AT+CSQ"); // GSM signal strength. 0-31, 31 is the best
    GetRep();
}