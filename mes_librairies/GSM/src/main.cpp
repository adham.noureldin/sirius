// #include "gsm.h"

// // On communique avec le module GSM via le port Serial2, en lui envoyant des commandes AT. Voir sur internet pour les commandes AT

// GSM gsm;

// void setup()
// {
//   gsm.Init();
//   gsm.SendSMS("Hello, Regards from SIM800");
// }

// void loop()
// {
//   gsm.PrintSignalStrength();
// }

////////////////////////////////
////////////////////////////////
////////////////////////////////

#include <Arduino.h>

#define gsmSerial Serial2 // Le nom du port serial correspondant au module GSM

void UpdateSerial()
{
  delay(100);                   // On laisse le temps au module GSM de lire la commannde qu'on lui a envoyé et d'écrire sa réponse
  while (gsmSerial.available()) // On lit ce que le module GSM envoie à la Teensy via gsmSerial (si il envoie qch)
  {
    Serial.write(gsmSerial.read());
  }
  while (Serial.available()) // On lit ce que la Teensy envoie au module GSM via Serial (si elle envoie qch)
  {
    gsmSerial.write(Serial.read());
  }
  delay(100);
}

int t = 0;

void setup()
{
  Serial.begin(115000);
  while (!Serial)
    ;
  gsmSerial.begin(115200);
  while (!gsmSerial)
    ;
  Serial.println("Serial OK");
  gsmSerial.println("AT");
  UpdateSerial();
  gsmSerial.println("AT+CPIN?");
  UpdateSerial();
  gsmSerial.println("AT+CSQ"); // GSM signal strength. 0-31, 31 is the best
  UpdateSerial();
  gsmSerial.println("AT+CMGF=1");
  UpdateSerial();
  gsmSerial.println("AT+CMGS=\"+33787200814\""); // Mettre le numéro de téléphone auquel on veut envoyer un message
  UpdateSerial();
  gsmSerial.print("Hello, Regards from SIM800");
  gsmSerial.print(char(26));
  delay(100);
  gsmSerial.println();
  Serial.println("text sent");
#ifdef GETLOCATION
  gsmSerial.println("AT+SAPBR=3,1,\"Contype\",\"GPRS\"");
  UpdateSerial();
  gsmSerial.println("AT+SAPBR=3,1,\"APN\",\"free\"");
  UpdateSerial();
  gsmSerial.println("AT+SAPBR=1,1");
  UpdateSerial();
  gsmSerial.println("AT+SAPBR=2,1");
  UpdateSerial();
  gsmSerial.println("AT+CIPGSMLOC=2,1");
  UpdateSerial();
#endif
}

void loop()
{
#ifdef GETLOCATION
  gsmSerial.println("AT+CIPGSMLOC=2,1");
  UpdateSerial();
#endif
  gsmSerial.println("AT+CSQ"); // GSM signal strength. 0-31, 31 is the best
  UpdateSerial();
  delay(1000);
}
