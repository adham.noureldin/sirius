#include <Arduino.h>

#define SERIAL_ON

#define gsmSerial Serial2 // Le nom du port serial correspondant au module GSM

class GSM
{
public:
    GSM();
    bool Init();
    void GetRep();
    void SendSMS(String message);
    void PrintSignalStrength();
};
