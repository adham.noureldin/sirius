#include "barometer.h"

Barometer::Barometer()
{
    for (unsigned int i = 0; i < pressuresSize; i++)
    {
        pressures[i] = 0;
    }
}

bool Barometer::Init()
{
    Serial.println("Starting");

    Wire1.begin();

    // Initialize pressure sensor
    // Returns true if initialization was successful

    int i = 0;
    while (!sensor.init(Wire1) && i < 2)
    {
        Serial.println("Init failed!");
        Serial.println("Are SDA/SCL connected correctly?");
        Serial.println("Blue Robotics Bar30: White=SDA, Green=SCL");
        Serial.println("\n\n\n");
        delay(5000);
        i++;
    }

    // .init sets the sensor model for us but we can override it if required.
    // Uncomment the next line to force the sensor model to the MS5837_30BA.
    sensor.setModel(MS5837::MS5837_02BA);

    sensor.setFluidDensity(1.2); // kg/m^3 (freshwater, 1029 for seawater)

    return sensor.init(Wire1);
}

void Barometer::Update()
{
    // Update pressure and temperature readings
    sensor.read();
    float currentPressure = sensor.pressure();
    temperature = sensor.temperature();
    altitude = sensor.altitude();

    // Store pressure readings in an array
    pressures[nextPosition] = currentPressure;
    nextPosition = (nextPosition + 1) % pressuresSize;

    // Serial.print("Pressure: ");
    // Serial.print(currentPressure, 15);
    // Serial.print(",");
    // delay(10);
    // Serial.println(" mbar");

    /*Serial.print("Temperature: ");
    Serial.print(currentTemperature);
    Serial.println(" deg C");*/

    /*
    Serial.print("Depth: ");
    Serial.print(sensor.depth());
    Serial.println(" m");

    Serial.print("Altitude: ");
    Serial.print(sensor.altitude());
    Serial.println(" m above mean sea level");
    */
    // delay(2000);
}

bool Barometer::PressureHasReachedMinimum() const
{
    float pressureAvgFirstHalf = 0;
    float pressureAvgSecondHalf = 0;
    unsigned int startPos = nextPosition;
    unsigned int endPos = (nextPosition + pressuresSize / 2) % pressuresSize;

    for (unsigned int i = startPos; i != endPos; i = (i + 1) % pressuresSize)
    {
        pressureAvgFirstHalf += pressures[i];
    }
    pressureAvgFirstHalf /= pressuresSize / 2;

    startPos = endPos;
    endPos = nextPosition;

    for (unsigned int i = startPos; i != endPos; i = (i + 1) % pressuresSize)
    {
        pressureAvgSecondHalf += pressures[i];
    }
    pressureAvgSecondHalf /= pressuresSize / 2;

    // bilan Fabian: besoin d'avoir accès aux pressions dans l'intervalle de temps [t-0.5s, t], appliquer
    // cet algorithme et alors on détecte l'apogée avec un retard de 0.25s
    // L'année dernière on pouvait détecter l'apogée avec 0.25s de retard au minimum mais
    // comme cette année le parachute ne s'ouvrira pas avant on peut être encore plus précis... si besoin

    return pressureAvgSecondHalf > pressureAvgFirstHalf;
}

float *Barometer::GetPressures()
{
    return pressures;
}

float Barometer::GetPressure(){
    return pressures[(nextPosition-1)%pressuresSize];
}

unsigned int Barometer::GetPressuresSize()
{
    return pressuresSize;
}

float Barometer::GetTemperature()
{
    return temperature;
}

float Barometer::GetAltitude()
{
    return altitude;
}

void Barometer::shutdown(){
    Wire1.end();
}