#include <Wire.h>
#include "MS5837.h"

#define PRESSURE_BUFFER_SIZE 76

class Barometer
{

    MS5837 sensor;
    float pressures[PRESSURE_BUFFER_SIZE];
    float temperature;
    float altitude;
    const unsigned int pressuresSize = PRESSURE_BUFFER_SIZE;
    unsigned int nextPosition = 0;

public:
    Barometer();
    bool Init();
    void Update();
    bool PressureHasReachedMinimum() const;
    float *GetPressures();
    float GetPressure();
    unsigned int GetPressuresSize();
    float GetTemperature();
    float GetAltitude();
    void shutdown();
};