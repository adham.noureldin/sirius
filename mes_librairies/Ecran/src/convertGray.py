from PIL import Image

# Open the image file
image_path = 'src/chaton2.png'  # Replace with your image path
image = Image.open(image_path)

# Resize it to fit your display
image = image.resize((320, 240))

# Convert the image to 8-bit grayscale
image = image.convert('L')

# Convert the image to a 8-bit array
gray_array = list(image.getdata())

# Convert the list to a C-style array
c_array_str = '{' + ', '.join('0x{:02X}'.format(pixel) for pixel in gray_array) + '}'
print(c_array_str)
