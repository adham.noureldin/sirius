from PIL import Image
import numpy as np

# Load the image
img = Image.open('src/Logo_CACS.jpeg')

# Convert image to RGB
img_rgb = img.convert('RGB')

# Extract RGB values
rgb_array = np.array(img_rgb)
flattened_rgb_array = rgb_array.flatten()

# Print the width and height of the image
width, height = img.size

print(list(flattened_rgb_array), width, height)
