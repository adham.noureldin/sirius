#include "screen.h"
#include <SPI.h>

#define TFT_MISO 12
#define TFT_LED 30
#define TFT_SCK 13
#define TFT_MOSI 11
#define TFT_DC 26
#define TFT_RESET 27
#define TFT_CS 10

Screen::Screen() : tft(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCK, TFT_RESET, TFT_MISO) {}

bool Screen::Init()
{
    Serial.begin(9600);

    pinMode(TFT_LED, OUTPUT);
    TurnOn();

    tft.begin();        // Quelques initialisations utiles
    tft.setRotation(1); // Choisir l'orientation de l'écran. 0: portrait, 1:paysage, 2:portrait inversé ou 3:paysage inversé

    tft.fillScreen(tft.color565(255, 255, 255)); // colorie tous les pixels de l'écran de la couleur spécifiée

    /*for (int i = 0; i < (int)sizeof(chaton); i++) {
        chaton[i] = 255;
    }*/
    // tft.fillScreen(tft.color565(255, 255, 255));

    int arrWidth1 = 199; // NE PAS TOUCHER
    int arrHeight1 = 81; // NE PAS TOUCHER
    float aspectRatio1 = (float)arrWidth1 / (float)arrHeight1;
    int realWidth1 = 150; // Toucher pour changer la taille de l'image
    int realHeight1 = (int)(realWidth1 / aspectRatio1);

    int arrWidth2 = 200;  // NE PAS TOUCHER
    int arrHeight2 = 200; // NE PAS TOUCHER
    float aspectRatio2 = (float)arrWidth2 / (float)arrHeight2;
    int realWidth2 = 120; // Toucher pour changer la taille de l'image
    int realHeight2 = (int)(realWidth2 / aspectRatio2);

    for (int realX = 0; realX < realWidth1; realX++)
    {
        for (int realY = 0; realY < realHeight1; realY++)
        {
            int x = round(realX / (float)realWidth1 * (float)arrWidth1);
            int y = round(realY / (float)realHeight1 * (float)arrHeight1);
            int i = round(1 * (x + y * arrWidth1));
            /*Serial.print(tft.color565(
                Logo_CACS_199x81_565[i],
                Logo_CACS_199x81_565[i + 1],
                Logo_CACS_199x81_565[i + 2]));
            Serial.print(", ");*/

            tft.drawPixel(315 - realWidth1 + realX, realY + 5, Logo_CACS_199x81_565[i]);
        }
    }

    for (int realX = 0; realX < realWidth2; realX++)
    {
        for (int realY = 0; realY < realHeight2; realY++)
        {
            int x = round(realX / (float)realWidth2 * (float)arrWidth2);
            int y = round(realY / (float)realHeight2 * (float)arrHeight2);
            int i = round(1 * (x * arrHeight2 + y));
            /*Serial.print(tft.color565(
                Logo_CACS_199x81_565[i],
                Logo_CACS_199x81_565[i + 1],
                Logo_CACS_199x81_565[i + 2]));
            Serial.print(", ");*/

            tft.drawPixel(320 - realWidth2 + realX, 200 - realHeight2 + realY, logoPoleEspace_200x200_565[i]);
        }
    }

    // tft.drawRect(0, 0, width * rectSize, height * rectSize, tft.color565(255, 0, 0));
    // tft.drawBitmap(0, 0, chien, 100, 100, tft.color565(0, 0, 0));

    tft.setTextSize(2);
    tft.setTextColor(tft.color565(0, 0, 0));

    return true;
}

void Screen::SetInitTextToPrint(std::vector<std::pair<String, bool>> elementValueCouples)
{
    this->elementValueCouples = elementValueCouples;
}

void Screen::SetLoopTextToPrint(String text)
{
    loopTextToPrint = text;
}

void Screen::Update()
{
    // tft.setFont(&FreeMonoBoldOblique12pt7b);
    // tft.setFont(&FreeMonoBoldOblique12pt7b);

    tft.startWrite();

    tft.fillRect(0, 0, 160, 180, tft.color565(255, 255, 255));
    tft.fillRect(0, 180, 210, 30, tft.color565(255, 255, 255));
    tft.fillRect(0, 210, 300, 30, tft.color565(255, 255, 255));

    tft.setCursor(0, 10);
    unsigned long currentTime = millis();
    unsigned long minutes = currentTime / 60000;
    unsigned long seconds = (currentTime / 1000) % 60;
    unsigned long milliseconds = (currentTime % 1000) / 10;

    // tft.println(" Hello CACS!");
    tft.println(" " + String(minutes) + ":" + String(seconds) + ":" + String(milliseconds));
    //tft.println();

    for (std::pair<String, bool> elementValueCouple : elementValueCouples)
    {
        tft.setTextColor(tft.color565(0, 0, 0));
        tft.print(elementValueCouple.first);
        tft.setTextColor(elementValueCouple.second ? tft.color565(0, 150, 0) : tft.color565(150, 0, 0));
        tft.println(elementValueCouple.second ? "ON" : "OFF");
    }

    tft.setTextColor(tft.color565(0, 0, 0));
    //tft.println();
    tft.println(loopTextToPrint);

    tft.endWrite();
    delay(20);
}

void Screen::TurnOn()
{
    digitalWrite(TFT_LED, HIGH); // Allumer l'écran
}

void Screen::TurnOff()
{
    digitalWrite(TFT_LED, LOW); // Éteindre l'écran
}