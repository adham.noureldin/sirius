#include <Arduino.h>
#include <TinyGPSPlus.h>
#include <pgmspace.h>


// The serial connection to the GPS device
// #define GPSSERIAL Serial8 // ANAS : vaut mieux passer le serial en paramètre du constructeur

class GPS
{
private:
    HardwareSerial *GPSSerial;
    TinyGPSPlus gps;
    void GPS_SendConfig(const uint8_t *Progmem_ptr, uint8_t arraysize);
    // float latitude;
    // float longitude;
    //bool isOK;

public:
    GPS();
    bool Init(HardwareSerial *GPSSerial, unsigned long baud);
    void Update();
    // Sats HDOP  Latitude   Longitude   Fix  Date       Time     Date Alt    Course Speed Card  Distance Course Card  Chars Sentences Checksum
    uint32_t GetTime();
    double GetLatitude();
    double GetLongitude();
    uint32_t GetSatellites();
    double GetCourse();
    double GetHDOP();
    double GetAltitude();
    double GetSpeed();
    bool IsOK();
    void SmartDelay(unsigned long ms);
    void set_mode_sol();
};