#include <Arduino.h>
#include <TinyGPSPlus.h>
// #include <SoftwareSerial.h>
/*
   This sample code demonstrates the normal use of a TinyGPSPlus (TinyGPSPlus) object.
   It requires the use of SoftwareSerial, and assumes that you have a
   4800-baud serial GPS device hooked up on pins 4(rx) and 3(tx).
*/
// static const int RXPin = 4, TXPin = 3;
static const uint32_t GPSBaud = 9600;

// The TinyGPSPlus object
TinyGPSPlus gps;

// The serial connection to the GPS device
// SoftwareSerial ss(RXPin, TXPin);

#define ss Serial8

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
    unsigned long start = millis();
    do
    {
        while (ss.available())
            gps.encode(ss.read());
    } while (millis() - start < ms);
}

static void printFloat(float val, bool valid, int len, int prec)
{
    if (!valid)
    {
        while (len-- > 1)
            Serial.print('*');
        Serial.print(' ');
    }
    else
    {
        Serial.print(val, prec);
        int vi = abs((int)val);
        int flen = prec + (val < 0.0 ? 2 : 1); // . and -
        flen += vi >= 1000 ? 4 : vi >= 100 ? 3
                             : vi >= 10    ? 2
                                           : 1;
        for (int i = flen; i < len; ++i)
            Serial.print(' ');
    }
    smartDelay(0);
}

static void printInt(unsigned long val, bool valid, int len)
{
    char sz[32] = "*****************";
    if (valid)
        sprintf(sz, "%ld", val);
    sz[len] = 0;
    for (int i = strlen(sz); i < len; ++i)
        sz[i] = ' ';
    if (len > 0)
        sz[len - 1] = ' ';
    Serial.print(sz);
    smartDelay(0);
}

static void printDateTime(TinyGPSDate &d, TinyGPSTime &t)
{
    if (!d.isValid())
    {
        Serial.print(F("********** "));
    }
    else
    {
        char sz[32];
        sprintf(sz, "%02d/%02d/%02d ", d.month(), d.day(), d.year());
        Serial.print(sz);
    }

    if (!t.isValid())
    {
        Serial.print(F("******** "));
    }
    else
    {
        char sz[32];
        sprintf(sz, "%02d:%02d:%02d ", t.hour(), t.minute(), t.second());
        Serial.print(sz);
    }

    printInt(d.age(), d.isValid(), 5);
    smartDelay(0);
}

static void printStr(const char *str, int len)
{
    int slen = strlen(str);
    for (int i = 0; i < len; ++i)
        Serial.print(i < slen ? str[i] : ' ');
    smartDelay(0);
}

void setup()
{
    Serial.begin(115200);
    ss.begin(GPSBaud);

    Serial.println(F("FullExample.ino"));
    Serial.println(F("An extensive example of many interesting TinyGPSPlus features"));
    Serial.print(F("Testing TinyGPSPlus library v. "));
    Serial.println(TinyGPSPlus::libraryVersion());
    Serial.println(F("by Mikal Hart"));
    Serial.println();
    Serial.println(F("Sats HDOP  Latitude   Longitude   Fix  Date       Time     Date Alt    Course Speed Card  Distance Course Card  Chars Sentences Checksum"));
    Serial.println(F("           (deg)      (deg)       Age                      Age  (m)    --- from GPS ----  ---- to London  ----  RX    RX        Fail"));
    Serial.println(F("----------------------------------------------------------------------------------------------------------------------------------------"));
}

void loop()
{
    static const double LONDON_LAT = 51.508131, LONDON_LON = -0.128002;

    printInt(gps.satellites.value(), gps.satellites.isValid(), 5);
    printFloat(gps.hdop.hdop(), gps.hdop.isValid(), 6, 1);
    printFloat(gps.location.lat(), gps.location.isValid(), 11, 6);
    printFloat(gps.location.lng(), gps.location.isValid(), 12, 6);
    printInt(gps.location.age(), gps.location.isValid(), 5);
    printDateTime(gps.date, gps.time);
    printFloat(gps.altitude.meters(), gps.altitude.isValid(), 7, 2);
    printFloat(gps.course.deg(), gps.course.isValid(), 7, 2);
    printFloat(gps.speed.kmph(), gps.speed.isValid(), 6, 2);
    printStr(gps.course.isValid() ? TinyGPSPlus::cardinal(gps.course.deg()) : "*** ", 6);

    unsigned long distanceKmToLondon =
        (unsigned long)TinyGPSPlus::distanceBetween(
            gps.location.lat(),
            gps.location.lng(),
            LONDON_LAT,
            LONDON_LON) /
        1000;
    printInt(distanceKmToLondon, gps.location.isValid(), 9);

    double courseToLondon =
        TinyGPSPlus::courseTo(
            gps.location.lat(),
            gps.location.lng(),
            LONDON_LAT,
            LONDON_LON);

    printFloat(courseToLondon, gps.location.isValid(), 7, 2);

    const char *cardinalToLondon = TinyGPSPlus::cardinal(courseToLondon);

    printStr(gps.location.isValid() ? cardinalToLondon : "*** ", 6);

    printInt(gps.charsProcessed(), true, 6);
    printInt(gps.sentencesWithFix(), true, 10);
    printInt(gps.failedChecksum(), true, 9);
    Serial.println();

    smartDelay(1000);

    if (millis() > 5000 && gps.charsProcessed() < 10)
        Serial.println(F("No GPS data received: check wiring"));
}

//////////////////////////////
//////////////////////////////

// #include <Arduino.h>
// #include <GPS.h>

// GPS gps;
// void setup()
// {
//   Serial.begin(115200);
//   Serial8.begin(9600);
//   delay(100);
//   gps.Init(&Serial8, 9600);
// }

// void loop()
// {
//   gps.Update();
//   Serial.print("Nb Sat: ");
//   Serial.print(gps.GetSatellites());
//   Serial.print(" - HDOP: ");
//   Serial.print(gps.GetHDOP());
//   Serial.print(" - Latitude: ");
//   Serial.print(gps.GetLatitude(), 9);
//   Serial.print(" - Longitude: ");
//   Serial.print(gps.GetLongitude(), 9);
//   Serial.print(" - Time: ");
//   Serial.print(gps.GetTime());
//   Serial.print(" - IsOK: ");
//   Serial.print(gps.IsOK());
//   Serial.println();

//   // do // prints the NMEA data
//   // {
//   //   if (Serial8.available())
//   //   {
//   //     Serial.write(Serial8.read());
//   //   }
//   // } while (1);
// }

// Latitude: 48.708526833 - Longitude: 2.167877833 - Time: 13004460 // 10Hz no config varie bcp (5)

// 90_UBlox_GPS_Configuration Starting

// $GNRMC,180227.00,A,4842.52722,N,00210.00626,E,0.076,,230524,,,A,V*12
// $GNVTG,,T,,M,0.076,N,0.142,K,A*3B
// $GNGGA,180227.00,4842.52722,N,00210.00626,E,1,09,1.55,171.8,M,46.2,M,,*40
// $GNGSA,A,3,04,19,06,03,09,17,,,,,,,2.78,1.55,2.31,1*0B
// $GNGSA,A,3,10,,,,,,,,,,,,2.78,1.55,2.31,3*0E
// $GNGSA,A,3,20,19,,,,,,,,,,,2.78,1.55,2.31,4*02
// $GNGSA,A,3,,,,,,,,,,,,,2.78,1.55,2.31,5*09
// $GNGLL,4842.52722,N,00210.00626,E,180227.00,A,A*72
// $GNRMC,180228.00,A,4842.52727,N,00210.00627,E,0.120,,230524,,,A,V*1B
// $GNVTG,,T,,M,0.120,N,0.222,K,A*3C
// $GNGGA,180228.00,4842.52727,N,00210.00627,E,1,09,1.55,171.7,M,46.2,M,,*44
// $GNGSA,A,3,04,19,06,03,09,17,,,,,,,2.78,1.55,2.31,1*0B
// $GNGSA,A,3,10,,,,,,,,,,,,2.78,1.55,2.31,3*0E
// $GNGSA,A,3,20,19,,,,,,,,,,,2.78,1.55,2.31,4*02
// $GNGSA,A,3,,,,,,,,,,,,,2.78,1.55,2.31,5*09
// $GNGLL,4842.52727,N,00210.00627,E,180228.00,A,A*79

// ClearConfig GPSSend  B5 62 06 09 0D 00 FF FF 00 00 00 00 00 00 FF FF 00 00 01 19 98

// �b 	@$GNRMC,180229.00,A,4842.52733,N,00210.00630,E,0.088,,$GNRMC,180231.00,A,4842.52747,N,00210.00632,E,0.049,,230524,,,A,V*1F
// $GNVTG,,T,,M,0.049,N,0.091,K,A*38
// $GNGGA,180231.00,4842.52747,N,00210.00632,E,1,09,1.55,171.2,M,46.2,M,,*4B
// $GNGSA,A,3,04,19,06,03,09,17,,,,,,,2.78,1.55,2.31,1*0B
// $GNGSA,A,3,10,,,,,,,,,,,,2.78,1.55,2.31,3*0E
// $GNGSA,A,3,20,19,,,,,,,,,,,2.78,1.55,2.31,4*02
// $GNGSA,A,3,,,,,,,,,,,,,2.78,1.55,2.31,5*09
// $GNGLL,4842.52747,N,00210.00632,E,180231.00,A,A*73
// $GNRMC,180232.00,A,4842.52754,N,00210.00635,E,0.020,,230524,,,A,V*16
// $GNVTG,,T,,M,0.020,N,0.036,K,A*3A
// $GNGGA,180232.00,4842.52754,N,00210.00635,E,1,09,1.55,171.0,M,46.2,M,,*4F
// $GNGSA,A,3,04,19,06,03,09,17,,,,,,,2.78,1.55,2.31,1*0B
// $GNGSA,A,3,10,,,,,,,,,,,,2.78,1.55,2.31,3*0E
// $GNGSA,A,3,20,19,,,,,,,,,,,2.78,1.55,2.31,4*02
// $GNGSA,A,3,,,,,,,,,,,,,2.78,1.55,2.31,5*09
// $GNGLL,4842.52754,N,00210.00635,E,180232.00,A,A*75

// GPGLLOff GPSSend  B5 62 06 01 08 00 F0 01 00 00 00 00 00 01 01 2B
// GPGSVOff GPSSend  B5 62 06 01 08 00 F0 03 00 00 00 00 00 01 03 39
// GPVTGOff GPSSend  B5 62 06 01 08 00 F0 05 00 00 00 00 00 01 05 47
// GPGSAOff GPSSend  B5 62 06 01 08 00 F0 02 00 00 00 00 00 01 02 32
// Navrate10hz GPSSend  B5 62 06 08 06 00 64 00 01 00 01 00 7A 12

// �b 8$GNRMC,180233.00,A,4842.52759,N,00210.00639,E,0.059,,60,N,00210.00626,E,0.107,,230524,,,D,V*1C
// $GNGGA,180234.90,4842.52660,N,00210.00626,E,2,08,1.56,173.7,M,46.2,M,,*40
// $GNRMC,180235.00,A,4842.52657,N,00210.00623,E,0.059,,230524,,,D,V*1F
// $GNGGA,180235.00,4842.52657,N,00210.00623,E,2,08,1.56,173.8,M,46.2,M,,*46
// $GNRMC,180235.10,A,4842.52654,N,00210.00622,E,0.068,,230524,,,D,V*1E
// $GNGGA,180235.10,4842.52654,N,00210.00622,E,2,08,1.56,173.9,M,46.2,M,,*44
// $GNRMC,180235.20,A,4842.52652,N,00210.00623,E,0.136,,230524,,,D,V*10
// $GNGGA,180235.20,4842.52652,N,00210.00623,E,2,08,1.56,174.0,M,46.2,M,,*4E
// $GNRMC,180235.30,A,4842.52649,N,00210.00621,E,0.038,,230524,,,D,V*16
// $GNGGA,180235.30,4842.52649,N,00210.00621,E,2,08,1.56,174.1,M,46.2,M,,*46
// $GNRMC,180235.40,A,4842.52647,N,00210.00620,E,0.052,,230524,,,D,V*12
// $GNGGA,180235.40,4842.52647,N,00210.00620,E,2,08,1.56,174.2,M,46.2,M,,*4D
// $GNRMC,180235.50,A,4842.52645,N,00210.00619,E,0.055,,230524,,,D,V*1C
// $GNGGA,180235.50,4842.52645,N,00210.00619,E,2,09,1.55,174.3,M,46.2,M,,*47
// $GNRMC,180235.60,A,4842.52643,N,00210.00618,E,0.108,,230524,,,D,V*11
// $GNGGA,180235.60,4842.52643,N,00210.00618,E,2,09,1.55,174.4,M,46.2,M,,*44
// $GNRMC,180235.70,A,4842.52641,N,00210.00616,E,0.121,,230524,,,D,V*17
// $GNGGA,180235.70,4842.52641,N,00210.00616,E,2,09,1.55,174.5,M,46.2,M,,*48
// $GNRMC,180235.80,A,4842.52639,N,00210.00615,E,0.122,,230524,,,D,V*17
// $GNGGA,180235.80,4842.52639,N,00210.00615,E,2,09,1.55,174.5,M,46.2,M,,*4B
// $GNRMC,180235.90,A,4842.52638,N,00210.00615,E,0.068,,230524,,,D,V*18
// $GNGGA,180235.90,4842.52638,N,00210.00615,E,2,09,1.55,174.6,M,46.2,M,,*48
// $GNRMC,180236.00,A,4842.52636,N,00210.00614,E,0.005,,230524,,,D,V*16
// $GNGGA,180236.00,4842.52636,N,00210.00614,E,2,09,1.55,174.7,M,46.2,M,,*4C
// $GNRMC,180236.10,A,4842.52635,N,00210.00614,E,0.090,,230524,,,D,V*18
// $GNGGA,180236.10,4842.52635,N,00210.00614,E,2,09,1.55,174.8,M,46.2,M,,*41
// $GNRMC,180236.20,A,4842.52633,N,00210.00614,E,0.107,,230524,,,D,V*12
// $GNGGA,180236.20,4842.52633,N,00210.00614,E,2,09,1.55,174.9,M,46.2,M,,*45
// $GNRMC,180236.30,A,4842.52631,N,00210.00614,E,0.026,,230524,,,D,V*13
// $GNGGA,180236.30,4842.52631,N,00210.00614,E,2,09,1.55,175.0,M,46.2,M,,*4E
// $GNRMC,180236.40,A,4842.52630,N,00210.00614,E,0.098,,230524,,,D,V*10
// $GNGGA,180236.40,4842.52630,N,00210.00614,E,2,08,1.57,175.1,M,46.2,M,,*4A
// $GNRMC,180236.50,A,4842.52629,N,00210.00614,E,0.056,,230524,,,D,V*1B
// $GNGGA,180236.50,4842.52629,N,00210.00614,E,2,08,1.57,175.2,M,46.2,M,,*40
// $GNRMC,180236.60,A,4842.52627,N,00210.00614,E,0.103,,230524,,,D,V*17
// $GNGGA,180236.60,4842.52627,N,00210.00614,E,2,08,1.57,175.3,M,46.2,M,,*4C
// $GNRMC,180236.70,A,4842.52626,N,00210.00615,E,0.109,,230524,,,D,V*1C
// $GNGGA,180236.70,4842.52626,N,00210.00615,E,2,08,1.57,175.4,M,46.2,M,,*4A
// $GNRMC,180236.80,A,4842.52626,N,00210.00616,E,0.050,,230524,,,D,V*1D
// $GNGGA,180236.80,4842.52626,N,00210.00616,E,2,08,1.57,175.5,M,46.2,M,,*47
// $GNRMC,180236.90,A,4842.52625,N,00210.00617,E,0.090,,230524,,,D,V*12
// $GNGGA,180236.90,4842.52625,N,00210.00617,E,2,08,1.57,175.6,M,46.2,M,,*47
// $GNRMC,180237.00,A,4842.52624,N,00210.00618,E,0.147,,230524,,,D,V*1F
// $GNGGA,180237.00,4842.52624,N,00210.00618,E,2,08,1.57,175.7,M,46.2,M,,*40
// $GNRMC,180237.10,A,4842.52624,N,00210.00618,E,0.096,,230524,,,D,V*13
// $GNGGA,180237.10,4842.52624,N,00210.00618,E,2,08,1.57,175.7,M,46.2,M,,*41
// $GNRMC,180237.20,A,4842.52622,N,00210.00619,E,0.179,,230524,,,D,V*17
// $GNGGA,180237.20,4842.52622,N,00210.00619,E,2,08,1.57,175.8,M,46.2,M,,*4A
// $GNRMC,180237.30,A,4842.52623,N,00210.00621,E,0.101,,230524,,,D,V*13
// $GNGGA,180237.30,4842.52623,N,00210.00621,E,2,08,1.57,175.9,M,46.2,M,,*40
// $GNRMC,180237.40,A,4842.52624,N,00210.00621,E,0.140,,230524,,,D,V*16
// $GNGGA,180237.40,4842.52624,N,00210.00621,E,2,08,1.57,176.0,M,46.2,M,,*4A
// $GNRMC,180237.50,A,4842.52624,N,00210.00622,E,0.085,,230524,,,D,V*1C
// $GNGGA,180237.50,4842.52624,N,00210.00622,E,2,08,1.57,176.0,M,46.2,M,,*48
// $GNRMC,180237.60,A,4842.52623,N,00210.00623,E,0.029,,230524,,,D,V*1F
// $GNGGA,180237.60,4842.52623,N,00210.00623,E,2,08,1.57,176.1,M,46.2,M,,*4C
// $GNRMC,180237.70,A,4842.52623,N,00210.00623,E,0.125,,230524,,,D,V*13
// $GNGGA,180237.70,4842.52623,N,00210.00623,E,2,08,1.57,176.2,M,46.2,M,,*4E
// $GNRMC,180237.80,A,4842.52623,N,00210.00624,E,0.132,,230524,,,D,V*1D
// $GNGGA,180237.80,4842.52623,N,00210.00624,E,2,08,1.57,176.3,M,46.2,M,,*47
// $GNRMC,180237.90,A,4842.52623,N,00210.00624,E,0.063,,230524,,,D,V*19
// $GNGGA,180237.90,4842.52623,N,00210.00624,E,2,08,1.57,176.3,M,46.2,M,,*46
// $GNRMC,180238.00,A,4842.52624,N,00210.00624,E,0.045,,230524,,,D,V*1C
// $GNGGA,180238.00,4842.52624,N,00210.00624,E,2,08,1.57,176.4,M,46.2,M,,*40
// $GNRMC,180238.10,A,4842.52624,N,00210.00624,E,0.056,,230524,,,D,V*1F
// $GNGGA,180238.10,4842.52624,N,00210.00624,E,2,08,1.57,176.4,M,46.2,M,,*41
// $GNRMC,180238.20,A,4842.52624,N,00210.00624,E,0.046,,230524,,,D,V*1D
// $GNGGA,180238.20,4842.52624,N,00210.00624,E,2,08,1.57,176.5,M,46.2,M,,*43
// $GNRMC,180238.30,A,4842.52625,N,00210.00625,E,0.023,,230524,,,D,V*1F
// $GNGGA,180238.30,4842.52625,N,00210.00625,E,2,08,1.57,176.5,M,46.2,M,,*42
// $GNRMC,180238.40,A,4842.52625,N,00210.00625,E,0.042,,230524,,,D,V*1F
// $GNGGA,180238.40,4842.52625,N,00210.00625,E,2,08,1.57,176.6,M,46.2,M,,*46
// $GNRMC,180238.50,A,4842.52625,N,00210.00625,E,0.022,,230524,,,D,V*18
// $GNGGA,180238.50,4842.52625,N,00210.00625,E,2,08,1.57,176.6,M,46.2,M,,*47
// $GNRMC,180238.60,A,4842.52625,N,00210.00626,E,0.072,,230524,,,D,V*1D
// $GNGGA,180238.60,4842.52625,N,00210.00626,E,2,08,1.57,176.7,M,46.2,M,,*46
// $GNRMC,180238.70,A,4842.52625,N,00210.00626,E,0.012,,230524,,,D,V*1A
// $GNGGA,180238.70,4842.52625,N,00210.00626,E,2,08,1.57,176.7,M,46.2,M,,*47
// $GNRMC,180238.80,A,4842.52626,N,00210.00627,E,0.084,,230524,,,D,V*18
// $GNGGA,180238.80,4842.52626,N,00210.00627,E,2,08,1.57,176.7,M,46.2,M,,*4A
// $GNRMC,180238.90,A,4842.52626,N,00210.00628,E,0.075,,230524,,,D,V*18
// $GNGGA,180238.90,4842.52626,N,00210.00628,E,2,08,1.57,176.8,M,46.2,M,,*4B
// $GNRMC,180239.00,A,4842.52626,N,00210.00628,E,0.062,,230524,,,D,V*16
// $GNGGA,180239.00,4842.52626,N,00210.00628,E,2,08,1.57,176.8,M,46.2,M,,*43
// $GNRMC,180239.10,A,4842.52626,N,00210.00628,E,0.169,,230524,,,D,V*1D
// $GNGGA,180239.10,4842.52626,N,00210.00628,E,2,08,1.57,176.9,M,46.2,M,,*43
// $GNRMC,180239.20,A,4842.52627,N,00210.00630,E,0.190,,230524,,,D,V*10
// $GNGGA,180239.20,4842.52627,N,00210.00630,E,2,08,1.61,176.9,M,46.2,M,,*4D
// $GNRMC,180239.30,A,4842.52628,N,00210.00630,E,0.161,,230524,,,D,V*10
// $GNGGA,180239.30,4842.52628,N,00210.00630,E,2,08,1.57,176.9,M,46.2,M,,*46
// $GNRMC,180239.40,A,4842.52628,N,00210.00631,E,0.088,,230524,,,D,V*10
// $GNGGA,180239.40,4842.52628,N,00210.00631,E,2,08,1.57,176.9,M,46.2,M,,*40
// $GNRMC,180239.50,A,4842.52627,N,00210.00631,E,0.178,,230524,,,D,V*10
// $GNGGA,180239.50,4842.52627,N,00210.00631,E,2,08,1.57,177.0,M,46.2,M,,*46
// $GNRMC,18023