#include "imu.h"

#define INTERRUPT_PIN freq

// IMPORTANT: en fonction du type d'IMU, il faut décommenter la fonction Init et Update correspondante, et commenter les autres.

IMU::IMU()
{
}

uint64_t IMU::getMicros()
{
    unsigned long currentMicros = micros();

    // Si currentMicros est inférieur à previousMicros, alors il y a eu un débordement
    if (currentMicros < previousMicros)
    {
        overflowCount++;
    }

    previousMicros = currentMicros;

    // Calculer le temps total en microsecondes en tenant compte des débordements
    return currentMicros + (overflowCount * 4294967296);
}

void IMU::ActualiserLesTemps()
{
    dt = getMicros() - tMesure;
    tMesure = tMesure + dt;
}

///// Init et Update du SparkFunMPU9250 /////
#if NOM_IMU == SparkFunMPU9250
void IMU::Init()
{
#if PRINT_SERIAL_MODE
    Serial.println("Initialize IMU");
#endif
    pinMode(INTERRUPT_PIN, INPUT_PULLUP);
    if (imu.begin() != INV_SUCCESS)
    {
        while (1)
        {
#if PRINT_SERIAL_MODE
            Serial.println("Unable to communicate with MPU-9250");
            Serial.println("Check connections, and try again.");
            Serial.println();
#endif
            delay(1000);
        }
    }
    // Enable all sensors, and set sample rates to <freq>Hz.
    // (Slow so we can see the interrupt work.)
    imu.setSensors(INV_XYZ_GYRO | INV_XYZ_ACCEL | INV_XYZ_COMPASS);
    imu.setSampleRate(freq);        // Set accel/gyro sample rate to <freq>Hz
    imu.setCompassSampleRate(freq); // Set mag rate to <freq>Hz
    // Use enableInterrupt() to configure the MPU-9250's
    // interrupt output as a "data ready" indicator.
    imu.enableInterrupt();
    // The interrupt level can either be active-high or low.
    // Configure as active-low, since we'll be using the pin's
    // internal pull-up resistor.
    // Options are INT_ACTIVE_LOW or INT_ACTIVE_HIGH
    imu.setIntLevel(INT_ACTIVE_LOW);
    // The interrupt can be set to latch until data has
    // been read, or to work as a 50us pulse.
    // Use latching method -- we'll read from the sensor
    // as soon as we see the pin go LOW.
    // Options are INT_LATCHED or INT_50US_PULSE
    imu.setIntLatched(INT_LATCHED);
#if PRINT_SERIAL_MODE
    Serial.println("IMU Initialized");
#endif
}

void IMU::Update() // renvoie un tableau d'éléments de type "byte"
{                  // After calling update() the ax, ay, az, gx, gy, gz, mx,
    // my, mz, time, and/or temerature class variables are all
    // updated. Access them by placing the object. in front:

    // Use the calcAccel, calcGyro, and calcMag functions to
    // convert the raw sensor readings (signed 16-bit values)
    // to their respective units.
    float accX = imu.calcAccel(imu.ax);
    float accY = imu.calcAccel(imu.ay);
    float accZ = imu.calcAccel(imu.az);
    float gyroX = imu.calcGyro(imu.gx);
    float gyroY = imu.calcGyro(imu.gy);
    float gyroZ = imu.calcGyro(imu.gz);
    ActualiserLesTemps();

    acc[0] = accX;
    acc[1] = accY;
    acc[2] = accZ;
    gyr[0] = gyroX;
    gyr[1] = gyroY;
    gyr[2] = gyroZ;

    imu.update(UPDATE_ACCEL | UPDATE_GYRO | UPDATE_COMPASS);
}
#endif

///// Init et Update du MPU6050 /////
#if NOM_IMU == MPU6050
void IMU::Init()
// Initialise l'IMU (c'est tiré d'un exemple de la librairie)
{
#if PRINT_SERIAL_MODE
    Serial.println("Initialize IMU");
#endif
    if (!mpu.begin())
    { // Change address if needed
#if PRINT_SERIAL_MODE
        Serial.println("Echec de l'initialisation de l'IMU");
#endif
        while (1)
        {
            delay(10);
        }
    }

    mpu.setAccelerometerRange(MPU6050_RANGE_16_G);
    mpu.setGyroRange(MPU6050_RANGE_250_DEG);
    mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
#if PRINT_SERIAL_MODE
    Serial.println("IMU Initialized");
#endif
}

void IMU::Update()
// Met à jour les données de l'IMU (c'est tiré d'un exemple de la librairie)
{
    sensors_event_t a, g, temp;
    mpu.getEvent(&a, &g, &temp);
    float accX = a.acceleration.x;
    float accY = a.acceleration.y;
    float accZ = a.acceleration.z;
    float gyroX = g.gyro.x;
    float gyroY = g.gyro.y;
    float gyroZ = g.gyro.z;

    ActualiserLesTemps();

    acc[0] = accX;
    acc[1] = accY;
    acc[2] = accZ;
    gyr[0] = gyroX;
    gyr[1] = gyroY;
    gyr[2] = gyroZ;
}
#endif

#if NOM_IMU == IMU3
void IMU::Init(int cspin)
{
    cspin = cspin;
    ICM42688 = DFRobot_ICM42688_SPI(cspin);
#if PRINT_SERIAL_MODE
    Serial.println("Initialize IMU");
#endif
    int ret;
    int cpt = 0;
    while ((ret = ICM42688.begin()) != 0 && cpt < 10)
    {
        if (ret == -1)
        {
#if PRINT_SERIAL_MODE
            Serial.println("bus data access error");
#endif
        }
        else
#if PRINT_SERIAL_MODE
            Serial.println("Chip versions do not match");
#endif
        delay(1000);
        cpt++;
    }
#if PRINT_SERIAL_MODE
    if (cpt == 10)
    {
        Serial.println("IMU not initialized");
        imu_on = false;
    }
    else
    {
        Serial.println("IMU initialized !");
        imu_on = true;
    }
#endif
    if (imu_on)
    {
        ICM42688.setODRAndFSR(/* who= */ GYRO, /* ODR= */ ODR_16KHZ, /* FSR = */ FSR_1);
        delay(200);
        ICM42688.setODRAndFSR(/* who= */ ACCEL, /* ODR= */ ODR_16KHZ, /* FSR = */ FSR_0);
        delay(200);
        // ICM42688.startTempMeasure();
        ICM42688.startGyroMeasure(/* mode= */ LN_MODE);
        delay(200);
        ICM42688.startAccelMeasure(/* mode= */ LN_MODE);
        delay(200);
        // ICM42688.startFIFOMode(); // this makes acquisition faster
        delay(1000);
    }
}

void IMU::Update()
{
    // ICM42688.getFIFOData();
    float accX = ICM42688.getAccelDataX();
    float accY = ICM42688.getAccelDataY();
    float accZ = ICM42688.getAccelDataZ();
    float gyroX = ICM42688.getGyroDataX();
    float gyroY = ICM42688.getGyroDataY();
    float gyroZ = ICM42688.getGyroDataZ();

    ActualiserLesTemps();
    // L'accélération est en mg (milli-g), le gyroscope en degrés par seconde
    acc[0] = accX * 9.81 / 1000.0;
    acc[1] = accY * 9.81 / 1000.0;
    acc[2] = accZ * 9.81 / 1000.0;
    gyr[0] = gyroX * DEG_TO_RAD;
    gyr[1] = gyroY * DEG_TO_RAD;
    gyr[2] = gyroZ * DEG_TO_RAD;
}
#endif

bool IMU::IsON()
{
    return imu_on;
}

void IMU::Calibrate()
// Calibrage de l'IMU: on fait la moyenne des mesures de gyro pendant 5 secondes,
// ce qui permet de déterminer le biais de gyro (tableau gyr_err)
{
    uint64_t ti = getMicros();
    int nb_mes = 0;
    while (micros() - ti < 5000000)
    {
        Update();
        gyr_err[0] += gyr[0];
        gyr_err[1] += gyr[1];
        gyr_err[2] += gyr[2];
        acc0[0] += acc[0];
        acc0[1] += acc[1];
        acc0[2] += acc[2];
        NormAcc0 += calculateNorm(acc);
        nb_mes++;
        delay(2);
    }
    Serial.println(nb_mes);
    for (int i = 0; i < 3; i++)
    {
        gyr_err[i] /= nb_mes;
        acc0[i] /= nb_mes;
    }
    NormAcc0 /= nb_mes;
}

void IMU::UpdateCal()
// Remplace Update() dans le reste du code pour avoir des données calibrées
{
    Update();
    for (int i = 0; i < 3; i++)
    {
        gyr[i] -= gyr_err[i]; // On enlève le biais de gyro
    }
}

// Getters

float *IMU::GetAcc()
{
    return acc;
}

float *IMU::GetGyr()
{
    return gyr;
}

float *IMU::Getp()
{
    return p;
}

float *IMU::Getv()
{
    return v;
}

float *IMU::Getq()
{
    return q;
}

float IMU::GetTheta()
{
    return theta;
}

float IMU::GetAlpha()
{
    return alpha;
}

uint64_t IMU::Getdt()
{
    return dt;
}

void IMU::Printpv()
{
#if PRINT_SERIAL_MODE
    Serial.print("p ");
    for (int i = 0; i < 3; i++)
    {
        Serial.print(p[i]);
        Serial.print(" ");
    }
    Serial.println();
    Serial.print("v ");
    for (int i = 0; i < 3; i++)
    {
        Serial.print(v[i]);
        Serial.print(" ");
    }
    Serial.println();
#endif
}

void IMU::PrintThetaAlpha()
{
#if PRINT_SERIAL_MODE
    Serial.print("Theta: ");
    Serial.print(theta * RAD_TO_DEG);
    Serial.print(" Alpha: ");
    Serial.println(alpha * RAD_TO_DEG);
#endif
}

void IMU::GetDataAsString()
{
#if PRINT_SERIAL_MODE
    for (int i = 0; i < (acc[0] + 2.0f) / 4.0f * 80; i++)
    {
        Serial.print("*");
    }
#endif
}

bool IMU::AnglesOK()
{
#ifdef FLAG_ANGLES_RAMPE
    return (theta0 - dtheta < theta && theta < theta0 + dtheta && alpha0 - dalpha < alpha && alpha < alpha0 + dalpha);
#else
    return (theta_sep_stabtraj - dtheta < theta && theta < theta_sep_stabtraj + dtheta && alpha0 - dalpha < alpha && alpha < alpha0 + dalpha);
#endif
}

bool IMU::ThetaOK()
{
    return (theta_sep_stabtraj - dtheta < theta && theta < theta_sep_stabtraj + dtheta);
}

bool IMU::AlphaOK()
{
    return (alpha0 - dalpha < alpha && alpha < alpha0 + dalpha);
}

// Calcul de beta (cf pdf)
float IMU::calculateBeta(float alpha0, float U[3])
{
    float Ux = U[0];
    float Uy = U[1];
    float Uz = U[2];
    float beta = -alpha0 + atan2(Ux * Uz / (sqrt(Ux * Ux * Uy * Uy + Ux * Ux * Uz * Uz + Uy * Uy * Uy * Uy - 2 * Uy * Uy * Uz * Uz + Uz * Uz * Uz * Uz)), -Uy / sqrt(Uy * Uy + Uz * Uz));
    return beta;
}

void IMU::calculateRotationMatrix(float beta, float *U, float RUbeta[3][3])
// Fonction pour calculer la matrice de rotation d'axe U et d'angle beta
{
    float Ux = U[0];
    float Uy = U[1];
    float Uz = U[2];
    // On actualise RUbeta
    RUbeta[0][0] = cos(beta);
    RUbeta[0][1] = -sin(beta);
    RUbeta[0][2] = 0;
    RUbeta[1][0] = sin(beta);
    RUbeta[1][1] = cos(beta);
    RUbeta[1][2] = 0;
    RUbeta[2][0] = 0;
    RUbeta[2][1] = 0;
    RUbeta[2][2] = 1;
}

void IMU::rotMatrixToQuat(float M[3][3], float *q)
// Prend en entree une matrice de rotation et modifie le quaternion q en conséquence (c'est le code du pdf)
{
    float T = M[0][0] + M[1][1] + M[2][2]; // Trace de M
    // On détermine le max entre la trace et les coeff diagonaux pour garantir la stabilite numerique
    float temp1 = max(M[0][0], M[1][1]);
    float temp2 = max(M[2][2], T);
    float m = max(temp1, temp2);
    float qmax = 0.5 * sqrt(1.0 - T + 2.0 * m);

    if (m == M[0][0])
    {
        q[0] = qmax;
        q[1] = (M[0][1] + M[1][0]) / (4.0 * qmax);
        q[2] = (M[0][2] + M[2][0]) / (4.0 * qmax);
        q[3] = (M[2][1] - M[1][2]) / (4.0 * qmax);
    }
    else if (m == M[1][1])
    {
        q[0] = (M[0][1] + M[1][0]) / (4.0 * qmax);
        q[1] = qmax;
        q[2] = (M[1][2] + M[2][1]) / (4.0 * qmax);
        q[3] = (M[0][2] - M[2][0]) / (4.0 * qmax);
    }
    else if (m == M[2][2])
    {
        q[0] = (M[0][2] + M[2][0]) / (4.0 * qmax);
        q[1] = (M[1][2] + M[2][1]) / (4.0 * qmax);
        q[2] = qmax;
        q[3] = (M[1][0] - M[0][1]) / (4.0 * qmax);
    }
    else
    {
        q[0] = (M[2][1] - M[1][2]) / (4.0 * qmax);
        q[1] = (M[0][2] - M[2][0]) / (4.0 * qmax);
        q[2] = (M[1][0] - M[0][1]) / (4.0 * qmax);
        q[3] = qmax;
    }
}

// Calcul de l'assiette
void IMU::calculateTheta()
{
    float qi = q[0];
    float qj = q[1];
    theta = M_PI_2 - acos(1.0 - 2.0 * (qi * qi + qj * qj));
}
float calculate_continuous_azimuth(float previous_azimuth, float new_azimuth)
{
    float delta = new_azimuth - previous_azimuth;

    // Adjust delta to the range [-180, 180] to handle wrap-around
    if (delta > M_PI)
    {
        delta -= M_TWOPI;
    }
    else if (delta < -M_PI)
    {
        delta += M_TWOPI;
    }

    // Update the continuous azimuth
    return previous_azimuth + delta;
}
// Calcul de l'azimuth
void IMU::calculateAlpha()
{
    float qi = q[0];
    float qj = q[1];
    float qk = q[2];
    float qw = q[3];
    float y = -2.0 * (qj * qw + qi * qk);
    float x = 2.0 * (qi * qw - qj * qk);
    float last_alpha = alpha;
    alpha = atan2(y, x);
    alpha = calculate_continuous_azimuth(last_alpha, alpha);
}

// Calcul de la norme d'un vecteur U
float IMU::calculateNorm(float *V)
{
    float accX = V[0];
    float accY = V[1];
    float accZ = V[2];
    return sqrt(accX * accX + accY * accY + accZ * accZ);
}

// Produit vectoriel de deux vecteurs U et V
void CrossProduct(float *U, float *V, float *UcroixV)
{
    UcroixV[0] = U[1] * V[2] - U[2] * V[1];
    UcroixV[1] = U[2] * V[0] - U[0] * V[2];
    UcroixV[2] = U[0] * V[1] - U[1] * V[0];
}

// Calcul du produit d'une matrice M par un vecteur colonne C (représenté par un tableau) MARCHE PAS (à voir où est le pb)
void ProduitMatCol(float M[3][3], float *C, float *res)
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            res[i] = res[i] + M[i][j] * C[j];
        }
    }
}

// Multiplication de tous les termes d'un tableau tab par un nombre mult
void produit(float *tab, float mult, float *res, int taille)
{
    for (int i = 0; i < taille; i++)
    {
        res[i] = tab[i] * mult;
    }
}

// Pour la multiplication par dt
void produit_dt(float *tab, uint64_t mult, float *res, int taille)
{
    for (int i = 0; i < taille; i++)
    {
        res[i] = tab[i] * (float)mult / 1000000.0;
    }
}

// Somme de deux tableaux tab1 et tab2 de même taille
void somme(float *tab1, float *tab2, float *res, int taille)
{
    for (int i = 0; i < taille; i++)
    {
        res[i] = tab1[i] + tab2[i];
    }
}

// Transposition d'une matrice M
void transpose(float M[3][3], float Mtransp[3][3])
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            Mtransp[i][j] = M[j][i];
        }
    }
}

// Calcul du "quaternion de changement de base" initial: q0
void IMU::Calculateq0()
{
    tMesure = getMicros(); // On initialise tMesure

    // Calcul de U (coordonnées de U dans la base de la fusée)
    float U[3] = {};
    produit(acc0, 1.0 / NormAcc0, U, 3);

    // Calcul de tN
    // float X0[3] = {1.0,
    //                0.0,
    //                0.0}; // Coordonnées de X0 dans la base (X0,Y0,Z0)
    // float UcroixX0[3] = {};
    // CrossProduct(U, X0, UcroixX0);
    // float tN[3] = {U[1], -U[0], 0};
    float tN[3] = {0, U[2], -U[1]};
    // produit(UcroixX0, 1.0 / calculateNorm(UcroixX0), tN, 3);
    produit(tN, 1.0 / calculateNorm(tN), tN, 3);

    float tE[3] = {
        tN[0] * U[1] - tN[1] * U[0],
        tN[1] * U[2] - tN[2] * U[1],
        tN[2] * U[0] - tN[0] * U[2]};   // tE = tN x U (produit vectoriel)
    produit(tE, 1.0 / calculateNorm(tE), tE, 3);

    // Calcul de beta (on prend alpha0 = 0, ce n'est pas dérangeant)
    float beta = -calculateBeta(0.0, U);

    // Calcul de la matrice de rotation d'axe U et d'angle beta (RUbeta)
    calculateRotationMatrix(beta, U, Rbeta);
    // Calcul de N
    float N[3] = {};
    ProduitMatCol(Rbeta, tN, N);
    produit(N, 1.0 / calculateNorm(N), N, 3);

    // Calcul de E
    float E[3] = {};
    CrossProduct(N, U, E);
    produit(E, 1.0 / calculateNorm(E), E, 3);

    // Création de M0
    float temp[3][3] = {{tE[0], tN[0], U[0]},
                      {tE[1], tN[1], U[1]},
                      {tE[2], tN[2], U[2]}};

    // Calcul de M0Rbeta
    float M0[3][3] = {};
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            for (int k = 0; k < 3; k++)
            {
                M0[i][j] += temp[i][k] * Rbeta[k][j];
            }
        }
    }

    float M0T[3][3] = {};
    transpose(M0, M0T); // Calcul de la transposée de M0
    // Calcul de q
    rotMatrixToQuat(M0T, q);
    // On normalise q
    float normq = sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);
    produit(q, 1.0 / normq, q, 4);

    ActualiserDonnees(false);
    alpha0 = GetAlpha();
    theta0 = GetTheta();
    for (int i = 0; i < 4; i++)
    {
        q0[i] = q[i];
    }

    // calculateRotationMatrix(alpha0, U, RUbeta);
    // // M0T = M0T @ RUbeta
    // float M0T2[3][3] = {0};
    // for (int i = 0; i < 3; i++)
    // {
    //     for (int j = 0; j < 3; j++)
    //     {
    //         for (int k = 0; k < 3; k++)
    //         {
    //             M0T2[i][j] += M0T[i][k] * RUbeta[k][j];
    //         }
    //     }
    // }

    // rotMatrixToQuat(M0T2, q);
    // // On normalise q
    // normq = sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);
    // produit(q, 1.0 / normq, q, 4);

    // ActualiserDonnees(false);
    // alpha0 = GetAlpha();
    // theta0 = GetTheta();
    // alpha_sep_stabtraj = GetAlpha();
    // for (int i = 0; i < 4; i++)
    // {
    //     q0[i] = q[i];
    // }
}

void IMU::ActualiserDonnees(bool actualiser_pos_et_vitesse)
// Actualise v, p et q avec la méthode d'Euler explicite.
// Avant l'appel de la fonction, acc et gyr contiennent des données correspondant au temps tMesure,
// et v,p et q contiennent des données correspondant au temps tMesure - dt.
// Après l'appel, v,p et q contiennent des données correspondant au temps tMesure.
{
    UpdateCal();
    float vx = v[0];
    float vy = v[1];
    float vz = v[2];
    float px = p[0];
    float py = p[1];
    float pz = p[2];
    float qi = q[0];
    float qj = q[1];
    float qk = q[2];
    float qw = q[3];
    float ax = acc[0];
    float ay = acc[1];
    float az = acc[2];
    float gx = gyr[0];
    float gy = gyr[1];
    float gz = gyr[2];

    // Calcul du vecteur f(x[k-1],u[k]) du pdf
    float f[10] = {(1.0 - 2.0 * (qj * qj + qk * qk)) * ax + 2.0 * (qi * qj - qk * qw) * ay + 2.0 * (qi * qk + qj * qw) * az,
                   2.0 * (qi * qj + qk * qw) * ax + (1.0 - 2.0 * (qi * qi + qk * qk)) * ay + 2.0 * (qi * qk - qi * qw) * az,
                   2.0 * (qi * qk - qj * qw) * ax + 2.0 * (qj * qk + qi * qw) * ay + (1.0 - 2.0 * (qi * qi + qj * qj)) * az - NormAcc0,
                   vx,
                   vy,
                   vz,
                   0.5 * (gx * qw - gy * qk + gz * qj),
                   0.5 * (gx * qk + gy * qw - gz * qi),
                   0.5 * (-gx * qj + gy * qi + gz * qw),
                   0.5 * (-gx * qi - gy * qj - gz * qk)};

    // Calcul du vecteur x du pdf (avec la méthode d'Euler explicite)
    float xprec[10] = {vx, vy, vz, px, py, pz, qi, qj, qk, qw};

    float fdt[10] = {};
    // Serial.print(dt,10);
    // Serial.print(" ");
    produit_dt(f, dt, fdt, 10); // fdt = f*dt = f(x[k-1],u[k])*(t[k]-t[k-1])
    // Serial.print(fdt[0],10);
    // Serial.print(" ");

    float x[10] = {};
    somme(xprec, fdt, x, 10); // x[k] = x[k-1] + fdt

    // Actualisation des vecteurs v, p et q
    if (actualiser_pos_et_vitesse)
    {
        v[0] = x[0];
        v[1] = x[1];
        v[2] = x[2];
        p[0] = x[3];
        p[1] = x[4];
        p[2] = x[5];
    }
    else // Si la fusée n'a pas décollé, on maintient v et p à 0 pour éviter que ça drifte
    {
        v[0] = 0.0;
        v[1] = 0.0;
        v[2] = 0.0;
        p[0] = 0.0;
        p[1] = 0.0;
        p[2] = 0.0;
    }

    q[0] = x[6];
    q[1] = x[7];
    q[2] = x[8];
    q[3] = x[9];
    // On normalise q
    float normq = sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);
    produit(q, 1.0 / normq, q, 4);
    calculateTheta();
    calculateAlpha();

    // Serial.print(x[0],10);
    // Serial.println();
}

void IMU::shutdown()
{
    ICM42688.sotpFIFOMode();
}

void IMU::setGyroBias(float gyro_bias[3], float acc_g[3])
{
    gyr_err[0] += gyro_bias[0];
    gyr_err[1] += gyro_bias[1];
    gyr_err[2] += gyro_bias[2];
    acc0[0] = acc_g[0];
    acc0[1] = acc_g[1];
    acc0[2] = acc_g[2];
    NormAcc0 = calculateNorm(acc0);
}

void IMU::KeepGroudValues()
{
    for (int i = 0; i < 3; i++)
    {
        v[i] = 0;
        p[i] = 0;
        q[i] = q0[i];
    }
    q[3] = q0[3];
}

void IMU::SetAcc0(float *newacc0)
{
    acc0[0] = newacc0[0];
    acc0[1] = newacc0[1];
    acc0[2] = newacc0[2];
    NormAcc0 = calculateNorm(acc0);
}