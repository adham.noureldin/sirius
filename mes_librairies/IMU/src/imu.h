#include <Arduino.h>
#include <string>
#include <cmath>
#include <array>
#include <iostream>
// #include "defines.h"

#define PRINT_SERIAL_MODE true

// #define FLAG_ANGLES_RAMPE

// Choix d'afficher ou pas les données sur le Serial

// Choix de l'IMU
#define SparkFunMPU9250 0
#define MPU6050 1
#define IMU3 2
#define NOM_IMU IMU3

#if NOM_IMU == SparkFunMPU9250
#include <SparkFunMPU9250-DMP.h>
#endif
#if NOM_IMU == MPU6050
#include <Adafruit_MPU6050.h>
#endif
#if NOM_IMU == IMU3
#include <DFRobot_ICM42688.h>
#endif

class IMU
{
private:
#if NOM_IMU == SparkFunMPU9250
    MPU9250_DMP imu; // Librairie SparkFunMPU9250
#endif
#if NOM_IMU == MPU6050
    Adafruit_MPU6050 mpu; // ALibrairie Adafruit_MPU6050
#endif
#if NOM_IMU == IMU3
    int cspin;
    DFRobot_ICM42688_SPI ICM42688;
#endif

    bool imu_on;
    // const int freq = 4;
    uint64_t tMesure;   // instant du dernier appel à Update(). On en a besoin pour calculer dt
    uint64_t dt;        // Pas de temps entre deux appels à Update(). On en a besoin pour l'intégration
    float NormAcc0 = 0; // On en a besoin dans ActualiserDonnées()
    float acc[3] = {0, 0, 0};
    float acc0[3] = {0, 0, 0};
    float gyr[3] = {0, 0, 0};
    float gyr_err[3] = {0, 0, 0};
    float p[3] = {0, 0, 0}; // vecteur position
    float v[3] = {0, 0, 0}; // vecteur vitesse
    float theta = 0;        // assiette EN RADIANS
    float alpha = 0;        // azimuth
    float theta0 = 0;
    float dtheta = 10 * DEG_TO_RAD; // écart max de theta par rapport au décollage pour pouvoir allumer le moteur
    float alpha0 = 0;
    float dalpha = 45 * DEG_TO_RAD;                         // écart max d'alpha par rapport au décollage pour pouvoir allumer le moteur
    float Rbeta[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}; // Matrice de rotation d'axe U et d'angle beta
    float q[4] = {1, 0, 0, 0};                              // "quaternion de changement de base"
                                                            /*
                                                            float E[3]; // Coordonnées de E dans la base de la fusée
                                                            float N[3]; // Coordonnées de N dans la base de la fusée
                                                            float U[3]; // Coordonnées de U dans la base de la fusée
                                                            */
    float q0[4] = {0, 0, 0, 0};
    uint64_t previousMicros = 0;
    int overflowCount = 0;

    float theta_sep_stabtraj = 72 * DEG_TO_RAD;
    // float alpha_sep_stabtraj = 0;

    float numero_bande_alpha = 0; // 0 -> -180<alpha<180, 1 ->  180<alpha<180+360 etc...

public:
    IMU();
    uint64_t getMicros();
    void Init(int cspin);
    void Update();
    void Calibrate();
    void UpdateCal();
    float *GetAcc();
    float *GetGyr();
    float *Getp();
    float *Getv();
    float *Getq();
    float GetTheta();
    float GetAlpha();
    uint64_t Getdt();
    void Printpv();
    void PrintThetaAlpha();
    void GetDataAsString();
    bool AnglesOK();
    bool AlphaOK();
    bool ThetaOK();
    float calculateBeta(float alpha0, float U[3]);
    void calculateRotationMatrix(float beta, float *U, float R[3][3]);
    void rotMatrixToQuat(float M[3][3], float *q);
    void calculateTheta();
    void calculateAlpha();
    float calculateNorm(float *U);
    void Calculateq0();
    void ActualiserLesTemps();
    void ActualiserDonnees(bool actualiser_pos_et_vitesse);
    bool IsON();
    void shutdown();

    void setGyroBias(float gyro_bias[3], float acc_g[3]);
    void KeepGroudValues();
    void SetAcc0(float *newacc0);
};
