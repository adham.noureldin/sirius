import matplotlib.pyplot as plt
import matplotlib.animation as animation
from datetime import datetime
import time
import glob
import os
import numpy as np


def quatToMatrix(q):
    # q = [i, j, k, w]
    M = np.zeros((3, 3))
    M[0, 0] = 1 - 2 * (q[1] ** 2 + q[2] ** 2)
    M[0, 1] = 2 * (q[0] * q[1] + q[2] * q[3])
    M[0, 2] = 2 * (q[0] * q[2] - q[1] * q[3])
    M[1, 0] = 2 * (q[0] * q[1] - q[2] * q[3])
    M[1, 1] = 1 - 2 * (q[0] ** 2 + q[2] ** 2)
    M[1, 2] = 2 * (q[1] * q[2] + q[0] * q[3])
    M[2, 0] = 2 * (q[0] * q[2] + q[1] * q[3])
    M[2, 1] = 2 * (q[1] * q[2] - q[0] * q[3])
    M[2, 2] = 1 - 2 * (q[0] ** 2 + q[1] ** 2)
    return M.T


# create a circular buffer
buffer_size = 1000
buffer = []

# create a dictionary to store file positions
file_positions = {}
animation_paused = False
# create a list of files to read
files = glob.glob("data.txt*")
fs = {file: open(file, "r") for file in files}


# Function to read lines from a file and update circular buffer
def update_buffer(file):
    global buffer, buffer_size, file_positions
    try:
        with open(file, "r") as f:
            f.seek(file_positions.get(file, 0))
            lines = f.readlines()
            for line in lines:
                if line.strip():  # Check if the line is not empty
                    try:
                        # line is t, i, j, k, w
                        # convert to (t, [i, j, k, w])
                        t = float(line.split(",")[0])
                        value = [float(x) for x in line.split(",")[1:]]
                        buffer.append((t, value))

                        if len(buffer) > buffer_size:
                            buffer.pop(0)  # Remove the oldest element
                    except ValueError:
                        print(f"Error converting line to float: {line}")

            file_positions[file] = f.tell()  # Update file position
    except FileNotFoundError:
        print(f"File not found: {file}")


# Function to check for changes in files
def check_files():
    global files, fs, file_positions
    current_files = glob.glob("data.txt*")

    # Check for deleted files
    deleted_files = [file for file in file_positions if file not in current_files]
    for deleted_file in deleted_files:
        fs.pop(deleted_file, None)
        del file_positions[deleted_file]
        if deleted_file in files:
            files.remove(deleted_file)
            print(f"File removed: {deleted_file}")

    # Check for new or modified files
    for file in current_files:
        if file not in fs:
            files.append(file)
            try:
                fs[file] = open(file, "r")
            except FileNotFoundError:
                print(f"File not found: {file}")
        elif file in file_positions and os.path.getsize(file) < file_positions[file]:
            # File has been truncated or modified, reset file position
            fs[file].seek(0)
            file_positions[file] = 0


# Function to update the plot
def update_plot(frame):
    global buffer, buffer_size
    # Check for changes in files
    check_files()

    # Read new data from files and update buffer
    for file in files:
        update_buffer(file)

    # Trim buffer to specified size
    buffer = buffer[-buffer_size:]

    # Extract the rotation matrix from the latest quaternion
    ts, data = zip(*buffer)
    # p = [x[6:9] for x in data]
    # v = [x[9:12] for x in data]
    # theta = [x[5] for x in data]
    # alpha = np.unwrap(np.array([x[4] for x in data])+180)-180
    quat = data[-1][:4]
    # print(ts)
    # # stop program now
    # exit()

    M = quatToMatrix(quat)

    # Plot the basis vectors in 3D
    ax.clear()
    E = np.array([1, 0, 0])
    N = np.array([0, 1, 0])
    U = np.array([0, 0, 1])

    X = np.dot(M, E)
    Y = np.dot(M, N)
    Z = np.dot(M, U)

    # Plot X, Y, Z vectors in the ENU frame
    ax.quiver(0, 0, 0, X[0], X[1], X[2], color="r", label="X")
    ax.quiver(0, 0, 0, Y[0], Y[1], Y[2], color="g", label="Y")
    ax.quiver(0, 0, 0, Z[0], Z[1], Z[2], color="b", label="Z")

    ax.set_xlim([-1, 1])
    ax.set_ylim([-1, 1])
    ax.set_zlim([-1, 1])
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    ax.legend()

    # # Subplot for theta and alpha
    # ax2.clear()
    # ax2.plot(ts, theta, label='Assiette', color='m')
    # ax2.set_xlabel('Time')
    # ax2.set_ylabel('Value')
    # ax2.set_ylim([-90, 90])
    # # set ticks at [-90, -45, 0, 45, 90]
    # ax2.set_yticks(np.arange(-90, 91, 45))
    # ax2.grid()
    # ax2.legend()

    # ax3.clear()
    # ax3.plot(ts, alpha, label='Azimuth', color='c')
    # ax3.set_xlabel('Time')
    # ax3.set_ylabel('Value')
    # ax3.set_ylim([-180, 180])
    # ax3.autoscale(enable=True, axis='y', tight=True)
    # # set ticks at [-180, -90, 0, 90, 180]
    # ax3.set_yticks(np.arange(-180, 181, 90))
    # ax3.grid()
    # ax3.legend()

    # ax4.clear()
    # ax4.plot(ts, p, label='Position')
    # ax4.set_xlabel('Time')
    # ax4.set_ylabel('Value')
    # ax4.set_ylim([-1, 1])
    # ax4.autoscale(enable=True, axis='y', tight=True)
    # ax4.legend()
    # ax4.grid()

    # ax5.clear()
    # ax5.plot(ts, v, label='Velocity')
    # ax5.set_xlabel('Time')
    # ax5.set_ylabel('Value')
    # ax5.set_ylim([-1, 1])
    # ax5.autoscale(enable=True, axis='y', tight=True)
    # ax5.legend()
    # ax5.grid()


# Create a 3D plot with three subplots, one over the other
fig = plt.figure(figsize=(10, 10))

# Create a 3D subplot on top
ax = fig.add_subplot(111, projection="3d")

# Create the second subplot in the middle
# ax2 = fig.add_subplot(512)

# # Create the third subplot at the bottom
# ax3 = fig.add_subplot(513)

# ax4 = fig.add_subplot(514)

# ax5 = fig.add_subplot(515)

# Set up the animation
ani = animation.FuncAnimation(fig, update_plot, interval=100)

# Show the plot
plt.show()
