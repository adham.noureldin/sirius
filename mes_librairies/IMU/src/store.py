# read serial port and display data
import logging
from logging.handlers import TimedRotatingFileHandler
import time
import serial
import serial.tools.list_ports
import numpy as np

file_name = "data.txt"

# Set up logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Define the file handler with TimedRotatingFileHandler
handler = TimedRotatingFileHandler(file_name, when="s", interval=50, backupCount=2)
handler.setFormatter(logging.Formatter("{message}", style="{"))
logger.addHandler(handler)

# list serial ports
ports = list(serial.tools.list_ports.comports())
# Ouverture du port série
print("Liste des ports disponibles :")
for p in ports:
    print(p)
ser = serial.Serial(
    ports[0][0], 115200, timeout=1
)  # changer le port s'il ne correspond pas à celui d'Arduino

print("Ouverture du port série : " + ser.name)


while True:
    line = ser.readline()
    if line == b"":
        continue
    line = line.decode("utf-8")  # convert bytes array to string
    # convert string to list of 4 doubles
    data = np.array(line.split(","), dtype=float)
    if len(data) != 4:
        continue

    # logger.info(str(time.time()) + "," + str(data[0]) + "," + str(data[1]) + "," + str(data[2]) + "," + str(data[3]) + "," + str(data[4]) + "," + str(data[5]))
    S = str(time.time())
    for i in range(len(data)):
        S = S + "," + str(data[i])
    logger.info(S)
