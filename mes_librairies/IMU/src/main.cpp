#include "imu.h"

IMU imu;

void setup()
{
  delay(5000);
  Serial.begin(115200);
  while (!Serial)
  {
  }

  Serial.println("Serial OK");
  delay(1000);
  imu.Init(9);     // On initialise l'IMU
  imu.Calibrate(); // On calibre le gyromètre

  imu.Calculateq0(); // On calcule q0, le quaternion obtenu à partir de M0 (cf pdf)
}

// Il y a plusieurs fonctions loop() commentées, à décommenter pour tester les différentes fonctionnalités de l'IMU

// Tests IMU de base

// void loop()
// {
//   imu.Update();
//   Serial.println("p");
//   imu.GetDataAsString();
//   delay(10);
// }

// void loop()
// {
//   imu.UpdateCal();
//   float *acc = imu.GetAcc();
//   for (int i = 0; i < 3; i++)
//   {
//     Serial.print(acc[i]);
//     Serial.print(" ");
//   }
//   float *gyr = imu.GetGyr();
//   for (int i = 0; i < 3; i++)
//   {
//     Serial.print(gyr[i]);
//     Serial.print(" ");
//   }
//   Serial.println();
//   delay(100);
// }

// // Code à faire tourner avant da lancer les deux codes python
// void loop()
// {
//   imu.ActualiserDonnées(); // imu.Update(); est fait dans cette fonction
//   float *q = imu.Getq();   // On récupère q
//   Serial.print(q[0], 10);
//   Serial.print(",");
//   Serial.print(q[1], 10);
//   Serial.print(",");
//   Serial.print(q[2], 10);
//   Serial.print(",");
//   Serial.print(q[3], 10);
//   Serial.println();
// }

// Code pour afficher l'assiette et l'azimuth
void loop()
{
  imu.ActualiserDonnees(true); // imu.Update(); est fait dans cette fonction
  Serial.print("theta: ");
  Serial.print(imu.GetTheta() * RAD_TO_DEG);
  Serial.print(" - alpha: ");
  Serial.println(imu.GetAlpha() * RAD_TO_DEG);
}