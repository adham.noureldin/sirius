#include "Micro.h"

Micro::Micro(TwoWire *wire, AudioRecordQueue_F32* queue1ptr, AudioRecordQueue_F32* queue2ptr, uint8_t nb_startups){
  this->wire = wire;
  this->queue1ptr = queue1ptr;
  this->queue2ptr = queue2ptr;
  this->nb_startups = nb_startups;
}

void Micro::Init(uint8_t nb_startups){
  this->nb_startups = nb_startups;
  
  AudioMemory(20);
  AudioMemory_F32(800);
  delay(10);
  wire->begin();
  delay(500);

  // Wake-up output block (default state after powering)
  // The first line sends the addressing packet "0x9C" (see TLV320ADC6120 datasheet)
  // The address is indeed 0x4E in 7 bits, the write bit transforms it to 0x9C
  wire->beginTransmission(0x4E);
  wire->write(0x02);
  wire->write(0x81);
  wire->endTransmission();
  delay(300); // Allows board to wake up

  // Board configuration
  // I2S output configuration
  wire->beginTransmission(0x4E);
  wire->write(0x07);
  wire->write(0x38);
  wire->endTransmission();
  delay(300);

  // Enable inputs 1 and 2
  wire->beginTransmission(0x4E);
  wire->write(0x73);
  wire->write(0xC0);
  wire->endTransmission();
  delay(300);

  // Enable audio output
  wire->beginTransmission(0x4E);
  wire->write(0x74);
  wire->write(0xC0);
  wire->endTransmission();
  delay(300);

  // Power up the microphones, internal ADC, and PLL
  wire->beginTransmission(0x4E);
  wire->write(0x75);
  wire->write(0xE0);
  wire->endTransmission();
  delay(300);

  // Apply gain of 1dB to ch1
  wire->beginTransmission(0x4E);
  wire->write(0x3D);
  wire->write(0x80);
  wire->endTransmission();
  delay(300);
  // Apply gain of 1dB to ch2
  wire->beginTransmission(0x4E);
  wire->write(0x42);
  wire->write(0x80);
  wire->endTransmission();
  delay(300);

  open_audio_files();
  delay(100);
}

void Micro::open_audio_files(){
  // String str_audio_filename1 = "audio" + String(nb_startups) + "_" + String(nb_files) + "_1.raw";
  char audio_filename[64];
  sprintf(audio_filename, "audio%d_%d_1.raw", nb_startups, nb_files);
  Serial.printf("Opening file: %s\n", audio_filename);
  // audio_file1 = SD.sdfs.open(audio_filename, FILE_WRITE | O_CREAT);
  audio_file1.open(audio_filename, O_WRITE | O_CREAT | O_TRUNC);

  sprintf(audio_filename, "audio%d_%d_2.raw", nb_startups, nb_files);
  Serial.printf("Opening file: %s\n", audio_filename);
  // audio_file2 = SD.sdfs.open(audio_filename, FILE_WRITE | O_CREAT);
  audio_file2.open(audio_filename, O_WRITE | O_CREAT | O_TRUNC);
  nb_files++;
  last_file_start_ms = millis();
}

void Micro::start_audio(){
  queue1ptr->begin();
  queue2ptr->begin();
}
void Micro::save_audio(bool remove_old_files){
  // print queue size
  // if(millis()%1000<100){
  //   Serial.println(queue1->available());
  // }
  // print memory

  if(queue1ptr->available() >= number_of_chunks){
    // Serial.printf("Memory32: %d\n", AudioMemoryUsageMax_F32());
    // Serial.printf("Memory: %d\n", AudioMemoryUsageMax());
    for(int i = 0; i < number_of_chunks; i++){
      memcpy(float_buffer1 + (i*128), queue1ptr->readBuffer(), 128 * 4);
      queue1ptr->freeBuffer();
    }
    audio_file1.write(audio_buffer1, number_of_chunks*512);
    audio_file1.sync();
  }
  if(queue2ptr->available() >= number_of_chunks){
    for(int i = 0; i < number_of_chunks; i++){
      memcpy(float_buffer2 + (i*128), queue2ptr->readBuffer(), 128 * 4);
      queue2ptr->freeBuffer();
    }
    audio_file2.write(audio_buffer2, number_of_chunks*512);
    audio_file2.sync();
  }

  if(millis() - last_file_start_ms > FILE_MS && remove_old_files){
    close_audio_files();
    open_audio_files();
  }
}

void Micro::close_audio_files(){
  audio_file1.close();
  audio_file2.close();
}

void Micro::end_audio(){
  queue1ptr->end();
  queue2ptr->end();
}

void Micro::shutoff(){
  close_audio_files();
  end_audio();
  wire->beginTransmission(0x4E);
  wire->write(0x2);
  wire->write(0x0);
  wire->endTransmission();
  delay(300);

  wire->end();
}