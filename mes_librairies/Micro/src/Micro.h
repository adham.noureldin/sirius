#include "OpenAudio_ArduinoLibrary.h"
#include "Wire.h"

#define FILE_MS 120*1000 // 2 minutes
#define number_of_chunks 16
class Micro {
    private:
        TwoWire *wire;
        AudioRecordQueue_F32* queue1ptr;
        AudioRecordQueue_F32* queue2ptr;

        byte audio_buffer1[512*number_of_chunks];
        byte audio_buffer2[512*number_of_chunks];
        float* float_buffer1 = (float*)audio_buffer1;
        float* float_buffer2 = (float*)audio_buffer2;

        FsFile audio_file1;
        FsFile audio_file2;

        uint8_t nb_startups = 0;
        uint32_t nb_files = 0;

        uint32_t last_file_start_ms = 0;

    public:
        Micro(TwoWire *wire, AudioRecordQueue_F32* queue1ptr, AudioRecordQueue_F32* queue2ptr, uint8_t nb_startups);
        void Init(uint8_t nb_startups);
        void open_audio_files();
        void start_audio();
        void save_audio(bool remove_old_files);
        void close_audio_files();
        void end_audio();
        void shutoff();
};