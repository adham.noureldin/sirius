#ifndef SDCard_h
#define SDCard_h

#include "DataLogger.h"
#include <SD.h>
#include <EEPROM.h>

#define FILEMS 60*1000 // 60 seconds

extern DataLogger mydl;
// extern uint8_t mybuffer[MAXBUFFER] DMAMEM; // buffer to store data

class SDCard
{
    private:
        char datafilename[64]; // stores raw data
        char full_datafilename[64]; // with extension and numbers
        char logfilename[64]; // stores log data (instead of Serial.print)

        FsFile datafile2; // file to store raw data. most recent
        FsFile datafile1; // last one
        FsFile datafile0; // second last one (to be removed if needed)

        FsFile logfile; // file to store log data

        uint16_t number_file = 0; // current file number
        uint32_t time_start_current_file = 0; // time when the current file was created

        uint32_t sample_rate; // sample rate of the sensors (Hz)
        uint16_t buffer_msec; // buffer size in milliseconds

        bool logging = false; // true if logging data

        TLoggerStat *lsptr;
        uint8_t *bufferend;
        elapsedMillis filemillis;
        elapsedMicros filemicros;
        
        unsigned int nb_startups;

    public:
        SDCard(uint16_t sample_rate, uint16_t buffer_msec);
        bool Init(uint16_t stsize, bool debug, char *filename);
        TLoggerStat * GetStatus();
        void CheckLogger(bool remove_old_files);
        void StartLogging();
        void MakeFileName(char *filename);
        void QuitLogging(void);

        
};


#endif