#include "Arduino.h"
#include "SDCard.h"
DataLogger mydl(10);

void LoggerISR(void) {  // call the data logger collection ISR handler
// Serial.println("here");
  mydl.TimerChore();
}

struct datrec {
  uint32_t microstime;
  float data1;
  float data2;
};

#define SAMPLERATE 32000
#define BUFFERMSEC 400
#define MAXBUFFER 200000
uint8_t mybuffer[MAXBUFFER];


SDCard sdcard = SDCard(SAMPLERATE, BUFFERMSEC);

void myCollector(void *vdp) {
  // Serial.println("here1");
  volatile struct datrec *dp;
  dp = (volatile struct datrec *)vdp;
  dp->microstime = micros();
  dp->data1 = 1.0;
  dp->data2 = 2.0;
  // Serial.printf("millis: %lu, data1: %f, data2: %f\n", dp->millistime, dp->data1, dp->data2);
}

void myBinaryDisplay( void* vdp) {
  struct datrec *dp;
  dp = (struct datrec *)vdp;
  TLoggerStat *tsp;
  tsp =  mydl.GetStatus(); // updates values collected at interrupt time

  Serial.printf("%8.3f,  ", dp->microstime / 1000.0);
}


void setup() {
    // put your setup code here, to run once:
    Serial.begin(115200);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for native USB port only
    }
    Serial.println("Initializing SD card...");
    char filename[] = "data";
    if (!sdcard.Init(sizeof(datrec), true, filename)) { // try starting SD Card and file system
    // initialize SD Card failed
        Serial.println("SD Card initialization failed");
        return;
    }
    uint32_t bufflen = mydl.InitializeBuffer(sizeof(datrec), SAMPLERATE, BUFFERMSEC, mybuffer);
    uint8_t *bufferend = mybuffer + bufflen;
    Serial.printf("End of buffer at %p\n", bufferend);
    if ((bufflen == 0) || (bufflen > MAXBUFFER)) {
        Serial.println("Not enough buffer space!  Reduce buffer time or sample rate.");
        return;
    }
    // uint8_t bufflen = mydl.InitializeBuffer(sizeof(datrec), SAMPLERATE, BUFFERMSEC, mybuffer);
    Serial.println("SD Card initialized.");

    mydl.AttachCollector(&myCollector);
    mydl.AttachDisplay(&myBinaryDisplay, 5000);
    Serial.println("Initialization done.");
    Serial.println("Starting logging...");
    // sdcard.StartLogging();
    Serial.println("Logging started.");
}
bool logging = false;
void loop() {
  if(!logging){
      sdcard.StartLogging();
      logging = true;
  }
    // put your main code here, to run repeatedly:
    sdcard.CheckLogger(false);
    delay(2);
}
