#include "SDCard.h"

SDCard::SDCard(uint16_t sample_rate, uint16_t buffer_msec){
    // mybuffer = mybuffer;
    sample_rate = sample_rate;
    buffer_msec = buffer_msec;
}
// afptr is a pointer to a function that takes a void pointer as argument and returns void
// This function is the callback function that will be called each time a sample is collected
bool SDCard::Init(uint16_t stsize, bool debug, char *filename){
    nb_startups = EEPROM.read(0);
    EEPROM.write(0, nb_startups + 1);

    strcpy(datafilename, filename);

    mydl.SetDBPrint(debug);
    uint32_t bufflen;
    if (!mydl.InitStorage(NULL)) { // try starting SD Card and file system
    // initialize SD Card failed
        Serial.println("SD Card initialization failed");
        return false;
    }
    // Now attach our customized callback functions
    // mydl.AttachCollector(&myCollector); // specify our collector callback function
    // If you attach no writer or a NULL pointer, by default all binary data is written
    // mydl.AttachDisplay(&myBinaryDisplay, 5000); // display written data once per 5 seconds
    // mydl.AttachPlayback(&myVerify);  // check for missing records
    return true;
}


TLoggerStat * SDCard::GetStatus(){
    return mydl.GetStatus();
}

void SDCard::CheckLogger(bool remove_old_files){
    // Serial.println("Checking Logger.");
    TLoggerStat *tsp;
    tsp =  mydl.GetStatus();
    mydl.CheckLogger();  // check for data to write to SD at regular intervals
    // Serial.println("logger checked");
    if (logging) {
        if (tsp->spaceavailable < (10240l * 1024)) { //we are within 10MB of end of card
            Serial.println("Halting logging.  Getting near end of SD Card.");
            QuitLogging();
            GetStatus();
        }
        if(remove_old_files && (millis() - time_start_current_file > FILEMS)){
            number_file++;
            datafile0 = datafile1;
            datafile1 = datafile2;
            // remove second last file
            if(number_file >= 2){
                number_file -= 2;
                // char old_datafilename[64];
                // MakeFileName(old_datafilename);
                // Serial.println("Removing old file: ");
                // Serial.println(old_datafilename);
                char get_name[64];
                datafile0.getName(get_name, 64);
                Serial.print("Removing file: ");
                Serial.println(get_name);
                // remove file
                bool res = datafile0.remove();
                // bool res = SD.sdfs.remove(old_datafilename);
                if(res){
                    Serial.println("File removed.");
                }else{
                    Serial.println("Error removing file.");
                }
                number_file += 2;
            }


            MakeFileName(full_datafilename);
            mydl.StartLogger(full_datafilename, 1000, &LoggerISR, &datafile2);  // sync once per second
            // print name of datafile
            char get_name[64];
            datafile2.getName(get_name, 64);
            Serial.print("New file: ");
            Serial.println(get_name);
            time_start_current_file = millis();
            filemillis = 0; // Opening file takes ~ 40mSec
            filemicros = 0;

        }
    }
}

void SDCard::StartLogging(){
    Serial.println("Starting Logger.");
    logging = true;
    MakeFileName(full_datafilename);
    mydl.StartLogger(full_datafilename, 1000, &LoggerISR, &datafile2);  // sync once per second
    // char get_name[64];
    // datafile.getName(get_name, 64);
    // Serial.print("New file: ");
    // Serial.println(get_name);
    time_start_current_file = millis();
    filemillis = 0; // Opening file takes ~ 40mSec
    filemicros = 0;
    Serial.print("\n");
}

void SDCard::MakeFileName(char *filename) {
    // {datafilename}_{nb_startups}_{number_file}.bin
    sprintf(filename, "%s_%d_%d.bin", datafilename, nb_startups, number_file);
}

void SDCard::QuitLogging(){
    Serial.println("Stopping Logger.");
    logging = false;
    mydl.StopLogger();
}