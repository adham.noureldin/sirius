#include "Pitot.h"

Pitot::Pitot()
{
}

bool Pitot::Init()
{
    Wire2.begin();
    Wire2.setClock(400000);
    /*
     * I2C address of 0x28, on bus 0, with a -1 to +1 PSI range
     */
    pres.Config(&Wire2, 0x28, 1.0f, -1.0f);
    /* Starting communication with the pressure transducer */
    if (!pres.Begin())
    {
        Serial.println("Error communicating with pitot");
        return false;
    }
    
    return true;
}

bool Pitot::Read()
{
    return pres.Read();
}

float Pitot::pres_pa()
{
    return abs(pres.pres_pa() + 120);
}

float Pitot::die_temp_c()
{
    return pres.die_temp_c();
}

float Pitot::vitesse_ms()
{
    return sqrt(2 * pres_pa() / rho_air);
}

void Pitot::Affichagestyle()
{
    /* Display the data */
    for (int i = 0; i < pres_pa() / 10; i++)
    {
        Serial.print('*');
    }
    Serial.println();
}
