#include "ms4525do.h"

class Pitot
{
private:
    bfs::Ms4525do pres;
    float rho_air = 1.204; // Masse volumique de l'air en kg/m^3

public:
    Pitot();
    bool Init();
    bool Read();
    float pres_pa();
    float die_temp_c();
    float vitesse_ms();
    void Affichagestyle();
};
