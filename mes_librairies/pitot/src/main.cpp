#include "Pitot.h"

/*
 * An MS4525DO object
 */

Pitot pitot;

void setup()
{
  /* Serial to display data */
  Serial.begin(9600);
  delay(1000);
  Serial.println("Serial OK");
  pitot.Init();
}

void loop()
{
  /* Read the sensor */
  if (pitot.Read())
  {
    Serial.print("Pression : ");
    Serial.print(pitot.pres_pa(), 6);
    Serial.print(" - Vitesse : ");
    Serial.println(pitot.vitesse_ms(), 6);
    // pitot.Affichagestyle();
  }
  delay(10);
}
