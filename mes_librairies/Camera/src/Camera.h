// #include "defines.h"
#include <Arduino.h>
#define BUFF_SIZE 20
#define TIME_BETWEEN_SWITCH 5000
// #define HWSERIAL Serial3

class Camera
{
private:
    uint8_t txBuf[BUFF_SIZE], crc; // array 8 bits
    int recState = 0;              // State of the camera, 0 if sleep state, 1 if activated
    HardwareSerial *CamSerial;
    uint32_t t_millis_dernier_switch = millis(); // Temps depuis la dernière fois qu'on a allumé ou éteint la caméra

public:
    Camera();
    uint8_t crc8_calc(uint8_t crc, unsigned char a, uint8_t poly);
    uint8_t calcCrc(uint8_t *buf, uint8_t numBytes);
    bool Init(HardwareSerial *CamSerial, unsigned long baud);
    bool Power_on();
    bool Power_off();
};
