#include "Camera.h"

Camera::Camera()
{
}

// crc : cyclic redundancy check, used to detect ingerence, transmission errors
uint8_t Camera::crc8_calc(uint8_t crc, unsigned char a, uint8_t poly)
{
    crc ^= a;                      // XOR byte to byte
    for (int ii = 0; ii < 8; ++ii) // Boucle sur chaque bit de crc
    {
        if (crc & 0x80)
            crc = (crc << 1) ^ poly; // décalage de bits vers la gauche puis XOR avec poly
        else
            crc = crc << 1; // décalage de bits vers la gauche
    }

    return crc;
}

uint8_t Camera::calcCrc(uint8_t *buf, uint8_t numBytes)
{
    uint8_t crc = 0;
    for (uint8_t i = 0; i < numBytes; i++)
        crc = crc8_calc(crc, *(buf + i), 0xd5);

    return crc;

} // calcCrcTx

bool Camera::Init(HardwareSerial *CamSerial, unsigned long baud)
{
    Serial.println("Init Camera");
    this->CamSerial = CamSerial;
    CamSerial->begin(baud);
    // give the runcam time to send whatever it does out of reset
    delay(1000);
    // serial command to toggle recording
    // more info on the serial packet structure --> https://support.runcam.com/hc/en-us/articles/360014537794-RunCam-Device-Protocol
    // Request packet structure : Header, Command ID, Action ID, Check code
    txBuf[0] = 0xCC;
    txBuf[1] = 0x01;
    txBuf[2] = 0x01;
    txBuf[3] = calcCrc(txBuf, 3); // compute the CRC
    t_millis_dernier_switch = millis() - TIME_BETWEEN_SWITCH;
    // Initialement, la caméra est éteinte
    recState = 0;
    // Power_off();
    delay(600);

    Serial.println(txBuf[3]);
    Serial.println("Camera initialized");

    bool res = CamSerial->available();
    while (CamSerial->available())
    {
        Serial.write(CamSerial->read());
    } // Flush the buffer
    return res;
}

bool Camera::Power_on()
{
    if (recState == 0 && abs(millis() - t_millis_dernier_switch) >= TIME_BETWEEN_SWITCH) // Si la caméra n'est pas allumée, on l'allume. Sinon, rien à faire
                                                                                         // et on vérifie qu'on a éteint la cam depuis assez longtemps
    {
        Serial.println("Camera: Starting Recording");
        recState = 1;
        t_millis_dernier_switch = millis();
        CamSerial->write(txBuf, 4);
    }
    bool res = CamSerial->available();

    while (CamSerial->available())
    {
        Serial.write(CamSerial->read());
    } // Flush the buffer
    return res;
}

bool Camera::Power_off()
{
    if (recState == 1 && abs(millis() - t_millis_dernier_switch) >= TIME_BETWEEN_SWITCH) // Si la caméra est allumée, on l'éteint. Sinon, rien à faire.
                                                                                         // et on vérifie qu'on a allumé la cam depuis assez longtemps
    {
        Serial.println("Camera: Stopping Recording");
        recState = 0;
        t_millis_dernier_switch = millis();
        CamSerial->write(txBuf, 4);
    }
    bool res = CamSerial->available();
    while (CamSerial->available())
    {
        Serial.write(CamSerial->read());
    } // Flush the buffer
    return res;
}
