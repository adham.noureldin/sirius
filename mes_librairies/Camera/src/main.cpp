// #include <Arduino.h>

// #define BUFF_SIZE 20
// #define HWSERIAL Serial3

// uint8_t txBuf[BUFF_SIZE], crc; // array 8 bits
// int recState = 0;              // State of the camera, 0 if sleep state, 1 if activated

// void setup(void)
// {
//     Serial.begin(9600);
//     HWSERIAL.begin(115200);
//     // give the runcam time to send whatever it does out of reset
//     delay(3000);

//     // serial command to toggle recording
//     // more info on the serial packet structure --> https://support.runcam.com/hc/en-us/articles/360014537794-RunCam-Device-Protocol
//     // Request packet structure : Header, Command ID, Action ID, Check code
//     txBuf[0] = 0xCC;
//     txBuf[1] = 0x01;
//     txBuf[2] = 0x01;
//     txBuf[3] = calcCrc(txBuf, 3); // compute the CRC
//     Serial.println(txBuf[3]);
// }

// void loop()
// {
//     Serial.println("Enter 1 to start recording or 2 to stop recording:");

//     while (Serial.available() == 0)
//     {
//         Serial.println("Starting not available");
//     }

//     int userInput = Serial.parseInt();

//     if (userInput == 1)
//     {
//         Power_on();
//     }
//     else if (userInput == 2)
//     {
//         Power_off();
//     }

//     delay(500);
// }

// void Power_on()
// {
//     if (recState == 0)
//     {
//         Serial.println("Starting Recording");
//         recState = 1;
//         HWSERIAL.write(txBuf, 4);
//     }
// }

// void Power_off()
// {
//     if (recState == 1)
//     {
//         Serial.println("Stopping Recording");
//         recState = 0;
//         HWSERIAL.write(txBuf, 4);
//     }
// }

// uint8_t calcCrc(uint8_t *buf, uint8_t numBytes)
// {
//     uint8_t crc = 0;
//     for (uint8_t i = 0; i < numBytes; i++)
//         crc = crc8_calc(crc, *(buf + i), 0xd5);

//     return crc;

// } // calcCrcTx

// // crc : cyclic redundancy check, used to detect ingerence, transmission errors
// uint8_t crc8_calc(uint8_t crc, unsigned char a, uint8_t poly)
// {
//     crc ^= a;                      // XOR byte to byte
//     for (int ii = 0; ii < 8; ++ii) // Boucle sur chaque bit de crc
//     {
//         if (crc & 0x80)
//             crc = (crc << 1) ^ poly; // décalage de bits vers la gauche puis XOR avec poly
//         else
//             crc = crc << 1; // décalage de bits vers la gauche
//     }

//     return crc;
// }

///////////////////////
/////// Test Cam //////
///////////////////////

#include <Arduino.h>
#include "Camera.h"
// #define PRINT_SERIAL_MODE true
#define IS_BETA

Camera cam1;
#ifdef IS_BETA
Camera cam2;
Camera cam3;
Camera cam4;
#endif
// bool cam1_res = cam1.Init(&Serial3, 115200);
//     bool cam2_res = cam2.Init(&Serial4, 115200);
//     bool cam3_res = cam3.Init(&Serial7, 115200);
//     bool cam4_res = cam4.Init(&Serial8, 115200);
void setup()
{
    // put your setup code here, to run once:
    Serial.begin(115200);
    delay(1000);
    Serial.println("Start");
    delay(10000);
#ifdef IS_BETA
    cam1.Init(&Serial3, 115200);
    cam2.Init(&Serial4, 115200);
    cam3.Init(&Serial7, 115200);
    cam4.Init(&Serial8, 115200);
#else
    Serial5.setRX(20);
    Serial5.setTX(21);
    cam1.Init(&Serial5, 115200);
#endif
    Serial.println("Camera initialized");
    cam1.Power_on();
#ifdef IS_BETA
    cam2.Power_on();
    cam3.Power_on();
    cam4.Power_on();
#endif
    Serial.println("Camera on");
    delay(15000);
    cam1.Power_off();
#ifdef IS_BETA
    cam2.Power_off();
    cam3.Power_off();
    cam4.Power_off();
#endif
    Serial.println("Camera off");
    delay(10000);
}

void loop()
{
    // while(Serial8.available())
    // {
    //     Serial.write(Serial8.read());
    // }
}