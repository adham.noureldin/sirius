#include <Arduino.h>

#define LoraSerial Serial3

class Lora
{
private:
    int mode;
    int baud_rate;

public:
    Lora();
    bool Init(int baud_rate, float frequency);
    int get_mode(); // const permet d'éviter dla modification de l'objet, &L objet L en référence à la classe Lora
    void set_mode(int new_mode);
    int get_baud_rate();
    void set_sleep_mode();
    void set_transmit_received_mode();
    void send_data(uint8_t *data, size_t length); // utiliser ça pour envoyer une struct
    void send_string(char *data);
    void Update();
};

uint8_t reverse_bits(uint8_t byte);
void reverse_bits_in_array(uint8_t* array, size_t length);
uint8_t fromUint7ToUint8(uint8_t data);
uint8_t fromUint4ToUint8(uint8_t data);
uint16_t fromFloatToUint16_t(float data);
uint32_t fromFloatToUint32_t(float data);
void toHex(void *data, size_t len, char *out);
