#include <Arduino.h>
#include "LoRa.h"

Lora Lora_emitter;

void setup()
{
  Serial.begin(115200);
  delay(1000);
  Lora_emitter.Init(115200, 868);
  Serial.println("lora setup done");
  Serial.println("Mode : ");
  Serial.println(Lora_emitter.get_mode());
  Serial.println("Starting");
}

void loop()
{
  Lora_emitter.send_string("Salut");
  delay(5000);
  // Read and print the response from the LoRa module (pour le debuggage)
  while (LoraSerial.available())
  {
    // Serial.println("here");
    Serial.write(LoraSerial.read()); // display the response from the Grove LoRa module
  }
  while (Serial.available())
  {
    LoraSerial.write(Serial.read()); // send message to the Grove LoRa module
  }
  Serial.println("Data sent");
  delay(2000);
  /*
  if (micros() > 100000000)
  {
    Lora_emitter.set_sleep_mode();
    Serial.println("Mode : ");
    Serial.println(Lora_emitter.get_mode());
  }
  */
}

