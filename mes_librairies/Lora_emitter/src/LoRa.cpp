#include "LoRa.h"

Lora::Lora()
{
}

int Lora::get_mode()
{
    return mode;
}

void Lora::set_mode(int new_mode)
{
    mode = new_mode;
}

int Lora::get_baud_rate()
{
    return baud_rate;
}

bool Lora::Init(int baud_rate, float frequency)
{
    baud_rate = baud_rate;  // This line has no effect and can be removed
    mode = 0;
    LoraSerial.begin(baud_rate);
    delay(1000);
    LoraSerial.write("AT+MODE=TEST\n");
    delay(1000);
    
    char freq[10];
    dtostrf(frequency, 6, 3, freq);  // Convert float to string with 3 decimal places

    // AT+TEST=RFCFG,[FREQUENCY],[SF],[BANDWIDTH],[TX PR],[RX PR],[TX POWER],[CRC],[IQ],[NET]
    LoraSerial.write("AT+TEST=RFCFG,");
    LoraSerial.write(freq);
    LoraSerial.write(",SF10,125,8,8,14,ON,OFF,OFF\n");
    delay(1000);

    return LoraSerial.available();
}


void Lora::set_sleep_mode()
{
    if (mode != 1)
    {
        LoraSerial.write("AT+MODE=1");
        set_mode(1);
    }
}

void Lora::set_transmit_received_mode()
{
    if (mode != 0)
    {
        LoraSerial.write("AT+MODE=0");
        set_mode(0);
    }
}

void Lora::send_data(uint8_t *data, size_t length)
{
    char hex[2 * length + 1];
    reverse_bits_in_array(data, length);
    toHex(data, length, hex);
    send_string(hex);
    reverse_bits_in_array(data, length);
    // Serial.println(hex);
}

void Lora::send_string(char *data)
{
    // send AT+TEST=TXLRPKT,"HEX"\r\n
    // LoraSerial.flush();
    LoraSerial.write("AT+TEST=TXLRPKT,\"");
    LoraSerial.write(data);
    LoraSerial.write("\"\r\n");
    // LoraSerial.flush();
}

void Lora::Update()
{
    while (LoraSerial.available())
    {
        // Serial.println("here");
        Serial.write(LoraSerial.read()); // display the response from the Grove LoRa module
    }
    // while (Serial.available())
    // {
    //     LoraSerial.write(Serial.read()); // send message to the Grove LoRa module
    // }
}

uint8_t reverse_bits(uint8_t byte)
{
    byte = (byte & 0xF0) >> 4 | (byte & 0x0F) << 4; // Swap nibbles
    byte = (byte & 0xCC) >> 2 | (byte & 0x33) << 2; // Swap pairs of bits
    byte = (byte & 0xAA) >> 1 | (byte & 0x55) << 1; // Swap individual bits
    return byte;
}

// Function to reverse the bits of each element in the array
void reverse_bits_in_array(uint8_t *array, size_t length)
{
    for (size_t i = 0; i < length; i++)
    {
        array[i] = reverse_bits(array[i]);
    }
}

uint8_t fromUint7ToUint8(uint8_t data)
{
    // reverse the bits of the data
    data = reverse_bits(data);
    // // shift the data to the right by 1 bit
    data >>= 1;
    return data;
}

uint8_t fromUint4ToUint8(uint8_t data)
{
    // reverse the bits of the data
    data = reverse_bits(data);
    // // shift the data to the right by 1 bit
    data >>= 4;
    return data;
}

uint16_t fromFloatToUint16_t(float data)
{
    union
    {
        float f;
        uint32_t u;
    } f2u = {.f = data};

    uint16_t sign = (f2u.u >> 31) & 0x0001;
    uint16_t exponent = (f2u.u >> 23) & 0x00FF;
    uint32_t mantissa = f2u.u & 0x007FFFFF;

    uint16_t hfloat = 0;
    if (exponent == 0 && mantissa == 0)
    {
        // Zero
        hfloat = sign << 15;
    }
    else if (exponent == 0xFF)
    {
        // Infinity or NaN
        hfloat = (sign << 15) | 0x7C00 | (mantissa >> 13);
    }
    else
    {
        // Normalized number
        exponent -= 127;
        exponent += 15;
        if (exponent >= 31)
        {
            // Overflow, return infinity
            hfloat = (sign << 15) | 0x7C00;
        }
        else if (exponent <= 0)
        {
            // Underflow
            if (exponent < -10)
            {
                // Too small for subnormal representation, return zero
                hfloat = sign << 15;
            }
            else
            {
                mantissa |= 0x00800000;
                uint32_t shift = 14 - exponent;
                hfloat = (sign << 15) | (mantissa >> shift);
                if ((mantissa >> (shift - 1)) & 0x0001)
                    hfloat += 1;
            }
        }
        else
        {
            hfloat = (sign << 15) | (exponent << 10) | (mantissa >> 13);
            if (mantissa & 0x00001000)
                hfloat += 1;
        }
    }
    // reverse_bits_in_array((uint8_t *)&hfloat, sizeof(hfloat));
    return hfloat;
}

uint32_t fromFloatToUint32_t(float data)
{
    union
    {
        float f;
        uint32_t u;
    } f2u = {.f = data};
    // reverse_bits_in_array((uint8_t *)&f2u.u, sizeof(f2u.u));
    return f2u.u;
}

void toHex(void *data, size_t len, char *out)
{
    static const char hex_table[] = "0123456789ABCDEF"; // Lookup table for hex digits

    uint8_t *byteData = (uint8_t *)data;
    for (size_t i = 0; i < len; i++)
    {
        uint8_t byte = byteData[i];

        // Convert the most significant nibble (4 bits) to hex
        *out++ = hex_table[(byte >> 4) & 0xF];

        // Convert the least significant nibble (4 bits) to hex
        *out++ = hex_table[byte & 0xF];
    }

    // Add null terminator for the string
    *out = '\0';
}